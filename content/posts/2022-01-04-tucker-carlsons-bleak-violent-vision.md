---
title: Tucker Carlsons bleak, violent vision
author: guyberliner
type: post
date: 2022-01-04T00:40:40+00:00
url: /2022/01/04/tucker-carlsons-bleak-violent-vision/
categories:
  - Uncategorized

---
Curators of the newish leftist, collaborative political blog, "Money on the Left", and an associated podcast, "Superstructure" (https://moneyontheleft.org/ ), have been putting out some genuinely engaging and thought provoking content recently, in my opinion. A notable case in point is their recent dissection of far right media gadfly Tucker Carlson.

Carlson is, in their estimation, a uniquely dangerous character in contemporary public life to have emerged out of the ultrareactionary Murdoch media empire in the past two decades. They exploit what they regard as flawed leftist critiques of his provocations as their foil to shed light on his modus operandi, as well as considering better responses to him.

The most common, flawed critique of Carlson that they cite is his supposed lack of populist "authenticity", as if there might actually be something worth salvaging from his antics, were he only more "sincere" about them. The "Superstructure" podcasters do an excellent job mercilessly debunking this flawed premise.

Along the way, they both conclude that Carlson very likely is largely sincere in his notorious drift towards openly fascistic demagoguery, while simultaneously pointing out the meaninglessness of "sincerity" as a norm for applying to such a provocateur.

Carlson, they point out, emerges out of a long line of similarly notorious "rightwing populists", harkening back to Father Coughlin in the 1930s, and earlier. All of them are driven by similar kinds of social conditions, and similarly seek to exploit such conditions for self-serving "political entrepreneurship" purposes.

In Carlson's bleak, violent vision, they explain, we are moving through a cycle of "decadence and decline" recalling many previous protofascistic theories of civilizational cycles (a la Oswald Spengler and Houston Stewart Chamberlain in Europe, or Lothrop Stoddard and Madison Grant on the North American side of the Atlantic) early in the last century.

Such historical background is important, they point out, because Carlson sees himself as a lineal inheritor of a certain aristocratic tradition that he ardently and apparently sincerely defends as "enlightened". "I'm the last real liberal! I believe in the Enlightenment!" he loudly protests.

But, in Carlson's retelling, the Enlightenment depended for its success on the predominance of the right people, with the right (naturally, genetic, in the final analysis) heritage of intelligence and culture. Today, all of that is threatened. And it's threatened, above all, from within, by a decadent "élite" that, instead of intelligently and realistically grappling with the demographic challenges presented by the rise of culturally (and, for practical purposes, essentially genetically) inferior groups, is courting disaster by lazily catering and pandering to those groups.

The proof of certain groups' "inferiority", in Carlson's completely stereotypical, social Darwinist conceptions, is completely demonstrated by their "natural failure" in the theoretically impartial and natural "marketplace". The obvious inconsistency inherent in railing against the same "marketplace" as also being "rigged" (against its rightful beneficiaries, ie, the demographically historical white majority) is completely beside the point. Fascism has always thrived on exactly such logical inconsistencies.

Underlying all of Carlson's concepts is the implicit notion of capitalist scarcity, and the assumption that reality is inherently a "zero sum game", and more for thee automatically means less for me. This invidious comparison sets the stage for all his vicious incitements against historically disadvantaged groups.

Carlson readily acknowledges the dangers of unlimited economic inequality and unbridled capitalism. "You let that fester too long, and voila, you wind up with Venezuela!" he flamboyantly lectures his fellow capitalists. The answer according to him, then, is to make some intelligent, strategic concessions to "the working class" -- but, importantly, to the "right" elements of that working class, those who pose the least demographic threat. And, meanwhile, to mobilize as much of those "right" elements in opposition against the "wrong", more threatening onesas possible.

Carlson meanwhile pronounces his dizzying array of bigotry across multiple different platforms, always "testing the waters" for his edgier provocations on his narrowest, predominantly online audiences first for their reactions, before deploying them to broader ones.

The biggest point the podcasters of "Superstructure" want to get across is that we cannot stop at debunking some of the obvious logical absurdities of Carlson's current worldview. Rather, we have to challenge its underlying assumptions head on. Because the greatest danger posed by Carlson, from a left perspective, is not any such "inconsistency" or "insincerity", but rather precisely his continued alignment with much of the "commonsense" of capitalist realism and neoliberalism.

The fact that, for all his "inconsistencies", Carlson's basic worldview is really only a relatively small deviation from most mainstream ¨centrist" orthodoxy on economics and politics represents precisely its greatest danger. Because at present, there is no really credible, fundamental critique being presented against it, based on first principles, from either the "center" or the "left".
