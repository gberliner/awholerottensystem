---
title: A New Axial Age
author: guyberliner
type: post
date: 2022-03-07T00:40:40+00:00
url: /2022/03/07/a-new-axial-age/
categories:
  - Uncategorized

---
German historian Karl Jaspers is credited with coining the highly influential phrase, "the Axial Age", to describe the historical epoch of approximately five to seven hundred years that witnessed the rise of most of the world's great religions (Buddhism, Christianity, Islam, Zoroastrianism, Jainism, etc). This period also witnessed the rise of large, urbanized civilizations and empires, hitherto unparalleled in extent and power, and a great intellectual and religious ferment. But alongside them, numerous other seers and religious visionaries, in the same times and places as the founders of these still extant world religions, were also active, on multiple continents, the vast majority of whom are little known today.

Nothing much could have been considered highly unique or remarkable about Jesus, for example, during his own lifetime, by most intelligent observers at that time, that would have uniquely distinguished him from a panoply of other, vaguely similar magi, wisemen, or zealots who were also active in early 1st century BCE Roman occupied Palestine. 

Likewise, the historical Buddha, Prince Gautama, scion of a minor royal family from a very small principality in Northern India, who joined one of numerous, loosely affiliated bands of wandering religious ascetics active in that region in the 5th century BCE. It would hardly have been possible for an ordinary observer at the time to have guessed that any of these figures would grow to become regarded as divine interlocutors of the first order by hundreds of millions of followers all over the world, in countries distant from their places of birth. 

Likewise, Muhammad started as the leader of obscure bands of Bedouins in the vast, (still to this day) sparsely populated wilderness of the sixth century CE Arabian peninsula. Etc.

And yet, these figures, active in a remarkably brief span out of the larger timeline of human history, wound up becoming pivotal over the past two thousand years of it. Whence an "axial age", where an axis is a line, a very slender pivot point (eg, "axle"), about which a much larger expanse (of time, in our case, or of physical mass, in the case of a wheel's axle) can revolve. A wheel, of course, is a rotational form of a lever, whose axle corresponds with a lever's fulcrum, tool upon which a modest force exerted closer to the axle/fulcrum can be converted into a vastly larger force/torque farther away from it. 

So, in this way even minor and originally barely heralded and possibly confabulated events in the lives of these figures came to have epochal significance for large masses of humanity thousands of years later.

Now, today, it is clear beyond doubt that we are likewise living in a new "axial age", in the sense that seemingly minor, daily events in our own lives (especially when considered collectively), are destined to have epochal significance, not just to human beings, but all other life on our planet, and not just a thousand or two thousand years hence, but on geological timescales, on the order of tens or possibly hundreds of thousands of years. Multiple "crises" (where "crisis" literally means "moment of judgment") are now in our laps (climate, pollution, the threat of nuclear war, and breakneck technological changes, to name a few of the most prominent), any single one of which could spell terminal disaster for the human race. 

So the temporal "axle" we are turning at the moment is monstrously larger than that earlier one, to which those famous prophets and visionaries just mentioned pressed their shoulders, two thousand years ago. And, most striking of all, unlike that earlier Axial Age, we can all KNOW, with hardly any doubt, that we are in fact in the midst of this one!

And yet, we do not widely consider the matter this way. Instead of regarding ourselves as we indeed are ("we are as Gods, and had might as well acknowledge it", Timothy Leary reportedly once said), we shrink with dread at these thoughts, this responsibility. Many if not most of us either live in active denial of these manifest truths, or look for something, anything, to distract us from our awesome new powers and responsibilities.

But it does not have to be this way. What if we acknowledged the really simple, inescapable truth that, for good or ill, today even the most ordinary man or woman on the street possesses a power over the future destinies of all life on our planet alongside which the importance of the historical Buddha or Jesus shrinks into almost provincial insignificance?

Could we rise to the occasion, to realize our mission, both individually and collectively, to soberly embrace our awesome powers and responsibilities, alongside which our personal, momentary creature comforts shrink into pathetic, infantile triviality?

The most ordinary human beings have been and are clearly capable of immense heroism and self-sacrifice, even during occasions that, in all truth, are not imbued with a minute fraction of the significance with which are own lives are at our own current historical moment -- and the latter verifiably, according to all objective, scientific reports by the world's best informed experts. 

Perhaps thoughts like these are the inspiration for projects like Stewart Brand's "Clock of the Long Now", to build a clock that can keep time continuously for ten thousand years. His associated foundation has adopted the custom of inserting a zero at the beginning of dates.  According to which, the current year should be written "02022". No easily foreseeable yet unanticipated "Y2K" type problems for them!

So welcome to the year 02022, bang on the axis of the Second Axial Age!
