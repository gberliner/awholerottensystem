---
title: "Overdose prevention sites"
author: guyberliner
type: post
date: 2023-05-15T00:40:40+00:00
url: /2023/05/15/overdose-prevention-sites/
categories:
  - Uncategorized

---

In the bourgeois version of utopia, proletarians are meant to be shiny, happy people, cheerfully producing and consuming for the benefit of the master class. Or, failing that, there's option 2, patiently and quietly awaiting their opportunity to do so in the reserve army of labor. Or failing that yet again, there's option 3, quietly expiring and being promptly disposed of in an inconspicuous but utmost sanitary pauper's cemetery.

In the real world, of course, nothing unfolds with such immaculate delicacy. Working class people who find themselves flung into the reserve army of labor, or onto the capitalist refuse heap of surplus humanity, tend instead to linger on and make a nuisance of themselves.

And it is at this point we can soon observe the coextensive nature of generic bourgeois ideology and its early 20th century offspring, full-blown fascism.

For while generic bourgeois "liberalism" can tolerate a certain modicum of charitable philanthropy for the "unfortunate", and can even profit from the cautionary tales they tell to workers prone to any flagging "work ethic" (ie, the dangers posed by any insufficiently ardent embrace of their subordinate social roles in surrendering their labor, and themselves, to the imperative of an adequate surplus value extraction for the maintenance of the master capitalist class), there are limits to such patience.

Lately, Philadelphia has become a prime example of this bourgeois "liberal" fascism. After many years of the current middle-of-the-road mayor pushing for "overdose prevention sites", and directing the City to sue (successfully!) the Trump administration's legal schemes to thwart their opening and operation, almost the entire political class at both the city and state levels has now lined up in opposition to them. Even Helen Gym, the progressive mayoral candidate endorsed by Bernie Sanders, who boldly endorsed then in 2017, has become markedly more muted on the subject, while the recently elected "resistance liberal" governor, Josh Shapiro, outright opposes them.

Astonishingly, in the entire United States, apparently, the only officially legally sanctioned overdose prevention sites remain limited exclusively to NYC! This despite a sterling safety track record in many other countries worldwide. But auch facts do not prevent Shapiro from harrumphing, "there's no safe way to inject yourself with this type of 'poison'!"

Shapiro is a rather well educated and accomplished man (magna cum laude at U Rochester, Georgetown Law grad, former state AG, etc), so it's unlikely that he is simply ill informed on this matter. Rather, a more parsimonious explanation is the simple fact that merely saving the lives of drug overdose victims can only frustrate option 3 above, and prolong the nuisance they pose for polite society.

The only real difference between a man like Shapiro and a man like Trump is that the latter is more blunt about his class's viciousness.

(Interestingly, it should be noted, the current deadly opioid epidemic is not an altogether historically unprecedented phenomenon. During the Prohibition era of the early 20th century, in fact, tens of thousands died from drinking moonshine - ie, alcohol tainted with methanol due to sloppy and improper amateur distillation techniques. See https://www.pastemagazine.com/drink/alcohol-history/prohibition-history-methanol-poisoning-bootlegging-alcohol)
