---
title: American Dreams and American Racism
author: guyberliner
type: post
date: 2021-06-28T00:40:40+00:00
url: /2021/06/28/2021-06-28-american-dreams-and-american-racism/
categories:
  - Uncategorized

---
It stands to reason that a society like that of the United States would be especially susceptible to racism. A country that elevates something it calls "The American Dream", which is, very simply, "to get ahead" (ie, to acquire more wealth and status) is going to have a problem in principle with equality. 

When a centerpiece of one's identity consists of achieving this particular kind of dream, then any claims seeking greater equality by any disadvantaged groups run a risk of being perceived as existential threats.

Perhaps "getting ahead" works as a durable identity for an entire society during times of "shared prosperity", when you are not so worried that somebody else "getting ahead" is going to negatively impact your own ability to do so. Even greater equality might be tolerable under such circumstances. But when capitalist crises intensify, then the bourgeoisie retrenches, hoards and redirects all capital flows upwards towards itself, to protect its own "getting ahead".

At such a moment, it would take almost a miracle to avoid a recrudescence of the worst forms of racism, even without powerful sociopaths deliberately stoking the flames.

As a matter of fact, I would venture to suggest that, the more intense the crises of capitalism become in such a society, the more importance will almost inevitably be attached to "psychological wages", ie, the principle that, "even if I'm not 'getting ahead' in absolute terms, at least I'm better off than the poor slobs over there." As long as somebody else has it worse than you, you can even console yourself about diminishing circumstances imposed by your superiors, whose decisions under capitalism escape the reach of any democratic appeals. 

One can predict immediately the ferocious backlash likely under such conditions to any new claims made by disadvantaged social groups.

It should also then be obvious the severe implications this holds for the prospects of many other more or less well meaning efforts. 

Take movements for reparations, the most notable of which these days has to be reparations for descendants of slaves. The conditions today are highly unfavorable for this movement in certain ways, because of the intensifying conditions of neoliberal austerity we face, despite "liberal" generational tendencies towards the subject of race relations.

Of course, the field of "corporate race relations" is the most fraught of all. Efforts by major corporations to project a "progressive" image for reputational purposes now help reactionaries supercharge their "Culture War" today, which plays every chord of "white grievance" about loss of "psychological wages" like a virtuoso. Especially because bourgeois-led efforts at "racial reconciliation", for reasons of class interest, can never afford to do anything that would embolden multiracial working class solidarity, or demands for redistribution from economic oligarchs.

Accordingly, we can predict that any very vigorous reparations movement, absent this last element, ie, divorced from a class analysis prioritizing multiracial working class solidarity, is bound to reap the same whirlwind that the reactionary "Culture War" has, albeit inadvertently, instead of deliberately, as in the latter case.

In passing, we can also note some other bizarre symptoms of this character pathology of US society. I find it particularly ironic, for example, that in a society notorious for excess of personal possessions and gross waste at every level, where even (especially!) very poor people are particularly susceptible to associated psychological dysfunctions such as "hoarding", a few innocent lines taken out of context from an article appearing in a publication by the World Economic Forum fanned remarkable hysteria. 

Conspiracy theories (eg, "the Great Reset", which is also taken from a banal coinage by WEF themselves), associated with certain random, out-of-context remarks have spread far and wide enough to provoke pointed refutations by quasi-professional rumor debunkers. Type "World Economic Forum says you will own nothing and be happy" into a search engine for details. (To make a long story short, the feverish reactions occasioned by this headline concerned nothing more than a very banal article published by a contributor to some Forum publication noting the growing proliferation of service contract based goods and services, eg, long-term leases, in preference to outright purchase and ownership.)

Clearly, we are talking about a society in which mania around acquisition of material possessions and status, together with extreme paranoia and fear of loss, make for an explosive combination.
