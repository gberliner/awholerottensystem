---
title: Capitalism and necrophilia
author: guyberliner
type: post
date: 2021-03-30T21:13:27+00:00
url: /2021/03/30/capitalism-and-necrophilia/
categories:
  - Uncategorized

---
 

Twentieth century psychologist Erich Fromm wrote about character &#8220;orientations&#8221; (or better, &#8220;deformations&#8221;) that, while rare under normal human conditions, can develop a terrifying frequency of occurrence under conditions of capitalism and modern industrialism. 

The most disturbing of these deformations Fromm named &#8220;necrophilous&#8221;. Under normal conditions, human beings, dependent as they are on the larger biosphere, have a natural preference for life and the living. But, under the highly novel conditions of an industrialized society that requires dedicated castes of workers who can be disciplined into narrow specializations, and particularly who rarely work with living things, but mostly only with mechanical, technical products of human ingenuity, a kind of character can become more common that actually favors the dead, the mechanical, the maximally predictable, and is actively hostile towards life and living things. 

Not surprisingly, he also made the connection to 20th century totalitarian political ideologies, especially fascism, but also cults of personality like Stalinism. A common fascist slogan originating in Spain with one of Franco&#8217;s generals was even, &#8220;Long live death!&#8221; 

If certain economic and social arrangements favor the frequency and spontaneous development of highly objectionable character traits, surely that becomes an independent factor according to which we must judge the expediency and morality of accepting those conditions. In an idealized fantasy of the world, we can imagine that human beings are possessed of something called a &#8220;free will&#8221; that is majestically independent of any and all environmental conditions, but there are abundant reasons to doubt that idealized conception of the world. 

(Parenthetically, we should not be dismissive the possibility and even likelihood of profound psychological influences resulting from our altered modern environment. Consider the physiological evidence alone: the precise cause remains unexplained, but average human body temperatures have plunged by an astonishing 1-2 degrees F (0.6-1 degrees C) worldwide (https://theconversation.com/peoples-bodies-now-run-cooler-than-normal-even-in-the-bolivian-amazon-148901), and there is ample evidence suggesting a connection with the promiscuous contamination of the environment with long-lived hormone-disrupting pollutants (https://www.sciencedirect.com/science/article/pii/S089062381630363X). So if previously unsuspected physiologic changes associated with modernity like this are possible, why should we doubt the possibility psychological ones as well?) 

As solutions, I think of ideas like Michael Albert&#8217;s and Robin Hahnel&#8217;s &#8220;parecon&#8221; and its &#8220;balanced job complexes&#8221;, where we actively encourage GENERALIZATION instead of narrow specialization, so that human beings can cultivate what Michael Lebowitz (with Marx) calls the &#8220;joint product&#8221; of work: in every form of work, &#8220;the worker ‘acts upon external nature and changes it, and in this way he simultaneously changes his own nature’ &#8220;