---
title: Capitalism produces the working class it needs
author: guyberliner
type: post
date: 2020-12-30T06:05:47+00:00
url: /2020/12/30/capitalism-produces-the-working-class-it-needs/
categories:
  - Uncategorized

---
We are accustomed to seeing a paradox in the principle that that which is harmful may often at the same time be that which is most attractive. But really there is no paradox. The most obviously and immediately lethal parasite is never the most dangerous one. It would kill its host too fast to spread.

The most dangerous adversary is always the one not immediately recognized as such. In that light, it is no longer any kind of mystery why people &#8220;vote against their own interests&#8221;, for example. But in the central contradiction of capitalism itself, that between the working class and the bourgeoisie, there is no paradox at all. Because the very concept presupposes that one can perceive a category called &#8220;one&#8217;s own interests&#8221; as being independent of and in contradiction with another&#8217;s. But what we call the &#8220;working class&#8221; or the &#8220;proletariat&#8221; does not even exist without a ruling class first creating it (albeit initially by a process of overt dispossession, which is always very bloody &#8212; aka, &#8220;primitive accumulation&#8221; in Marx&#8217;s terminology, but its resentful, subjugated masses never start as proletarians in the first place), but later by almost completely automatic and routine processes of reproduction.

To quote Michael Lebowitz, paraphrasing Marx, &#8220;capitalism produces the working class it needs&#8221;:<figure class="wp-block-pullquote">

> Is it likely, then, that people produced within capitalism can spontaneously grasp the nature of this destructive system? On the contrary, the inherent tendency of capital is to produce people who think that there is no alternative. Marx was clear that capital as it develops tends to produce the working class it needs, workers who treat capitalism as common sense. As he explained in Capital: “The advance of capitalist production develops a working class which by education, tradition and habit looks upon the requirements of that mode of production as self-evident natural laws. The organization of the capitalist process of production, once it is fully developed, breaks down all resistance.”11
> 
> <cite><a href="https://monthlyreview.org/2015/04/01/if-youre-so-smart-why-arent-you-rich/">https://monthlyreview.org/2015/04/01/if-youre-so-smart-why-arent-you-rich/</a></cite></figure>