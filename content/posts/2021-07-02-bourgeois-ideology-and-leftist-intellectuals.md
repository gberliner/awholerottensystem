---
title: Bourgeois ideology and the duties of leftist intellectuals
author: guyberliner
type: post
date: 2021-07-02T00:40:40+00:00
url: /2021/07/02/bourgeois-ideology-and-leftist-intellectuals/
categories:
  - Uncategorized

---
I believe it's the historical duty of leftist intellectuals, as the authentic heirs of the political Enlightenment, to root out, discredit, and debunk every vestige of filthy bourgeois ideology. But why use a term like "filthy", though? Isn't that excessively emotional, unbalanced, and vituperative? No, I believe it's entirely appropriate. 

Because, first of all, we are talking about an actual, precise, real phenomenon. There is a crucial ideological component behind why humanity is paradoxically seemingly paralyzed at a moment of both unprecedented threats to its survival, of extraordinarily recent vintage in the larger span of human history, while also simultaneously possessed of unprecedented technical capabilities, seemingly a match for any physical challenge.

Second of all, the bourgeois ideology I speak of is at once both very distinct from, but also a massive potentiator of certain inherent and destructive human potentials. Buddhism, one of the world's great wisdom traditions, referred to these as the "Three Poisons": greed, hate, and delusion. Others have used different names for them, no doubt. In Buddhism, these are also referred to as "mental defilements", once again evoking a connotation analogous to the word "filth". So we have the raw stuff of human nature, COMBINED with something very distinct and novel, something entirely contingent and NOT "innate".

It's also our duty to carry out this work if we are to have any hope, however faint, of approximating, even very roughly, the greatest possible dignity and freedom for the human individual. I have in mind something roughly like that devoutly to be wished for outcome which ethicist John Rawls referred to as the "maximin": the maximal expression of the human potential of every individual, combined with the minimum of completely unnecessary suffering on account of accidents of birth, ethnicity, nationality, the "genetic lottery", etc.

It's our duty to do so because, at present, this dominant ideology has the majority of the most active and capable portion of the population bound up and tightly rolled into a ball of fear and paralysis, the very condition that is swiftly leading us to oblivion in the face of mortal but entirely avoidable threats of our own creation. And ironically, it's precisely this fear and paralysis that is perhaps the best proof of Rawls's own hypothesis about his "Original Position": that people in it would choose such a social contract so different from our present one because people on average are highly risk averse.
