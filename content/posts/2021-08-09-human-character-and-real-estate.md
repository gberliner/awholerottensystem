---
title: Human character and real estate
author: guyberliner
type: post
date: 2021-08-09T00:40:40+00:00
url: /2021/08/09/human-character-and-real-estate/
categories:
  - Uncategorized

---
It's amazing how the mettle of human character miraculously and precipitously deteriorates so coincidentally whenever and wherever the rents skyrocket! It's almost as if land is a finite resource that God isn't making more of. Almost as if druggies who discreetly shoot up behind closed doors when they can afford the rents wind up shooting up outdoors when they no longer can. Almost as if wages don't rise by double digit percentage points year over year even though rents and mortgages do. Hmmm...

The rich sow chaos and destruction across our entire society, then sit back and tut-tut about the depravity of their social inferiors. And because the rich are the whole society's culture heroes, the classes below them are wont to imitate the imbecile incomprehension of the  heroes whose ranks they aspire to join.

The depravity of the whole society finally consists of an infernal hierarchy in which "each social class fearfully bows and scrapes before the superiors above them, while kicking and despising their inferiors beneath them whom they more closely resemble".
