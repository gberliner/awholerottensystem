---
title: Free speech vs freedom writ large
author: guyberliner
type: post
date: 2022-03-17T00:40:40+00:00
url: /2022/03/17/free-speech-vs-freedom-writ-large/
categories:
  - Uncategorized

---
I am generally sympathetic to the many arguments of ardent civil libertarians like Noam Chomsky, who take up the cause of defending the rights of unpopular people across the political spectrum. And yet, some of his and others' arguments lack historical depth and nuance, in my opinion, and also suffer from a kind of North American centrism that fails to reach Chomsky’s usual level of acute comparative national analysis.

Indeed, from the standpoint of civil liberties, arguably some of our biggest problems today stem from a marked historical *failure* to sufficiently crush the “civil liberties” of landed Southern aristocrats in the wake of the Civil War, along the lines of, say, the Allies and their denazification program of Germany after WW2, but for which the program of the original Reconstruction era may have been vastly more successful in bringing about a full political democracy in the United States, the legacy of whose failure still haunts us to this day.

Today, our courts go to fairly absurd and arbitrary lengths to protect what they deem to be the “free speech” of certain elements of the population, up to and including open threats of violence and racial genocide that would be tolerated in practically no other country in the world, adhering to the arbitrary distinctions enumerated in cases like Brandenburg v. Ohio (1969), according to which a threat is only legally actionable vs “protected speech” when it names a specific party, place, time, etc.

So I can yell, “let’s kill all people of such-and-such race!” to an agitated crowd, but I just can’t specify one particular member of that race. Or, maybe I can even name a specific person to kill, but provided they aren’t on the scene when I do so, and provided I don’t instruct the crowd on the precise time, place, and manner of doing so, I am also within my rights.

However, needless to say, although this kind of speech is tolerated by US courts, that does not prevent our police and security forces from engaging in their own highly selective harassment of members of what they consider to be “threat groups”, which are highly disproportionately likely to consist of minorities and left-leaning subversives. It’s on such grounds that Chomsky and others tend to vehemently oppose any restrictions on “hate speech”, and even approve of the wildly liberal treatment it continues to be afforded by courts in the wake of Brandenburg and similar cases.

Reasoning like this strikes me as shockingly shallow, though. Beyond the fact that, as others so often point out, regardless of these arguments about “slippery slopes”, authorities “still do it to us [the left] anyways!”, nevermind what the courts have to say about the matter, they also leave a gaping hole of crucial current and historical context out of consideration.

In particular, the coddling of reactionaries by (already strongly reactionary leaning) official repressive forces has historically created a larger atmosphere of intimidation in many places, one which still actively stifles dissenting voices to this day. And this unfolds in a context of barely concealed, official white supremacist policies, like Cold War “counterinsurgency doctrine” which, as historian Kathleen Belew has pointed out, amount to the lineal progenitors of modern, *informal, unofficial* white supremacist terrorism networks.

For example, the (barely noted in national news headlines) 2019 arson attack against buildings at Tennessee’s Highlander Center (the training grounds for generations of civil rights leaders and environmental, labor, and other activists, past and present, including Martin Luther King), which was accompanied by white power graffiti sprayed nearby, recalls some of the worst incidents of white supremacist terrorism in the late Jim Crow era.

Belew details in her book, “Bring the War Home” (2018), how today’s violent rightwing extremists cut their teeth as veterans of imperial US hot wars during the nominal “Cold War”, from Central America to Vietnam, in the latter of which, General Westmoreland, “ComusMACV” (Commander, US Military Assistance Command, Vietnam) explained that “human life is cheap among the Oriental races”, making it sometimes sadly necessary to “destroy a village in order to save it” (from the Commies, of course, who in turn had to be fought and killed “over there”, so we don’t have to do it “over here”).

Language routinely used today by white gun vigilantes eerily echos exactly Westmoreland’s kind of twisted logic to explain why they feel compelled to show up at anti-police brutality marches in cities they don’t even live in, to “protect” people. Because “life is cheap in the ghetto,” and they keep “killing each other in ‘Black-on-Black’ violence”. And also so that the participants are kept in check and don’t “invade” *their* neighborhoods. As such, a Kyle Rittenhouse should be viewed as essentially a direct descendant of the barely pubescent boys who were sent to Vietnam with M16s and zippo lighters to burn down and destroy the villages of dirt poor peasants in that country.

Indeed, official state ideology of the US during the Cold War and after, which has consistently glorified imperialist violence worldwide in the name of “protecting freedom”, has become a kind of recruitment propaganda for voluntarist rightwing extremism, whether deliberately or accidentally, in a similar fashion to the way in which Saudi Arabia’s official Wahabist ideology of Islamic fundamentalism has sown the seeds of violent Salafist extremism worldwide.

The whole question of what really defines "free speech" bleeds right into arguably far more momentous questions, like whether "money is speech". Historically "liberal" groups like the ACLU have taken highly ambiguous stances on these issues, lacking as they do any class or power analyses. In all these cases, though, a common thread is discernible: members of dominant social groups like to hide behind a façade of free speech claims in order to intimidate and suppress other groups and opinions.

Today, we can see the spectacle of an openly white nationalist cable TV host of one of the most widely watched national shows inciting his audience to pick fights on the street with anybody wearing a mask during a global pandemic. We can see billionaire political donors openly engaged in what amounts to clear political bribery, also under the doctrine of "free speech". We can watch "reality tv" shows that literally glorify cops methodically violating the rights of criminal suspects. We can see so many shocking spectacles that only a few years ago would read like something out of a Ray Bradbury novel, all under the guise of "free speech".

Maybe some of these abuses are legally actionable. Others, although clear threats to any kind of civilized society, may only be suppressed through a combination of public pressure and indirect government actions. But any way you look at it, we have to face the fact that speech can have dire consequences, especially when it's calculated to sow chaos and destruction, or to intimidate non-dominant groups -- even when it falls within legally permitted boundaries.

Whether any or all of this could have been avoided had the United States ever completed its own post-Civil War counterpart of Germany’s aggressive “denazification” program remains a big, open question. And what implications it should have towards our attitudes today vis a vis the bizarrely liberal albeit highly selective construction of “free speech” doctrines by US courts past and present should also be a much more vigorously and intelligently debated question, but one which remains mostly ignored by intellectuals like Chomsky today.
https://www.hup.harvard.edu/catalog.php?isbn=9780674286078
