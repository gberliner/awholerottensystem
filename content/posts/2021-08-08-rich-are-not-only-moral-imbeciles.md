---
title: The Rich are not Only Moral Imbeciles...
author: guyberliner
type: post
date: 2021-08-08T00:40:40+00:00
url: /2021/08/08/the-rich-are-not-only-moral-imbeciles/
categories:
  - Uncategorized

---
In my last post, I asserted that ["the rich are moral imbeciles"](/2021/08/05/the-rich-are-moral-imbeciles/). But a far more consequential assertion, and one that really ultimately requires far more documentation and extended argumentation, is that their overall ideological and practical project depends crucially on reducing the rest of us to their own debased level.

I don't know how many book length investigations there are of this reality, but it could be said that the entire "neoliberal" order consists of an unending demonstration of it. The gutting of the public sector, the pervasive infiltration of billionaire funded charitable foundations into every remaining public and NGO organization (for example, the increasing intrusion of Bill Gates's tentacles into all facets of international public health), the surrender of academia to a bourgeois administrative élite obsessed with commercial research "partnerships", the financialization of the entire economy and the ever intensifying commodification and exploitation of everything in sight, are all examples of factors driving in this direction.

When at least half the population is engaged in "service sector" jobs, and most of the demand for "services" is concentrated in the dwindling subsegment of the population who are not under the gun of precarity, then most of us are forced to become suckups to the bourgeoisie for our very survival. The relentless and vicious assault on unions, and the intensifying siege on the public sector, which is now left as the only portion of the domestic economy still heavily unionized, is also part and parcel of this.

It helps the bourgeois ideological project immensely that practically no "do-gooder" in the NGO world can escape accusations, cynical or not, of being "sellouts". Bourgeois financialization and monopolization of every facet of contemporary life makes it almost impossible for anybody to escape either working for them or paying extortionary tribute to them in one way or another. 

The covid crisis is further exacerbating this preexisting crisis, by starving petty bourgeois elements of income. The shuttering of the economy during this period, together with the altogether inadequate PPP programs which have been heavily siphoned off by big bourgeois interests, has left small businesses, including small landlords, increasingly vulnerable to being absorbed altogether by massive private equity and other big bourgeois entities.

The usual far right and reactionary elements have masterfully exploited these phenomena. Even if, say, a Colin Kaepernick has made massive personal sacrifices to protest for the cause of civil rights and racial equality, they can point to something like his deal with Nike and say, "look, see, it was all a big grift!" And whatever the motivations of an Alicia Garza might be, nobody in her position could likely escape similar accusations, however pure their intentions.

The eclipse of an egalitarian leftist horizon makes some form of "selling out" a seemingly natural inevitability. Pascal Robert, Stefan Bertram-Lee, et al, discussed these bleak conditions with rather acute insight on a recent installment of the podcast, ["This is Revolution"](https://www.youtube.com/watch?v=pWLLAod5Mcg).

Ironically, the first victims of this phenomenon of "fighting a pig means rolling around in their filth, but the pig likes it" were the [original "neoliberal thought collective"](/2021/07/15/ironies-of-mont-pèlerin/) themselves. It has to be one of the greatest and least appreciated or understood ironies of 20th century liberal thought that the founders of "neoliberalism" would find what is now called by that name practically unrecognizable. But from the very beginning, the signs were apparent of the likely direction their movement would take, given their funding sources. (Not to say that the original neoliberals were conscious of "fighting" the bourgeoisie, obviously, but only that they were jealous of their intellectual independence, which they prized above all else, and even downright suspicious of their rich backers at times, at least early on.)
