---
title: An American Chartism
author: guyberliner
type: post
date: 2022-08-03T00:40:40+00:00
url: /2022/08/03/an-american-chartism/
categories:
  - Uncategorized

---
Reading radical British historian EP Thompson’s “Making of the English Working Class” recently, the shocking parallels between 19th century Britain and 20th and 21stcenturies United States deeply struck me. In particular, the extraordinary challenges faced in both places by proponents of both majoritarian political democracy as well as the interests of the vast, working class majority stood out for me.

Thompson emphasizes how inextricably linked the decades long struggle in England for both labor rights and political rights always were. And, even before universal male suffrage was won, the country first had to overcome one of its paramount antidemocratic obstacles, the problem of “rotten boroughs”. These were parliamentary constituencies which, due to the lack of any fair and timely redistricting process, often contained absurdly small numbers of actual constituents, thereby giving a tiny, land-owning aristocracy an absurd stranglehold over political power. Sound familiar?

The premier constitutional reform movement that fought and finally won basic political rights for masses of the English population was called “Chartism”, although initially it was regarded by British rulers as a mortal, subversive threat to national security, and was savagely repressed right up to the 1830s and 40s. But most remarkably, this movement was embraced from their beginnings by the earliest trade unions and working class radicals, and regarded as indispensable by both to their long term prospects of success. These visionaries took up a long fight, facing shocking and often brazenly illegal brutality at the hands of the state (as well as state-sanctioned gangs, aka “Church-and-King mobs”) for generations, before finally and decisively winning their goals.

In the United States, our version of “rotten boroughs” is perhaps most clearly recognizable in the form of the US Senate, which has not changed its size or apportionment in over two hundred years, and which becomes more hostile to majority rule with every passing year, as population becomes ever more concentrated in a small number of urbanized regions. The institutional obstacle of the Senate alone constitutes a mortal threat to any realistic prospects of majoritarian democracy in the US, let alone dramatically expanding rights and improving conditions for working class people. So is it time the United States started to develop its own counterpart to Chartism?

If so, it’s essential here, too, like the English Chartists did, to take a long view. We should start with clearly understanding both the obstacles and opportunities. Obstacle number one is embedded in our current understanding of the Constitution itself. Because the same forces that make the Senate more and more undemocratic with every passing year, also erect more and more insuperable obstacles to passing constitutional amendments that could mitigate the problem. Here, too, we are relying on a process that puts more and more minority power in the hands of the lowest population density regions of the country, whose representatives have the strongest disincentives to permitting any such reforms.

Instead, though, we should develop a better understanding of the built-in conflicts within the Constitution itself. Notably, the 14thAmendment guarantees “equal protection under law”. And yet, as the viral exchange in the Senate between the new Amazon Labor Union (ALU) President Christian Smalls and Senator Lindsey Graham demonstrated, we are effectively confronted by the absurdity of Jefferson Davis (or his heirs, at least) legislating the workplace rights of majority non-white workers in New York City!

Accordingly, there is a powerful argument to be made that the current composition of the Senate directly violates our 14th Amendment rights. And, although the Constitution (Article 1, Section 3, as amended by the 17th Amendment) states that “The Senate of the United States shall be composed of two Senators from each State, elected by the people thereof, for six years; and each Senator shall have one vote.”, it does not explicitly say “composed solely of”.

Another remarkable parallel that comes up constantly in Thompson's book is England's own version of "exceptionalism" which, much like "American exceptionalism" rests on a notion of a certain moral superiority conferred on it by its enduring "constitutional order", which supposedly guaranteed a quality of freedom and individual rights unique in Europe. Even "Church-and-King" mobs instigated to harass and kill radicals espoused this form of patriotic "constitutional" reverence.

But from Chartism's beginnings, there was an ever present tension, Thompson explains, between a very conservative view of "constitutionalism" (what we call "strict [constitutional] construction" in the United States today), and a broader view of "Rights of Man", as espoused most notably by Thomas Paine.

Remarkably, Paine, an American of English birth, functioned in England as a sort of Noam Chomsky of his day, a radical dissident intellectual par excellence. And his vindication of civil and democratic rights was a shocking rebuke to the conservatism of the "Engish exceptionalism" of his day.

For generations, Paine's views on monarchy, religion, and civil society excited the leading radicals and horrified and scandalized the leading establishment authorities of Britain.

Today, the United States needs a "Painite" renewal of its commitments to majoritarian democracy, one that, much like Paine's original model, challenges received truths about "constitutionalism" and does not shy away from tackling the baked-in, antidemocratic defects of our existing constitutional order head on, just as Paine did those of his time.

US Chartists, like their earlier British counterparts, should start with the constitutional basis we have to realistically work with. We can and should debate the feasibility of the amendment process, but we should not shy away from more radical arguments, either. Given the 14thAmendment, and the fact the 17thAmendment sets out no explicit limitation to the size of the Senate, there is a constitutional argument to be made for adding to 17thAmendment’s Senate composition a further 14thAmendment contingent.

By a simple majority (obviously sans filibuster!), a “14thAmendment Restoration Act” could be passed, adding another one hundred Senators to the composition set out by the 17thAmendment. But unlike the 17thAmendment, these “14thAmendment senators” would be apportioned among the states BY POPULATION. This measure would substantially mitigate our “rotten boroughs” problem today.

But, as we have seen more clearly than ever recently, our courts are often even more hostile to majoritarian democratic rights than our legislatures. Fortunately though, here too, the existing Consttution already offers us realistic potential remedies, notably, Article 3, Section 2.

Article 3, Section 2, provides that the jurisdiction of federal courts is limited by "such exceptions, and under such regulations as the Congress shall make." (cf: https://en.wikipedia.org/.../Article_Three_of_the_United...and https://en.wikipedia.org/wiki/Jurisdiction_stripping ). Accordingly, a clause should be included in any “14thAmendment Restoration Act” precluding challenges to it in federal courts, which, as we have clearly seen recently, are already themselves grievously tainted by the antidemocratic structures of the very Senate that this Act is meant to rectify, and without such a provision would therefore predictably create their own insuperable obstacles to its implementation.

(Interestingly, a clear and high profile precedent for the use of Article 3, Section 2 in this way already exists and remains relevant to everyday life in this country. Notably, the 1935 National Labor Relations Act strips federal courts of the power to hear cases brought under it, except such as are explicitly referred to them by the National Labor Relations Board itself.)

Obviously, like British Chartism, a project like this cannot happen overnight. Only by visionary organizing and education is success possible. But what are we waiting for? If we don’t start now, then when??
