---
title: The Merit Pecking Order
author: guyberliner
type: post
date: 2021-07-21T00:40:40+00:00
url: /2021/07/21/merit-pecking-order/
categories:
  - Uncategorized

---
The bourgeoisie (aka, "capitalist class"), are a tiny minority of society (between one and ten percent, depending on how you count the intermediate "coordinators" or "professional managers", who may control and manage the assets of the infamous "one percent", but without directly owning many of them). Nevertheless, they manage to impose a ruthless pecking order on the rest of society, based on supposed "merit".

This serves a double ideological purpose:

One, it obviously is meant to justify their deathgrip over the rest of us, and the resources we all need to survive, in exchange for access to which they are able to extract crushing rents from the rest of us.

Two, it ushers in the bourgeois "war of all-against-all", plunging the rest of us into an endless blame game, a most crucial distraction, that ensures that most of us are always "punching down" (or laterally), instead of noticing the antics of those on top and combining forces to emancipate ourselves from them forever.

Almost any kind of antisocial behavior, prejudice, or vileness always has a ready made cloak to disguise itself, thanks to the bourgeoisie and its ruthless ideology of "meritocracy". So no matter how hard it tries at times at putting on enlightened appearances via its rhetoric on such matters as "identity politics", it alone bears the lion's share of guilt for all the crimes of fascism, racism, and social violence. 

Of course this is not to say that but for them society would be magically free of all vices, yet how else can one account for the combination of extreme inequities and extreme social violence, unique in the industrialized world, to be found in a society like the United States, of all places the bourgeosie's metaphorical and literal planetary "Fort Knox"? If their ideology really led to peace and harmony, instead of its polar opposite, we would surely see signs of it by now.
