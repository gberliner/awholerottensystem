---
title: Int’l maritime law vs “lifeboat economics”
author: guyberliner
type: post
date: 2021-04-08T01:37:47+00:00
url: /2021/04/08/intl-maritime-law-vs-lifeboat-economics/
categories:
  - Uncategorized

---
If we applied the literal equivalent of international maritime law to the climate crisis, there would be no &#8220;humanitarian crisis at the border&#8221;. In maritime law, the nearest functional vessel that can do so is required to rescue the passengers of any nearby vessel in distress. 

What we are doing at present, though, is something akin to the racist eugenicist Garrett Hardin&#8217;s &#8220;lifeboat economics&#8221; instead, or what I call &#8220;the application of the principle of Lebensraum to the planetary carbon cycle&#8221; (ie, ringing the planet&#8217;s rapidly dwindling habitable real estate round with razor wire, against the pleas of people in lower lying and less favored areas, whose metaphorical &#8212; and sometimes literal &#8212; boats are literally being &#8220;swamped&#8221;), thus forcing the Earth&#8217;s carbon budget to be balanced on the backs of those who contributed the least to triggering the crisis in the first place, all so that its prime movers and beneficiaries can avoid making any mildly inconvenient changes to their lavish lifestyles. 

We should emphasize the word &#8220;mild&#8221; here, because in contrast to what refugees are having to go through, the changes that are demanded of us are exceedingly modest in comparison. It is estimated that bringing our emissions into balance with the planet&#8217;s remaining carbon sinks would require reducing average emissions down to approximately three metric tonnes CO2 per person per year, the equivalent of the level of the European nation of Slovenia (ie, far less profligate than the USA at present, but also far from a paleolithic level of resource consumption). (https://blogs.scientificamerican.com/eye-of-the-storm/fifth-straight-year-of-central-american-drought-helping-drive-migration/)