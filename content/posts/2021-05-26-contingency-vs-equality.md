---
title: Contingency vs Equality
author: guyberliner
type: post
date: 2021-05-26T20:42:51+00:00
url: /2021/05/26/contingency-vs-equality/
categories:
  - Uncategorized

---
While one&#8217;s personal efforts are important for many reasons, the overwhelming bulk of sociological data demonstrates that one&#8217;s &#8220;station in life&#8221; in most cases is determined, within certain broad limits, by contingent historical and personal circumstances.

Maybe that sounds fatalistic. Maybe it&#8217;s distasteful to hear that for most people, whether they are ambitious and hoping to &#8220;get ahead&#8221;, or are happy about where they are and basking in pride about it. Either way, that doesn&#8217;t make it untrue.

Again, that&#8217;s not to say that you cannot achieve anything through your own efforts. But it DOES mean that, notwithstanding the occasional Horatio Alger unicorn story, you aren&#8217;t going to become the next Bill Gates if your mom and dad don&#8217;t already have a net worth in excess of a few tens of millions of dollars. You aren&#8217;t going to become the next Senator from state X if you did not go to Harvard and don&#8217;t own a rolodex of highrollers you can &#8220;dial for dollars&#8221;, or are not already a member of an existing family political dynasty, etc, etc.

Why? Because the real differences between human beings are too comparatively tiny to account for any other way. For example, while one person may be &#8220;twice as smart&#8221; as another, say, or twice as good at skill X, Y, or Z, they are never one hundred times, or one thousand times, etc, etc. Only contingent circumstances and historical accidents can account for such vast multiplier effects.

So, for another example, it appears like the Israelis right now have the upper hand over Palestinians. But it&#8217;s not because Jews are ten thousand times smarter, or harder working, or more rational, or any such nonsense like that.

It&#8217;s important we understand such things, so as not to delude ourselves, or others. And it&#8217;s important for the sake of building a better, more just, and more beautiful world.