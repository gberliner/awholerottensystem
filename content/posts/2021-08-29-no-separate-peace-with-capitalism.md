---
title: No separate peace is possible with capitalism
author: guyberliner
type: post
date: 2021-08-29T00:40:40+00:00
url: /2021/08/29/no-separate-peace-with-capitalism/
categories:
  - Uncategorized

---
A lot of people dream of "being their own boss", starting a modest business, "making a separate peace" with capitalism-blood-red-in-tooth-and-claw, as it were. If that's you, I hate to break it to you, but...

You need more than "smarts" to secure a living wage under capitalism. You need a convergence of conditions which capitalism tends to make increasingly scarce. Watch [this video](https://www.youtube.com/watch?v=iU4edZSRhfI) and others by David Harvey and other Marxist thinkers, and you will start to see more of the problem. 

The competition Harvey describes here tends to force down the rate of profits inexorably. In fact, under the conditions of an ideal "efficient market hypothesis", where supply and demand reach perfect equilibrium, no capitalist accumulation or profits are even possible anymore. Naturally, though, those conditions are idealized, and crises intervene before long that break them apart.

In the meantime, though, there are other factors that also suppress profit rates. The laws of atmospheric physics and chemistry are currently impinging on them, for example. And the profitably recoverable world reserves of fossil fuels are dwindling and also set to become another limiting factor. The net return on investment in exploration and extraction of these energy sources, for example, has plummeted, as measured in joules of energy invested to joules obtained, any much touted innovations in "hydrofracking", et al notwithstanding.

So the profitability of industrial enterprises has been dwindling for decades. But that does not mean the appetites of capitalists to continue living in the style to which they have become accustomed also adjusts downwards! No, instead they look for other ways to prop their returns up. Those now mainly consist of converting all of society into an endless array of profitable rent collection points. Rents of all forms (whether literal, such as when Bill Gates buys up farm land and rents it out to aspiring young farmers), or based on service monopolies, such as when US based Big Insurance/Big Pharma pays off Boris Johnson and the Tories to sell off more and more bits of the British NHS to no-bid "contractors".

Now imagine you find a comfortable niche for yourself, selling artisanal cheeses or candles, let's say, at your local farmer's market. Or making furniture from very nice, green and sustainable materials, maybe salvaged or something. You charge a bit of a premium but can make a living because enough people are still bumping elbows at those local markets who can afford it. But as Bill Gates and his fellow big bourgeoisie watch their returns decrease, they keep buying up more and more land and other natural monopolies, as backdoor ways of double-dipping and extracting surplus value they can't get anymore out of their first pass (ie, production). What do you think is going to happen, then, to the disposable incomes of all the middle income people buying your artisanal goods and services? That's right: they are going to get squeezed. They are going to be forced to buy from cheaper, less ethical sources, or not at all.

If so, you are going to learn sooner or later that there are no individualist solutions to our collective problems.

