---
title: The Conventionist
author: guyberliner
type: post
date: 2022-10-11T00:40:40+00:00
url: /2022/10/11/the-conventionist/
categories:
  - Uncategorized

---
> "I insist", continued the Conventionist G. "You mentioned Louis XVII. Let's understand each other. Are we weeping for ALL the innocents, ALL the martyrs, ALL the children, the ones from down below just like the ones from on high? Because *I* am. But now then, as I said, in that case we have to go back BEFORE 1793, and our tears must start BEFORE Louis XVII. And then I will weep with you for the children of kings, provided you will weep with me for the children of the common people."

The Conventionist's sermon to the Bishop in *Les Misérables* is one of the more striking dialogues in Hugo's novel. What gives with the stunning role reversal?? How can Hugo put the latter, this holy man, whom he obviously admires for his sincere devotion to the poor, in the position of being lectured to by a mere profane, washed-up politician??

But the Conventionist clearly bests the Bishop in this argument, pointing out a blindspot that the latter has scarcely considered.

The same blindspot arises in many, many contexts. Call it "survivorship bias". It's a key factor in the success of rightwing propaganda, a key weapon in perpetuating the license of the opulent few, and a key obstacle in securing the needs of the humble many against them. 

"We are many, they are few", true. But the few have a deathgrip on the world's attention. "All the world's a stage" -- for THEM. But for the rest of us, all the world's a vale of untold sorrows and "quiet desperation"

We don't know and can't care about what we don't hear about. There's a basic asymmetry between "the many and the few", and a superficially paradoxical moral advantage enjoyed by the latter over the former!

And the greater that inequality becomes, the stronger the systematic force it exerts -- like the force of gravity -- in this moral economy favoring the opulent few over the humble many. Shake a jar half full of marbles, and no matter how hard you do it, no matter how many marbles hit the lid on the top, still and all you find that more of the marbles, subject to the force of gravity, always cluster closer to the BOTTOM of the jar than the top. And the inverse applies for the force of wealth and the concentration of the world's economy of moral attention. The few at the top enjoy the majority of the world's attention. The many clustered at the bottom enjoy the least.

European imperialists sometimes called the colonized peoples they conquered "people without history". Whether the latter ACTUALLY were "without history" is beside the point, though. The point is that the latter being DEPRIVED of "history", being unnamed and unsong, ensured the colonists their sought-after ideological victory over the colonized in the world's economy of moral attention.

All things considered, a healthy suspicion is called for, whenever we are called upon to heap uncritical praise on "the successful". Not on "success" itself, mind you. But we have to always ask, did the apparent "success" come quietly at someone else's expense? Asking questions does not automatically imply a slight to the "successful". And, given a realistic understanding of our world as it is, we should demand that the "successful" indulge this suspicion willingly. 

After all, "success" at someone else's unjust expense should be scorned by the truly virtuous. Because surely an attitude of jealousy is no less unbecoming of the high and mighty than of the humble, and if they are really endowed with the moral virtues they fancy, the former will have no problems with honest scrutiny.
