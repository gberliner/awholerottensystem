---
title: A diffraction grating view of the world
author: guyberliner
type: post
date: 2022-10-20T00:40:40+00:00
url: /2022/10/20/a-diffraction-grating-view-of-the-world/
categories:
  - Uncategorized

---
USAnians view the rest of the world through a very narrow slit -- often even those who indulge in a certain amount of travel, whether for business or leisure. And like a diffraction grating, the light that passes back through presents a very distorted picture of the objects it reflects back to the viewer.

A recent debater discussing proposed reforms of Portland's form of government admitted he had "never heard of proportional representation" (even though it has featured most prominently in high-minded debates for at least the past seventy five years, surrounding the defects of the form of democracy prevailing in the United States, since long before "Citizens United"). 

But this is not even slightly surprising. Because, first of all, "Debates surrounding the defects of the form of democracy prevailing in the United States", however vigorous OR high-minded they may ever be, are already in themselves the most marginal of public spectacles, generally. Then, to compound matters, the finer points surrounding any COMPARATIVE political DIFFERENCES between this country and the rest of the world are a marginal subtopic on TOP of a marginal subtopic! A fraction of a fraction of a percent.

