---
title: "Bernie's \"corruption\" problem"
author: guyberliner
type: post
date: 2022-10-04T00:40:40+00:00
url: /2022/10/04/bernies-corruption-problem/
categories:
  - Uncategorized

---
I hear critics of Bernie Sanders often complain about his  unwillingness to sufficiently stridently denounce “corruption” among his  opponents. Obviously this was a notable complaint against his conduct  of his primary campaign against Joe Biden. But it has also come up in  the context of Senators Manchin and Sinema and their sublimely obstinate  obstruction against Biden’s “Build Back Better” bill.

Bernie’s  own temperament of being “classy” and avoiding personal attacks, in  favor of appealing to reason and the greater good, is certainly a  factor. But Bernie has undoubtedly also made an informed calculation,  well grounded in statistical facts. The fact is, “negative campaigning”  suppresses turnout and voter participation. This is borne out time and time again. And when your entire political strategy revolves around  activating “low-propensity” voters, that is a definite no-no.

But  there’s another, deeper consideration. On the one hand, of course,  “negative campaigning” simply escalates a kind of political arms race, a  “he-said-(s)he-said” cycle, which merely amplifies public confusion, and in most cases, a state of confusion which dominant media companies rarely show the slightest interest in reducing.

What’s more, though, is  that these media companies themselves, being indissolubly wed to the larger animal of  corporate capitalism, the very beast out of which political corruption emerges, are active enablers of the corruption and confusion. And so it’s by no means obvious that simply yelling, “you’re corrupt!” amidst  the corporate media din – even if your words are heard loud and clear –  leads to any public enlightenment. Rather, public enlightenment requires  capturing a big picture view of the forest and not just individual trees, seen only one at a time.

A kind of childish “he-said-(s)he-said” game is not merely fodder for Twitter trolls, either. This kind of obstacle to public understanding comes up even when  interacting with normal people, while canvassing voters in neighborhoods, say.

On one extreme, a Twitter troll might allude to some vast,  unspecified conspiracy of a mysterious Big Granola/Big Windmills  alliance, supposedly buying out and corrupting all academic climate and  earth science faculty worldwide, to the detriment of innocent, patriotic  oil billionaires.

But although such extremes of ridiculousness  might be limited to Internet trolls, at the other end, a middle aged or  older voter met at their doorstep by a canvasser might respond with a  shrug when you tell them that candidate X takes corporate PAC money and  candidate Y doesn’t, and that you are worried that the climate crisis  that ensues as a result will kill us all. 

“Sure, but one way or another,  those companies will buy out everybody. Sure, you CLAIM candidate Y is  some kind of miraculous exception, but how can I really know that??! Oh  well, what can you do?! But in the meantime, tell me about what  candidate Y is going to do about all these drug dealers and gangbangers  that candidate X promises to get much tougher on?”, my voter replies.

My hypothetical  voter-at-their-doorstep is based on real life experience. These voters  are not necessarily at all indifferent to such problems. 

The particular  voter I have in mind professed a lifetime of membership in environmental  organizations, attendance of rallies, etc, and I have no reason to  disbelieve him. But he had obviously given up on any expectation that  doing practically anything to forestall climate apocalypse was really  rational or feasible (at least not at the political level), whereas locking up people committing crime and  making his neighborhood less safe seemed eminently feasible to him. And these are not necessarily entirely irrational evaluations either, under present  circumstances.
