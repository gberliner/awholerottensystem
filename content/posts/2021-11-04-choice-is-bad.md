---
title: Choice is bad
author: guyberliner
type: post
date: 2021-11-02T00:40:40+00:00
url: /2021/11/02/choice-is-bad/
categories:
  - Uncategorized

---
"Choice" is very often bad, actually.

In the secular religion of consumer capitalism, however, such a statement, on the face of it, will come across as the most obscene sort of profanity. "Choice is good!" we are constantly told, with evangelical fervor.

By definition, in the political sphere, in theory, we are meant to make choices aggregated into collective decisions that in principle should serve the greater good. But, in practice, when choice operates on a disaggregated, amoebic, indivual consumer level, the results are often highly perverse and deleterious to society as a whole, whether those disaggregated, amoebic choices are being made in the economic marketplace, or in the so-called political "marketplace of ideas". 

People acting as purely private, self-serving individuals tend to make very different choices than they do when called upon, in public, to make choices in service of a larger purpose than just themselves, especially when everybody else can see the choices they are making.

In practice, we have seen that "consumer choice" as a model for human behavior, when applied to the political sphere, tends to degenerate into "anti-politics", a kind of political devolution dominated by bourgeois cults of personality, careerism, and political "entrepreneurship" -- ie, "politics as business enterprise" -- largely indistinguishable from the operation of all the rest of capitalism.

One of the most important flaws in the existing constitution of the United States, we often hear, is a lack of any proportional representation. But we don't often pick apart all the many components of that defect. Of course, we understand the immediate, obvious consequence that it neuters third parties. But another consequence we seldom tend to recognize is, it also neuters ANY form of collective (ie, "party") political organization altogether. For what is a party but an organized political bloc? And how can a party realistically operate and impose discipline on its elected members, when they all run as disaggregated, individual actors?

Even without strictly proportional representation per se, one of the implicit mechanisms required to implement proportional representation, which is running elections in which all or many seats in any jurisdiction are always up for grabs at once, is of central importance to any proper "party politics". Only when large numbers of seats are all up for grabs at once can a party efficiently operate, by running slates of candidates. Otherwise, when elections run in a completely balkanized fashion, with numerous staggered terms, and numerous "off-cycle" elections all the time, every individual candidate is each basically FORCED to operate as their own political "enterprise of one", and cannot depend exclusively or even primarily on any political party for their support and funding.

In such a balkanized structure, parties always degenerate into little more than loose-knit fundraising networks, certainly largely incapable of enforcing any discipline on individual elected members, for the simple fact that those members are not strongly dependent on a central party structure, and CANNOT be.

For this reason, the United States has NEVER had anything like a form of "party politics" that would be recognizable in other modern democracies. Even before the all-out "financialization" of US politics in the wake of a series of bad court decisions, starting in the early 1970s, and culminating in the infamous "Citizens United", the basic constraints this country has operated under have never been promising for the sake of a robust majoritarian political democracy.

Nowadays, we hear an incessant mantra about the importance of "small dollar donations", as a desperate kind of rear-guard action hoping to return us to some more innocent, halcyon, pre-1970s Buckley v Valeo days. But even were we to achieve that, it would amount to applying a tourniquet to a bullet wound where gangrene has already set in.

What we really need is to prepare for a fundamental rupture, because business as usual is becoming increasingly unthinkable. But what can we do as individuals when, ironically, the whole problem we are up against requires COLLECTIVE action??

How about actually putting our favorite slogan, "get organized!" into the most practical possible form?  The first thing that I humbly suggest is, STOP BEHAVING AS AN AMOEBIC INDIVIDUAL. Stop giving any money at all to individual politicians. Give money only to organized collectives, who can in turn choose to aggregate those funds to support candidates whom they believe they can bring to office to support their goals.

I don't know how to bring this to fruition, since it seems to run counter to the conventional wisdom about "small dollar donations" we are all having drilled into us. Nonetheless, what if "small dollar donations" are as much a part of the larger problem of capitalist anti-politics as "large dollar donations"?

I personally am of a mind henceforth, though, to respond to all entreaties by candidates seeking my financial support with, "I don't ever give money to individual candidates, but can you tell me about any organizations you have received or sought support from, whom I might consider supporting in turn?"
