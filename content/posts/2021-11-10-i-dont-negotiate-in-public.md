---
title: I dont negotiate in public
author: guyberliner
type: post
date: 2021-11-10T00:40:40+00:00
url: /2021/11/10/i-dont-negotiate-in-public/
categories:
  - Uncategorized

---
"I don't negotiate in public", said Arizona Senator Kirsten Sinema. 

Who else says that? Union-busting bosses say it during contract negotiations, that's who.

Of course the bosses (and their proxies) "don't negotiate in public". They want to apply maximum pressure, psychological and otherwise, to their counterparts in any negotiations, by cutting them off from their bases of support, the mass of people whom the latter are standing in for, the same way that bosses and rulers have always done, to break the solidarity of the many they seek to divide and rule.

If we let them get away with THAT, then they will win, every single time. Once they set the terms of the confrontation that way, most of their work is already done in advance, because they have succeeded in rigging the card deck in their favor.

Now imagine, on the other hand, a bargaining team that insists on "big, open negotiations". In those, OUR negotiators are NEVER alone in the room with the boss and his surrogates! Can you see how pivotal that can become in the outcome?

And we can and should translate that strategy to every context possible. Congressional aides, for example, don't materialize out of the ether. Working class politicians have to conduct class struggle on our behalf INSIDE the plush official chambers of power. 

They cannot and must not be alone in those rooms, either. Their aides can and must be people who serve dual roles, people who have emerged from class struggle organizations themselves, and continue to maintain loyalty to those organizations, not solely to their nominal legislator "bosses". They have to be there in those exalted offices of the powerful conducting those "big, open negotiations" on our behalf.

We cannot expect different outcomes without different ingredients. You could elect the noblest, greatest political geniuses in human history, but six of them in a room otherwise full of hundreds of rich sociopaths are not going to produce a different result.

(It also immediately follows logically from this, obviously, that "legislative aide" to a class struggle elected leader cannot be allowed to degenerate into a mere "career path". A working class "party" -- or party-like organization, that enforces discipline on its elected members -- would require those members to deliberately rotate aides after fixed lengths of service, so that the latter did not become too comfortable in their positions, and start to identify with the powerful and the institutions they have colonized via money and connections, instead of the constituencies they started off working for.)
