---
title: Changing reality
author: guyberliner
type: post
date: 2022-03-22T00:40:40+00:00
url: /2022/03/22/changing-reality/
categories:
  - Uncategorized

---
I had a conversation recently with a friend about what seems like the now perennial (though actually only very recently vastly metastasized) housing crisis in US cities, that reminded me of this little cartoon. My friend rehashed the usual arguments about how "the homeless don't care about all your ideas for earning a little income, or getting off the streets, or what have you. And even if they did, for 90% of them, their nerves are too shot by drugs, or alcohol, or trauma, or what have you, to even act on them!"

All of which may or may not be true, but all of which is hopelessly entangled with all the other economic and social crises that imminently threaten the sanity and survival of us all, probably a lot sooner rather than later, in this currently sad and undignified excuse for a human society we are living in. 

I can help but think of the rats-in-a-cage-being-fed-cocaine experiment, wherein the rats quickly became hopeless drug addicts when given free access to water laced with the addictive drug, but which was soon debunked by a modified version of the experiment, wherein the rats were outfitted with much more interesting cages which, unlike standard labrat cages, were designed to be a sort of little rat Shangrila, filled with rat amusements and opportunities for social interactions with other rats, in which no rats ever became addicts, despite free access to the drug laced water.

So when it comes to environment and affect, who can ever really say exactly which was the cause, and which the effect??

All of which also makes me think about Vijay Prashad's recent inspirational keynote address about cultural change at a conference last year. Prashad brought up Marx's famous quote towards the end, about how "formerly, philosophers spent their time seeking to understand the world, whereas today, our job is to change it", but Prashad sharply criticized this line from Marx as being shallow and almost backwards. In fact, Prashad maintained, far from being able to understand the world as some kind of ideal isolated from practice, it is actually precisely by way of attempting to CHANGE reality that we come to understand it better! 

So the philosopher in an ivory tower who fancies him or herself as some kind of disinterested savant is merely deceiving themselves. Instead, we mustn't be too cowardly or squeamish about actually wading into messy reality and trying things out, a squeamishness which too easily becomes as a mere cop-out for maintaining illusions about our own supposedly dispassionate, tidy positions atop the dungheap of reality.
