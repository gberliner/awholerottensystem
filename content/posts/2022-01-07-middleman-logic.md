---
title: Middleman logic
author: guyberliner
type: post
date: 2022-01-07T00:40:40+00:00
url: /2022/01/07/middleman-logic/
categories:
  - Uncategorized

---
Under capitalism, by default, industrially processed food manufacturers are most financially rewarded by optimizing for food products that simultaneously use the cheapest possible input ingredients while also maximizing the addictive/compulsive qualities of the product (the technical term in modern health science for the latter quality being "food reward").

Likewise, under capitalism, for-profit mass media firms are most financially rewarded by producing or curating content that costs as little to produce as possible while also maximizing its tendencies for "engagement" (ie, plays, views, clicks, etc).

The predictable results, in both cases, are products that do a lot more good for the bottom line of for-profit businesses than they do for the physical health, or mental or spiritual edification, respectively, of their hapless consumers.

(For example, for food, in the limit case, if you could legally get away with it, you would take literal dirt, or feces, or something, dress it up and deodorize it somehow, sprinkle it with something highly addictive, and come up with a cute name and packaging. For media, you would take the most lurid, sensationalist possible images and events, even snuff porn, or graphic, senseless violence or something, call it a "story" and publicize it. 

Of course, you can't do any of these things legally, though, and even if you could, people are still not total idiots. They would figure it out and shun your products. So the big bucks come from artfully disguising your rotten ingredients just well enough to escape easy detection by the average consumer.) 

Innovations like "social media" are merely new-fangled iterations of the same model, possessing the same set of basic incentives and qualities. So we should not be surprised if the results are similar -- albeit, possibly amplified.
