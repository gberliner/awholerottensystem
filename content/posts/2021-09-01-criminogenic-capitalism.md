---
title: Criminogenic capitalism
author: guyberliner
type: post
date: 2021-09-01T00:40:40+00:00
url: /2021/09/01/criminogenic-capitalism/
categories:
  - Uncategorized

---
B. Traven's novel, "La Carreta", is one of his series of dramas set in southern Mexico during the Mexican Revolution -- then and now the country's most heavily indigenous and least industrialized (as well as least fully capitalist integrated) regions. In it, he tells the story of the operations of cargo caravan companies who supply remote cities and towns with goods from the rest of the world. 

The operators of these freight companies, despite having excellent recordkeeping tracking their operating costs at all times, in order to maximize profits, routinely shortchange their drivers on the costs of spare parts and other necessities for keeping their vehicles operating, expecting the drivers to "wing it" somehow. The drivers, in turn, have a nasty habit of illegally chopping down trees for fashioning wooden replacement parts for their cargo trailers (all horse-drawn), grazing their animals illegally on pastures without consent of locals, and generally slyly pilfering whatever shortfalls their masters refuse to make good.

Once in a while, a driver is caught at this and locked up for his troubles, until a company's owner ransoms him and pays the fine for his depredations -- always swearing, of course, that he never "authorized" such illegal activities.

This, as it turns out, is a model in miniature for criminogenic capitalism.

"Conservatives" often lampoon observations of this sort, caricaturing "whining liberal apologists for criminals" as always seeking to "blame socieity". But nobody seriously claims that crime is nonexistent or never the fault of individual perpetrators, only that the amount of such crime depends heavily on the rules and customs adopted by the larger society.

Material reality does exist and does exercise an independent influence on the shape of human events, regardless of the airy pretenses of law abiding morality that continue to be affected by the latter day counterparts of the freight companies of B. Traven's novel today.

We can say with unequivocal logic, for example, that if the laws of atmospheric physics and chemistry, among other things, impose any outer limits on the rate of profits that capital can sustainably extract from the Earth, then exceeding those limits will entail theft of one form or another. 

Capitalists who exceed those limits are then invariably like the freight company owners of "La Carreta", or like drug kingpins today: they merely insulate themselves from legal repercussions for their crimes via layers of subordinates and middlemen. 

It may well be, though, that capitalists can legitimately use the excuse that all their competitors are doing the same, and that staying in business (and providing all our cherished "jobs") also requires them to do likewise. But that only further illustrates my point: the shape of society's laws and customs dictates much of the behavior of individuals under it.
