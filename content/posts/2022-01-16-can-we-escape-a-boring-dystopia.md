---
title: "Can we escape a boring dystopia?"
author: guyberliner
type: post
date: 2022-01-12T00:40:40+00:00
url: /2022/01/12/can-we-escape-a-boring-dystopia/
categories:
  - Uncategorized

---
"Can we escape this boring dystopia?" goes the title of an interesting recent video by Doug Lain, the online media content creator (formerly of Zero Books, the small, independent publishing imprint). For the sake of maximal honesty, I will say upfront that I am not yet addressing the "can we" part -- and, if so, how we can -- of the title. For the moment, I just want to draw attention to the existing, peculiar patterns it points out.

A "boring dystopia" strikes some as somehow a paradox. And yet, it's perfectly logical.

If it's boring by *accident*, that's just random bad luck. But, if it's boring by *design*, that's precisely dystopia, by definition.

Believe it or not, for example, war is chockful of extensive periods of boredom. Say I'm cooped up inside and can't go out and enjoy any regular activities because, while indoors I'm safe, but outside, a few blocks or a few miles along the way to where I want to go, a minefield or a sniper lies in the way. I can then easily get bored out of my mind. As a matter of fact, if you read any war diaries by ordinary people, that is a routine experience they recount.

Likewise, on a more mundane level, if, say, I'm cooped up inside because air pollution threatens my health (call it "war on the Earth", if you like), I can once again get bored out of my mind.

"Dystopia" is precisely a condition of *society*, in which our goals are frustrated and we are trapped, and feel powerless to alter the *artificial*, imposed conditions of our suffering.

In its most extreme form, deliberate sensory deprivation by means of solitary confinement, as practiced in many US prisons, "boredom" crosses the line into literal torture, as internationally recognized by major human rights groups. Such treatment can literally permanently destroy people remarkably rapidly, both mentally and physically.

Human existence, like all ordered patterns in nature, depends on ordered cycles. But all such cycles operate within a "Goldilocks" zone, being neither too chaotic, nor too rigid. Self-regulating, living systems all exhibit these features: cycles with very predictable repetition, yet combined with adaptability and variability. Without the former, chaos, such as the disordered multiplication of cancer cells, tears apart living systems completely, killing them altogether. 

But, just as surely, without variability and adaptability, living systems become brittle and increasingly susceptible to inevitable, exogenous shocks, any one of which can eventually cause the same destruction as internal chaos itself. (And, as mentioned in the example above, sensory deprivation, a special, extreme form of "boredom" -- though not commonly thought of as such -- atrophies and degrades through disuse the human physiological circuitry responsible for all the constant, necessary responses to changing outside stimuli that a human depends on for his or her survival.)

The word "dystopia" arose as a description of various fictional realities, usually not too dissimilar from our own, but most often projecting already visible, worrisome trends to absurd extremes. And also, most often, these trends involve increasing authoritarianism and social regimentation.

Authoritarianism emerges spontaneously out of paranoia towards perceived threats arising from inevitable variability and adaptation, suspected of heralding the emergence of chaos. But authoritarians believe that such threats can best be warded off by maximally cutting off all signs of variability altogether. Thus they run afoul of the other shoal, fragility and loss of adaptability.

Today, we are already living in social conditions that would be nearly perfect materials for the making of the likes of many of the dystopian fictions of our recent past (think "Fahrenheit 451" (1953), "Network" (1976), "Bladerunner", (1982), and even the short, antifascist postwar satirical animation, "Chromophobia" (1966), etc).

All of these dystopias involve an outwardly bizarre flattening of many then-ordinary (when conceived) features of our daily reality. Again, all such radical simplifications are made by authoritarians as safety precautions against the feared onset of chaos. Often, though, these flattenings appear as ingenious innovations.

Fascism, for example, as a form of authoritarian distortion of reality, certainly has historically depended on generating excitement about supposedly wondrous innovations. In Italy, Mussolini's movement was deeply influenced by the Italian futurism of important cultural figures, including poet and populist demagogue Gabriele D'Annunzio, for example. Hitler's utopian fantasies were also chockful of eugenics, secret superweapons, etc. 

Without some outward appearances of novelty and excitement, such paranoid, rearguard, reactionary movements would never have promising prospects for success. Because, even without any elaborate intellectual justifications, people are naturally repulsed by appeals for the imposition of a radically simplified, more regimented existence.

Today, new converts to such movements continue to be attracted by a fascination with impressive weapons, prospects of exciting and daring adventures, and becoming protagonists able to break out of their current, humdrum existences and pioneer a brave new future.

Ironically enough, then, the increasing regimentation of everyday life, by which people feel an increasing loss of personal agency, due to restrictions imposed upon them, apparently senselessly and against their wills, continue to attract some people towards visions of far more radically restricted forms of coercion and regimentation. But such existing regimentation often gets distorted into a perception of (almost purely imaginary) threats of encroaching chaos, believed to be originating from inside of their society, yet also from "outside" (of their community, or their neighborhood, or their country, etc), by dark "others" with whom they are usually only vaguely acquainted.

Marx said that "capitalism produces the working class it needs", and it often requires regimentation to do it. But such regimentation does not so much permanently suppress people's needs for autonomy and variety as it does frustrate and horizontally displace them. The inner tension and frustration people experience cannot be safely directed by individuals against the (wholly familiar) figures representing power and prestige that they see before them everyday, on tv, in newspapers, and so on, for example, but they can be deliberately and skillfully redirected, laterally, by various "political entrepreneurs", against the many diverse and often historically disadvantaged groups in the society around them.

Authoritarianism and dystopias are, of course, by no means exclusively limited to the political right. Leftist regimes have on numerous occasions also succumbed to paranoia and siege mentalities and fallen into dystopian forms of authoritarian regimentation. But, differently from rightwing visions, however, leftist projects are not usually inspired from the beginning by primarily backwards leaps towards an imagined past of purity and simplicity, tranquil and unthreatened by new variation and adaptations. 

Rather, leftist dystopian tendencies emerge most commonly spontaneously, especially in the course of building projects of "socialism in one country", when confronted with the bitter realities of isolation, and fears of punishment -- real or imagined -- by a larger, hostile, integrated world system, a system which is itself entirely real. So, much as leftist sympathizers might fancy ourselves immune from these doleful phenomena, we ignore the lessons of various 20th century manifestations of leftist authoritarianism, Stalinism, etc, to our peril. 

(I should hasten to add, though, that contrary to philosophers the likes of Hannah Arendt, I reject any facile elisions of the profound differences between, say, fascism and Stalinism. Equating such drastically different historical interludes only tends to promote endless confusion, and serve as grist for apologies to justify established, powerful, regressive institutions, which themselves are actually sowing the seeds of new and emerging dystopian futures.)

In this way, dystopias are real phenomena, which fiction only prefigures via temporary exaggeration of existing trends, trends that have built-in positive feedback loops which can easily turn yesterday's outlandish fictions into today's mundane realities.
