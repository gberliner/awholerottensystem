---
title: Advice to good landlords
author: guyberliner
type: post
date: 2022-10-10T00:40:40+00:00
url: /2022/10/10/advice-to-good-landlords/
categories:
  - Uncategorized

---
*"Take heed that ye do not your alms before men, to be seen of them: otherwise ye have no reward of your Father which is in Heaven."* (Matthew 6:1)

Are you a "good landlord"? Do you refrain from constantly raising your rents as high as the market will bear? Attend to problems promptly? Refrain from meddling in your tenant's personal lives, or using your eviction power to intimidate them into having your way with them however you please? Congratulations! But know this:

If you are going around touting your "good landlord" bona fides, then, unfortunately, you are only carrying water for the "bad landlords"! 

Exactly who do you think that "bad landlord" corporate lobbyists trot out, anyways, to shoot down any and all efforts to protect tenants? If you don't know, just tune in to public hearings any time a proposal for any new tenant protections is considered or introduced, whether at the city, county, or state levels.

Hint: believe it or not, when big landlord lobby groups show up at these hearings to fill slots with landlord sob stories,  they never pick greedy Wallstreet hedgefund investors to explain how this is going to hurt their firm's next quarter returns!

No. It's ALWAYS someone explaining how the proposal being considered will sink their poor grandmother's hopes of feeding her cat, because any possible reduction in her rental income will force grannie to eat the cat food herself instead!

So if you really ARE a "good landlord" unicorn, then get out there and demand a world where granny doesn't have to sidle up to Blackstone and help them guarantee their company's next quarter earnings report, just so she can keep her cat fed on her rental income. 

Instead, demand a world where worker's wages are high and increasing enough to fund regular and generous cost of living adjustments to grannie's social security as necessary, which, as ever, are paid for out of those worker's withholding!

Instead, demand a world where we build robust public housing programs, so that those workers can find a patch of land to live on where nobody has license to lord it over them in the first place. And demand we create better, more attainable paths to home ownership for those tenants, too, since that remains a highly sought after ambition for millions of people, one that isn't going to suddenly disappear. (The latter will also help to relieve the unfair stigma our society has put on public housing, a viciousness not seen in other countries with much better housing programs.)

If we've done all this, and people still want to voluntarily rent housing from willing owners, fine. As long as nobody is being economically coerced into often exploitative relationships, no harm no foul. 

But until then, you are never really a "good landlord" until you are a landlord who is willing to contemplate a world where nobody else is ever effectively coerced into being a "good tenant" for your benefit.
