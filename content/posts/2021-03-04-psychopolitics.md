---
title: Psychopolitics
author: guyberliner
type: post
date: 2021-03-04T23:44:33+00:00
url: /2021/03/04/psychopolitics/
categories:
  - Uncategorized

---
 

In his book, &#8220;Psychopolitics&#8221; (2017, <https://www.versobooks.com/books/2505-psychopolitics>), German-Korean philosopher Byung-Chul Han has published a riveting (to my mind) synopsis of our emergent cybernetic &#8220;Brave New World&#8221; form of capitalism. 

Han picks up where the likes of Deleuze and Guattari left off, with a critique of the totalitarian &#8220;biopolitics&#8221; conception of modern authoritarianism, described most notably by Giorgio Agamben (cf: &#8220;Homo Sacer&#8221;), for which today&#8217;s contemporary &#8220;psychopolitics&#8221; is, he claims, the successor. 

While &#8220;biopolitics&#8221; is still very much with us &#8212; witness China&#8217;s brutal surveillance regime against the Uyghur population of Xinjiang province &#8212; it is the control regime of the past. Because it&#8217;s expensive to maintain, creates obstacles to commerce and capital, is bad for PR, and many other reasons, reliance on this control model, while not due for disappearance any time soon, is nonetheless a sign of weakness, not strength. 

In today&#8217;s &#8220;psychopolitics&#8221;, workers monitor and surveil themselves, publish their user profiles freely on Facebook, Tinder, and myriad other social networking sites, and generally willingly make available their personal data for the sake of profitable mining and commoditization by dominant economic and political elites. 

But for me, the originality and profundity of Han&#8217;s work really lies much more in his critique of the false consciousness implied by psychopolitics, his insights into which extend his earlier ones in books like &#8220;The Burnout Society&#8221; (»Müdigkeitsgesellschaft«), 2010, as he develops his observations of surplus &#8220;positivity&#8221;, or the regime of &#8220;cruel optimism&#8221; that undergirds the latest forms of neoliberal capitalism. 

&#8220;Cruel optimism&#8221; (the ideology of individual &#8220;positive thinking&#8221;, divorced &#8212; indeed, hermetically sealed off &#8212; from awareness of broader social forces and conditions) has long been with us (cf: Weber&#8217;s &#8220;Protestant work ethic&#8221;), but today more than ever, it is the foundation of entire industries, in the fields of psychology, self-help publishing, corporate consulting, religious cults, and on and on. 

&#8220;Psychopolitics&#8221; represents the infinite expansion of the ideology and practice of &#8220;positivity&#8221; (role playing, living out one&#8217;s &#8220;personal potential&#8221;, developing one&#8217;s personal &#8220;brand&#8221;, conceptualizing oneself as a perpetual entrepreneurial project of one). Psychopolitics recasts Erich Fromm&#8217;s conception of the &#8220;marketing character orientation&#8221; into a universal ideal. One is always and forever &#8220;building one&#8217;s brand&#8221;, recuperating &#8220;positive and enriching life lessons&#8221; from any tragedy, and contributing thereby to the uninterrupted flow and increase of global capital. 

In some of Han&#8217;s most memorable lines, he echoes Marx&#8217;s notions about &#8220;tricks of capital&#8221;: 

&#8220;The freedom of Capital achieves self-realization by way of individual freedom. In the process, individuals degrade into the genital organs of Capital. Individual freedom lends it an ‘automatic’ subjectivity of its own, which spurs it to reproduce actively. In this way, Capital continuously ‘brings forth living offspring’.4 Today, individual freedom is taking on excessive forms; ultimately, this amounts to nothing other than the excess of Capital itself.&#8221; 

In Han&#8217;s retelling of this (Marx&#8217;s original) account, our new version of capitalist &#8220;freedom&#8221; is the ultimate ruse. Ironically, the word &#8220;freedom&#8221; itself, as descended ultimately from etymological sources in multiple languages, implies &#8220;enjoying time with one&#8217;s friends&#8221;. (I verified this remarkable observation of his in multiple etymological dictionaries, including both the OED and the famous Deutsche Wörterbuch of the Brothers Grimm! cf: <https://woerterbuchnetz.de/>), the words &#8220;freedom&#8221;, &#8220;friend&#8221; (English) and &#8220;Freund&#8221;, &#8220;frei&#8221;, &#8220;freuen&#8221; and &#8220;froh&#8221; (German) being all closely related. But &#8220;freedom&#8221; as individual entrepreneurial &#8220;self-realization&#8221; project, dedicated to the production of fungible value, ie, capital, instead of an end in itself, is the antithesis of this! 

According to Han, &#8220;psychopolitics&#8221; can be seen as something akin to the endpoint or highest optimization of neoliberalism, in which a working class subject ceases to exist, even as a latent potential, replaced, terrifylingly, by a perfect neoliberal subject who frictionlessly &#8220;exploits him or herself&#8221;: 

> As a mutant form of capitalism, neoliberalism transforms workers into entrepreneurs. It is not communist revolution that is now abolishing the allo-exploited working class – instead, neoliberalism is in the course of doing so. Today, everyone is an auto-exploiting labourer in his or her own enterprise. People are now master and slave in one. Even class struggle has transformed into an inner struggle against oneself. 
> 
> The cooperative ‘Multitude’ that Antonio Negri has exalted as the post-Marxist successor to the ‘proletariat’ does not describe the contemporary mode of production. Rather, conditions are defined by the solitude of an entrepreneur who is isolated and self-combating and practises auto-exploitation voluntarily. As such, it is a mistake to believe that the cooperative ‘Multitude’ will overthrow the parasitic ‘Empire’ and bring forth a communist social order. The Marxist scheme to which Negri adheres will prove to have been yet another illusion. 
> 
> In fact, no proletariat exists under the neoliberal regime at all. There is no working class being exploited by those who own the means of production. When production is immaterial, everyone already owns the means of production him- or herself. The neoliberal system is no longer a class system in the proper sense. It does not consist of classes that display mutual antagonism. This is what accounts for the system’s stability. 
> 
> Today, the distinction between proletariat and bourgeoisie no longer holds either. Literally, ‘proletarian’ means someone whose sole possessions are his or her children: self-production is restricted to biological reproduction. But now the illusion prevails that every person – as a project free to fashion him- or herself at will – is capable of unlimited self-production. This means that a ‘dictatorship of the proletariat’ is structurally impossible. Today, the Dictatorship of Capital rules over everyone. </blockquote> 
> 
> Han&#8217;s book makes for chilling and sobering reading. While not long on solutions, I find it immensely valuable for its insights and debunking of false hopes and illusions. 
> 
>