---
title: "Capitalism and 'creative destruction'"
author: guyberliner
type: post
date: 2022-01-16T00:40:40+00:00
url: /2022/01/16/capitalism-and-creative-destruction/
categories:
  - Uncategorized

---
John D. Rockefeller famously extolled capitalism as "creative destruction". And, indeed, Marx would have agreed with him on that. Marx, too, famously remarked on capitalism's "continual revolutions" in production, drawing the whole world into its orbit. He even praised its resulting "civilizing effects", and clearly thought it heralded great potential progress for humanity.

The problem, of course, is that these "continual revolutions", this "creative destruction", has no constraints imposed on it by default, and left thus unbridled, alongside all the marvels it can produce, the chaos and torment it can inflict on the lives of ordinary people are also equally astonishing and perilous.

It should be counted as one of the bitterest ironies, then, that in a country like the United States, today's world center of capitalism as a global system, the word "conservatism" has been hijacked and repurposed to describe, as a political ideology, not a distrustful stance towards all innovation generally, but instead, an uncritical apology of precisely that capitalism and its attendant "creative destruction" that Rockefeller extolled.

Because, if there is anything that such "creative destruction" signifies, it's obviously the antithesis of any historical notions of purely conservative, cautious protection of existing relationships, human and otherwise. The "conservation" to which professed "conservatives" in the United States adhere does NOT refer to protecting the stability of life for the vast, working class majority of the population.

Enter "right populism", however. Very recently, it seems, figures on the political right have noticed the increasingly unbearable tensions inherent in this contradiction between "conservatism" in its plain language meaning of "protecting general conditions of stability" for the broadest possible majority, vs "conservatism" as referring to espousing a very specific economic ideology, which happens to admirably serve the interests of a small minority, but increasingly *threatens* the interests and stability of a much larger part of the population.

Of course, these "right populists" tend to adopt solutions to such contradictions that, instead of advocating for protecting the stability of the lives of the broadest mass of the population, instead involve completely historically predictable reactions *against* certain groups, on the reasoning that, by accusing such groups of "crime", "greediness", "reverse racism", and supposedly endless conspiracies against historical ethnic majorities or pluralities, the clock can be rolled back. These theories merge with historic forms of fascism and "third positionism", which consist of "tripartite" conceptions of politics, in which a "degenerate élite" conspires to gain power to the disadvantage of historically dominant ethnic groups by advancing "undeserving" minorities at their expense. 

Leftists sometimes accuse these new, emerging rightwing demagogues of "insincere populism". But that's a shallow critique that does little or nothing to disarm or neuter the attraction of such dangerous ideological actors. Whether they are "sincere" or not, whether they authentically want to mitigate the rigors of economic austerity in favor of some updated form of "Kraft durch Freude" or not, the dangers they pose are equally dire.

Any response to these dark ideological tendencies has to return to first principles, including a basic critique of capitalism. The conceit that "creative destruction", for example, can be left to its own devices at all, should be clearly identified as the genesis of the crisis that "right populists" claim to be talking about in the first place. From that, and the shared hardships it creates for *all* working class people, regardless of ethnic, national, religious, or any other identities, proceeds the necessity of a shared commitment to overcome this crisis via a new kind of economy and politics.
