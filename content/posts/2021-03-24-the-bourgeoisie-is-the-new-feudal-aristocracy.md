---
title: The bourgeoisie is the new feudal aristocracy
author: guyberliner
type: post
date: 2021-03-24T22:54:09+00:00
url: /2021/03/24/the-bourgeoisie-is-the-new-feudal-aristocracy/
categories:
  - Uncategorized

---
Originally, the bourgeoisie was at war with the landed feudal aristocracy. The French Revolution was the high point of this confrontation, but it continued into the 19th and in some cases even the 20th centuries. 

Today, however, the emergence and growth of an integrated worldwide &#8220;FIRE&#8221; sector (finance-insurance-real-estate) has enabled the bourgeoisie to assume a position with respect to the rest of society formerly occupied by the same landed feudal aristocracy it once rebelled against. 

By promoting the enclosure of what by logic and moral arguments should be treated as commons and public goods (arguments which they themselves sometimes formerly championed), the bourgeoisie as a class can escape the brutal competition and unpredictability implied by rigorous market capitalism. (Once upon a time, the bourgeoisie had to suffer paying extortionate tribute to the feudal nobility for access to these means of production. Nowadays, though, they have the upper hand, so their erstwhile principled objections to such extortion have not so surprisingly vanished.) 

Acquiring control of monopolist and rentier assets like land, banks, insurance companies, etc, nowadays allows them to fortify themselves permanently against the dangers of the kind of rigorous &#8220;competition&#8221; they extol in their own propaganda. Via real estate, they control land, a natural monopoly whose fixed supply God is not making any more of, and whose price they can bid up to levels only they can afford to buy and then collect arbitrary rents from (of which even such things as thirty-year mortgages are really only an extension of the same rentier logic). Via banking and insurance they can also gain access to the sovereign power to essentially mint money (running either of which kinds of companies requires a special government charter and proven control of certain large capital reserves not accessible to ordinary mortals). 

The resulting fact that today issues of healthcare and housing are so fraught in our society should come as no surprise, then. To challenge the bourgeoisie on their deathgrip over these kinds of assets is not merely to threaten their short term profits. It actually threatens their ability to safeguard their perpetual dominance over the rest of us into eternity. 

This is a point that has not been lost on observers like Yanis Varoufakis, former Syriza finance minister in Greece, who calls our current regime &#8220;no longer capitalism, but actually something worse&#8221;, ie, a kind of neo-techno-feudalism.