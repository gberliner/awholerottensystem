---
title: Michael Brooks on “Integral Theory”
author: guyberliner
type: post
date: 2021-05-24T23:19:00+00:00
url: /2021/05/24/michael-brooks-on-integral-theory/
categories:
  - Uncategorized

---
We are in humanity&#8217;s most perilous historical phase yet, but we are also in a moment that presents us with an opportunity to take stock of our million year history in ways that are perhaps unparalleled.Today, we have access to an extraordinary range of knowledge about the human experience spanning time and space, continents and cultures. The late Michael Brooks was onto something, and had some profound observations to make as he started to develop what he called a &#8220;cosmopolitan socialism&#8221; (<a rel="noreferrer noopener" href="https://www.youtube.com/watch?v=iWIkp7jR3uo&fbclid=IwAR3-GEBEV57q_P8yv57raFHJMvTrLIZ10lYJMITgU1WoPqjWlHBS6dHmDDc" target="_blank">https://www.youtube.com/watch?v=iWIkp7jR3uo</a>).



He mentions &#8220;integral theory&#8221; as a fumbling attempt to get at this. Ken Wilber is one of the noted exponents of this. A little known and under-appreciated philosopher, the nature writer Thomas J. Elpel (of &#8220;Botany in A Day&#8221; fame), also wrote an admirable little summary of Wilber&#8217;s ideas called &#8220;Roadmap to Reality&#8221;. A profound point Elpel makes is that consciousness is not an absolute binary. It&#8217;s actually a continuum. And much of our lives are necessarily spent in reflexive and instinctual behavior, and those behaviors also dominate most of our opinions and what we flatter ourselves to be &#8220;thoughts&#8221;, even (especially?) on political and moral subjects.



Consciousness, and our ability to think critically, is evolving, on both individual and social levels. And we owe a lot to &#8220;modernity&#8221;, for all its good and bad parts, as a part of that evolution. We cannot divorce ourselves from it, or label it entirely good or entirely bad, when it is in fact an inextricable part of both who we are and who we are becoming. And the story of that has not yet been written (and never will be fully, of course, until the last human being draws breath).<a rel="noreferrer noopener" href="https://www.youtube.com/watch?v=iWIkp7jR3uo&fbclid=IwAR1MfvWp3YIjj01T4af5cBE46eAUPYRnGc9p2_cofeZgpwxWdjkxvraD-XE" target="_blank"></a>