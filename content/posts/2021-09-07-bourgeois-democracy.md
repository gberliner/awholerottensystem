---
title: Bourgeois democracy
author: guyberliner
type: post
date: 2021-09-02T00:40:40+00:00
url: /2021/09/07/bourgeois-democracy/
categories:
  - Uncategorized

---
Bourgeois democracy is a stage managed democracy, whose success is predicated on a sufficiently convincing simulacrum of popular sovereignty so as to fend off threats of real class struggle with the potential of eroding bourgeois power long term (as opposed to unidirectional class warfare always favoring economic élites). But calling it a mere "simulacrum" probably discounts too cavalierly its need to manage and in some cases involuntarily extract sufficiently generous concessions from those economic élites to ensure sufficiently broad buy-in from the rest of the population.

Accordingly, this "stage managed democracy" can (and needs to) maintain impressively independent institutions, such as academia, courts, and others.

This is the crux of the sore point from which the real discontinuity and rupture between bourgeois democracy and fascism arises. There are always elements of the big bourgeoisie who categorically reject ANY class concessions, or most, and are prepared to trigger crises and even violent confrontations to get their way whenever they think they can realistically win this way.

In some cases, these élite reactionaries are guided mainly by their own inflexible ideological extremism. In others, like that of Trump, they are "gamblers" (as the assassinated Iranian leader Qasem Soleimani astutely described him).

Naturally, though, the success of these ultrareactionary elements is unlikely in the face of opposition to their antics from the overwhelming majority of the big bourgeoisie. Only when a critical mass of the ruling élites agrees with them are their odds of success high.

Accordingly, despite the frightening power they now exert over mass media, their success in undermining independent power centers like the judiciary, and their increasing willingness and ability to sow widespread social chaos, these ultrareactionary elements seem unlikely to achieve a fascist coup d´état or to realize their maximal ambitions in the near future.

But, as conditions deteriorate and economic élites lose their leverage over the situation and their room for maneuver via modest concessions diminishes, and as real class struggle becomes an increasingly realistic prospect, this balance of élite opinion could swing rapidly in the direction of the ultrareactionaries.
