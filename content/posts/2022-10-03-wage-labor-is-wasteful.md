---
title: Wage Labor is Wasteful
author: guyberliner
type: post
date: 2022-10-03T00:40:40+00:00
url: /2022/10/03/wage-labor-is-wasteful/
categories:
  - Uncategorized

---
My hypothesis: essentially all capitalist wage labor is inherently extremely heedless and wasteful. Even if for no other reason than that the wage laborers, who sell their labor power, are under the dictate of their bosses, and are not at leave to exercise wider discretion about how they perform their labor, under pains of being fired. 

Accordingly, they are not going to be magically using their own judgement and somehow "optimizing" their performance to protect essentially any important values external to the end product itself (whether relating to the health of others, or the environment, or even basic neighborliness), if they are not under detailed and explicit instructions to do so.

(Now, you might suppose this would be a problem specific or unique to low paid, manual labor, where workers are "on-the-clock" and have the least discretion of any category of labor. But you'd be wrong. Because the higher-paid sort of workers, the "specialists" and "experts", are going to be even more heedless of anything falling outside their "specialty". The "cult of expertise" contributes to this, as does the naturally accompanying conceit that everybody must "stay in their lane", along with the unfounded assumption that there's some OTHER "expert" somewhere or other who will surely handle any issues external to your own area. Helena Norberg Hodge interviewed a famous Buddhist teacher from Ladakh, Norbugang Rinpoche (?), who expounded on this subject.)
