---
title: "Student loans: we all pay"
author: guyberliner
type: post
date: 2022-01-07T00:40:40+00:00
url: /2022/01/07/student-loans-we-all-pay/
categories:
  - Uncategorized

---
When it comes to education, at least at the childhood and adolescent levels, our society has long since recognized that an educated population is a basic, shared social good that we all have to pay for.

When it comes to higher education, we USED to tacitly recognize this to a greater degree than now. The US established the system of land grant public colleges and universities in just such a spirit. And tuition fees were once nonexistent in practically all of them (for in-state residents, at least), or else vastly lower, than today.

In the neoliberal era inaugurated by Reagan and Thatcher, though, came cuts to public higher education, however, and colleges and universities were left to their own devices. They sought out profitable "public-private partnerships" with industry, developed a "marketing" orientation towards higher income families, by upgrading their facilities and offering new perks, like elaborate fitness facilities, conference centers, etc. And they started dramatically raising fees. But not everybody could afford these. Enter student loans.

Now, even though not everybody could afford higher tuition fees, the basic importance of public education was still recognized, and the importance of developing and not wasting the gifts of the entire population, regardless of socioeconomic status, was too. The GI Bill attested to this understanding, and later, expansion of federally backed student loans and loan guarantees to non-military students was carried out in the same spirit.

But costs continued to escalate, and the corresponding burdens of student debt have become more and more onerous. Today, federally guaranteed student debt amounts in the trillions, and rivals credit card and mortgage debt in size, while having much harsher terms. (Student debt cannot be discharged without the greatest difficulty, being exempt from bankruptcy laws.)

The most ridiculous thing about this debt is, we are ALL STILL paying it, even if indirectly! When students enter the workforce, their wages go to pay the debt, either increasing the cost of their labor, or diverting their income from circulating in their own communities. Either way, the general public pays the price for this, and only loan servicers profit.

Thatcher's doctrine that "there is no society" certainly did not suddenly -- at least in the case of education -- magically change reality. There still very much WAS and IS a society. The only thing it changed was, it provided a crowbar for financial capital to penetrate that society and extract rents from what used to be a purely public service.

It would be far cheaper to the public, and more logical of all of us, to just cut these parasitic middlemen out, or force massive haircuts on them all, and just directly pay all current and future tuition costs on a public basis.

(Naturally, I am eliding a lot of details for brevity sake. I have alluded to the fact, though, that colleges and universities resorted to lots of schmes to recoup their lost funding, including hiring expensive administrators to develop facilities and "partnerships". Just canceling all the debt certainly doesn't cancel the shared responsibility of administrators and other players in higher education for contributing to its escalating costs. But eliminating debt profiteering from the equation, while restoring more generous public funding for the basics, will itself go a long way towards reining in those practices, which were born mainly out of earlier austerity in the first place, rather than any independent "initiative" (read, greed) of educational institutions themselves.

The point is, it was a PUBLIC POLICY decision to essentially privatize previously public higher education, by privatizing its financing. All the adverse consequences that flowed from that initial decision were AFTEREFFECTS, not primary causal factors, to all ensuing escalating costs.

Figuring out the full measure of how to reverse the damaging toll of all the doleful effects that privatization has had on academia, including empowering an overweaning class of administrative bureaucrats, degrading the autonomy and academic freedom of professors, inducing the "precaritization" and "proletarianization" of a growing class of severely underpaid adjuncts and graduate student-employees, and on and on, must be saved for a separate discussion.)
