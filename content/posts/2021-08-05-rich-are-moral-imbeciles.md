---
title: The Rich are Moral Imbeciles
author: guyberliner
type: post
date: 2021-08-05T00:40:40+00:00
url: /2021/08/05/the-rich-are-moral-imbeciles/
categories:
  - Uncategorized

---
In a society such as that of the United States, the overwhelming majority of the rich are moral imbeciles. I am not engaging in hyperbole now, or expressing sour grapes, envy, or mere moral indignation, but observing a simple, prosaic matter of fact. And I should not have to engage in ten minutes of apologies and asides to the effect of "not all rich people..." to justify my meaning here. It should instead be self-evident in a society as simultaneously enormously materially wealthy as the United States and yet so seemingly incapable of organizing the most rudimentary social provisions for its population. If that is not the fault of "the rich", in a society where even a populist, egalitarian radical like Ralph Nader writes a book with a title like "Only the Super Rich Can Save Us", then whose is it exactly?? 

Nader expresses no joy in explicitly acknowledging the nearly total collapse of popular sovereignty in the country and the prosaic reality that the population is groomed to look for answers to its worst crises from self-appointed "health experts" like billionaire philanthropist Bill Gates, self-appointed forensics expert Tucker Carlson, self-appointed "climate champion" Mike Bloomberg, and on and on. 

The fact that all these bourgeois clowns can only deliver answers that further exacerbate all the problems they claim to take an interest in is down to something a little more complex than sheer malice (although, in some of their cases, there's plenty of that, too). So it's not JUST that they are superior, smart, and cunning people out to trick the rest of us. Not exactly.

Political philosopher Byung-Chul Han has captured a much more profound insight into the nature of the problem with his humourous and provocative assertion that the bourgeoisie are the "genital organs of capital". In other words, they cannot quite help themselves. An imbecile is not merely contemptible. He/she is someone whose capacities are crippled in ways that complicate even the clear attribution of moral blame.

In short, the very rich literally do not know how to survive in a world in which their money can't buy them anything and everything. A world in which all of human, animal, and plant life is not actively being reduced into profit generating commodities is a world in which their own lives would be inexorably damned to shrinking and dwindling. They are only responding to stimuli around them no different than a slime mold that emits toxins when threatened. If capitalism and its perpetual growth mandate is "the ideology of the cancer cell", as some have put it, then they are the organelles of the rogue cell itself.

All emancipatory left politics is necessarily centered around DETHRONING the "dictatorship of the dollar", that is to say, the centrality of monied wealth and its attendant power as the nexus of world civilization. There is no reconciling such politics with concentration of wealth, and there are no problems that are, if not directly caused, then certainly lethally aggravated by it. But to fight these pathologies is also necessarily to pose an existential threat to those who derive their very identities from them. 

Assassinating a rich man is quite unnecessary: taxing him until he is no longer some arbitrary multiple richer than the average man or woman on the street would amount to the same thing. Or even rendering his wealth incapable of being fungible for any and all commodities, including political power, would be basically equivalent (since it could and would presage the former fate, or vice versa).
