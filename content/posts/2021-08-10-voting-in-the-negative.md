---
title: Voting in the negative
author: guyberliner
type: post
date: 2021-08-10T00:40:40+00:00
url: /2021/08/10/voting-in-the-negative/
categories:
  - Uncategorized

---
The more prominent the arguments for a "no" vote are in an election -- ie, the more dominant the arguments are in the negative or in opposition to a candidate, ballot measure, or program, in contrast to POSITIVE arguments promoting something, whether a candidate, a measure, or a program -- the stronger the correlation with low voter turnout, as supported, again, by abundant election experience and statistics. 

This experience spans countries and has been masterfully employed in elections around the world, even by dictators! Notably, Pinochet ensured in 1989 that the ballot measure presented to voters in Chile would make a vote for his continued dictatorship a "sí", "yes" vote, and the vote for new elections and a possible new govt a "no". This put his adversaries in the uncomfortable position of having to argue a vote-suppressing negative position, knowing full well that his was a minoritarian govt that would be favored by suppressed voter turnout. In the documentary film, "No" (2012), campaign advisors for both sides are portrayed behind closed doors discussing this -- Pinochet's smirking at their cleverness, and opposition ones wincing while brainstorming over how to overcome their dilemma.

We see the same dynamic play out over and over again. The most cynical and vicious combatants in a public argument are usually favored. It's usually too hard for bystanders to sort out the rights-and-wrongs of complex arguments, and so whoever fights hardest and dirtiest tends to win by default.

Bernie Sanders's notorious aversion for negative campaigning undoubtedly owes much to an awareness of this. 

Leftist politics in the United States has been caught in the vise of an endless losing streak for lack of a credible, positive program to champion, in contradistinction to a pack of horrible miscreants to hurdle epithets at. And guess what? The miscreants just keep banking their wins and carrying on with big, shiteating grins all over their smug faces.

But, in contrast, when leftist campaigns have POSITIVE programs to champion, programs that are broadly popular and apparently resonate well with the concrete material interests of voters, they often win big, no matter how "red" the state they run in. The 15 campaign in Florida being a model case in point, and the enfranchisement of freed prisoners in the same state another. Both won on ballots where far right Republican candidates in the same election season beat their Democratic opponents (in 2018, when DeSantis beat Gillum for governor, and in 2020, when Trump carried the state over Biden, respectively).

Bernie, unfortunately, couldn't ride this positive dynamic through the Democratic primaries in 2020, though, because the "negative" imperative of defeating Trump favored the most reactionary candidate in the Democratic race, who happened to also be regarded as "safest", on the strength of being a known quantity and a former VP. The widely cited imperative among Democratic primary voters of achieving a purely negative result -- ie, defeating Trump -- obscured and overpowered any positive goals or programs Bernie had to offer.

But once again, we can regard this as just more confirmation of the thesis: the dominance of the "negative" argument favors the worst and most unscrupulous candidates and causes.

https://www.youtube.com/watch?v=lOeiw_BJPas
