---
title: Illth vs Wealth
author: guyberliner
type: post
date: 2021-04-09T17:17:15+00:00
url: /2021/04/09/illth-vs-wealth/
categories:
  - Uncategorized

---
Philosopher John Ruskin coined the term &#8220;illth&#8221; after the model of, but in contrast to, the word &#8220;wealth&#8221;, to describe a certain mode of thought and life. To explain it, he offers the metaphor of a corpse draped in richly ornamented and bejewelled attire, and asks the question, &#8220;Is the corpse &#8216;wealthy&#8217;?&#8221; &#8220;Wealth&#8221;, Ruskin insists, has a crucial linkage to its cognate words, &#8220;well&#8221; and &#8220;weal&#8221; (as in &#8220;commonweal&#8221;), and is inextricably connected to health and wellbeing. To ask whether a corpse can own anything at all is absurd on its face, but Ruskin&#8217;s even more profound point is, a corpse is obviously the very antithesis of &#8220;health&#8221;. 

Thus, true wealth consists in the beneficial, [living use of things][1]. 

I have a relative who came of age in what some have called the &#8220;better-living-through-chemistry&#8221; generation. This relative has a great kitchen with many fine implements at their disposal, including excellent wooden cutting boards, but insists when I visit them and want to help out in the kitchen that I chop vegetables on a flimsy plastic sheet, which, being slick, the knife (and vegetables) tend to slip off of, in preference to letting me directly cut them on the cutting board itself, because any nicks in the board could detract from its aesthetic perfection. My relative&#8217;s preferred approach to chopping vegetables thus makes it inconvenient to perform the task, and defeats the very purpose of a cutting board in the first place. The real cutting board remains pristine but unused. 

I maintain, with Ruskin, that much of bourgeois controlled civilization has the character of &#8220;illth&#8221;, and the richly attired corpse he refers to, rather than true wealth. 

Bill Gates has one of the world&#8217;s great fortunes, valued in the many tens of billions of dollars. But his &#8220;asset managers&#8221; are acquiring farmland with a part of this fortune, which has already made him &#8220;the world&#8217;s biggest farmer&#8221; in terms of acreage owned. No doubt he can rent this land out and earn income from it – but also, by buying up so much of it, he no doubt also bids up its price, driving any aspirations of REAL farmers, especially young people, to practice that noble profession further out of reach. Clearly, Gates has no need of the additional income, while the real farmers need real land. Gates&#8217;s intentions with the farmland were the source of wild speculation, until he freely admitted recently that its acquisition had &#8220;nothing to do with climate change&#8221;, and was a decision made independently of him by his own financial advisors. 

The kind of wealth Bill Gates presides over illustrates Ruskin&#8217;s &#8220;illth&#8221; perfectly.  


<p id="*">
  * Regarding &#8220;making the living use of things&#8221;, the late Zen teacher Abbot Kyogen Carlson of Portland&#8217;s Dharma Rain Zen Center used this exact phrase in his Dharma talks on multiple occasions, possibly in reference to certain teachings of Dogen. I have been looking for other Dharma references to this turn of phrase, because it is so remarkable, but have not found them yet.
</p>

 [1]: #*