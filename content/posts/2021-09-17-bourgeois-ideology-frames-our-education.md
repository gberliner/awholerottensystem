---
title: Bourgeois ideology frames our education
author: guyberliner
type: post
date: 2021-09-17T00:40:40+00:00
url: /2021/09/17/bourgeois-ideology-frames-our-education/
categories:
  - Uncategorized

---
Certain public investments have shaped the entire character of our modern world, in ways which would otherwise make it practically unrecognizable, but for their initial and ongoing funding. And the impacts of these investments are visible and implicated in every aspect of our economy, including all accumulated wealth, public and private.

It is easy to name just a couple right now. For instance, it is hard to imagine (for better or worse) anything resembling the current automotive, oil, petrochemical, steel, and numerous other allied industries existing in anything like their current forms, nor the fortunes of any of their industrial titans worldwide, absent the development of the US interstate highway system starting in the 1940s. Likewise, the development of semiconductors, upon which all modern computing and telecommunications now relies, owes its entire origin to the line of publicly funded basic science research beginning with the discovery of quantum mechanics thanks to the work of Michelson and Morley in the 1880s, through to Bohr and Heisenberg in the 1920s, and only culminating in Shockley and Brattain's invention of solid state electronics in the 1940s and 50s.

It's practically impossible to imagine any single private business or combination of businesses committing to massive, risky investments of resources to projects with payoffs only after thirty, sixty, or eighty year intervals.

In fact, any time the payoffs for an activity are either very broadly shared in space, across many beneficiaries (ie, possibly the entire society), and/or spread out over many decades, and where the initial investments are high, and consequently any foreseeable, immediate rewards for investors are slim to none, that activity will be severely underinvested in if it relies solely on for-profit businesses and entrepreneurs for its financing. Such activities positively require public investments funded with taxes. A civilized society not thoroughly dominated by a single economic class would incorporate an acquaintance with such basic facts into any program of standard civics education.

Accordingly, one way we can know we are living under a bourgeois class dictatorship is the way that extremist "laissez faire" ideology frames the limits of our education and upbringing.

How else can one explain the reflexive glibness with which a clownish personality like Fauxnews celebrity Candace Owens can repeat the common pseudo-libertarian trope that "taxation is theft" in a debate with Russell Brand? And Brand's rejoinder should have been almost equally glib, but for his (and our own) basic educational deficits. (Which in no way is meant to slight Brand specifically. Because otherwise, after all, even a dolt like Owens would no more have robotically repeated such fatuous lunacy than she would have carelessly proclaimed the doctrines of the Flat Earth Society.) (See: https://www.youtube.com/watch?v=7DY2dayjdx4)

The practical consequences of bourgeois extremist ideology like this -- which no one ought to be able to repeat with a straight face -- being allowed to fly practically under the radar so easily this way can be a combination of ills. Either we suffer exactly such severe underinvestment, or the benefits of such investment are severely misallocated (which, in view of their public nature, SHOULD be broadly shared across our entire society), or both.
