---
title: Lumpen proletariat
author: guyberliner
type: post
date: 2022-10-11T00:40:40+00:00
url: /2022/10/11/lumpen-proletariat/
categories:
  - Uncategorized

---
From the capitalist standpoint, the beauty of the "lumpen proletariat" is, they are a strong disciplinary tool to use against the next upward rung of the working class, but, unlike the latter, without presenting a realistic demographic threat of subversion. To the extent that the indefinite growth in their numbers represents a threat at all, it is purely a security threat, not a political one!

"Lumpen" are erased from political participation, even where they are technically "allowed to vote" (eg, leaving aside disenfranchised felons). They are always the lowest of the "low propensity voters". Accordingly, their voices present no dangerous influences as they are not even heard in most informal media polling, since voter polls are almost always conducted using exclusively "likely voters".

But even as a "security threat", though, the lumpen proletariat can still be made useful, to the extent that robust mass media operations successfully enlist working class people in equal-opportunity vilification against them and the constant crime threat they pose -- and even more so, against "hug-a-thug" leftist politicians deemed insufficiently vigilant at suppressing that threat.

Thus, curiously, under Goldilocks conditions, reactionaries can best advance their own mass political prospects by the very same tactics that most enrich their bourgeois class allies, ie, accelerating inequality and austerity, which both enlarge the ranks of the lumpen class, while not so coincidentally accelerating wealth concentration.
