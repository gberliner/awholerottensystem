---
title: Knowing what to do
author: guyberliner
type: post
date: 2021-09-13T00:40:40+00:00
url: /2021/09/13/knowing-what-to-do/
categories:
  - Uncategorized

---
The reality is, no matter how long we study and work at something, we DON'T ever just "know what to do to achieve 'success'", at much of anything. Were that the case, then the vast majority of new businesses wouldn't fail. It can't just be that all those failures were doomed because of the flaws of their founders, which contradicts everything else we know about innovation. 

Instead, a massive amount of pure luck and unforeseeable accidents are involved in any long term endeavor, as a result of which, all the "business development experts" universally tell us to "fail early and fail often".

Which, platitude though it may be, is excellent advice. It does, however, overlook certain basic facts about the physical world. One of which is that an irreducible minimum Investment is required for success at any undertaking, however modest. In other words, spreading your failures out over a large number of low risk experiments, until you discover "something that works", is still a luxury only accessible to those who have a certain minimum amount of material resources. 

And increasingly, that means that, for most of us, the solutions to our problems, in a society where resources are becoming further concentrated in fewer and fewer hands over time, have to come about more through collective actions working with others and less through miraculous feats of rugged individualism.
