---
title: Bourgeois apologies for capitalism’s inherent instability
author: guyberliner
type: post
date: 2021-01-19T02:35:01+00:00
url: /2021/01/19/bourgeois-apologies-for-capitalisms-inherent-instability/
categories:
  - Uncategorized

---
Capitalism has passed through periodic crises, of varying magnitudes, in every epoch throughout its history, for hundreds of years since its inception in the late sixteenth and early seventeenth centuries, on roughly four to seven year cycles, a fact that Marxist economist Richard Wolff, for example, never tires of pointing out.



Observations for hundreds of years throughout the same period have been recorded by scholars about the realities of these crises, and those of every stripe, by no means limited to explicitly socialist or leftist ones. But the bourgeoisie controls a vast ideological machinery which, especially in the United States, more than most places, has succeeded in mounting the most intransigent possible blockades guarding against fully open, widespread public discussion and diffusion of these prosaic historical and contemporary facts.

Naturally, these effective blockades against public discussion critical of capitalism and against dissemination of such elementary historical facts are also among the most effective obstacles in the way of even mild and commonsense reforms.



Émile Durkheim cites late 18th and early 19th century reformist economist Simonde de Sismondi, disputing the assumptions of classical economists like Adam Smith and Jean-Baptiste Say, that supply and demand always transparently balance each other when markets are left completely to their own devices. Even today, de Sismondi&#8217;s clear and simple observations are entirely a propos, could practically have been written yesterday, and would be a mind-blowing revelation for most ordinary North Americans, if they were ever aired on network television. Here is a synopsis of de Sismondi&#8217;s arguments (from 1822!):



Contrary to Smith&#8217;s case for markets always being inherently self-correcting, when they expand beyond the provincial scope of individual entrepreneurs, cities, and even whole countries, and become international and even global, competition in them can emerge from unexpected sources. Any single producer must be constantly on guard against attacks by new competitors, and ceaselessly work to expand their own production and market share, as a hedge against such uncertainty. But this invariably leads to overproduction and market saturation sooner or later, as supply outstrips demand.

With no external mechanisms providing any overarching economic coordination or planning, such overproduction saddles producers sooner or later with inventories that they are forced to sell at a loss. When this condition becomes bad enough, they will attempt to shift their losses onto their workers, in the form of pay cuts or layoffs, and to avoid otherwise going under. But when this problem becomes generalized, it leads to a self-amplifying cycle, because workers themselves supply much of the economic demand in the first place (whether for the products of their own employers, or for those of others, but those others will also cut back their purchases of the original producer&#8217;s products, when profitable demand for their own products shrinks). This illustrates one way in which recessions and depressions spontaneously ensue and induce catastrophic downturns of an economy-wide scale.