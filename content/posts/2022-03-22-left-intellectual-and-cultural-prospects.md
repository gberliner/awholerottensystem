---
title: Leftist intellectual and cultural prospects
author: guyberliner
type: post
date: 2022-03-22T00:40:40+00:00
url: /2022/03/22/leftist-intellectual-and-cultural-prospects/
categories:
  - Uncategorized

---
Although there's no shortage of negativity to sample from when it comes to the subject of the cultural and intellectual prospects facing the political left, all is not nearly as bleak, comparatively speaking, as the direst prognoses constantly seem to suggest.

I actually agree with the more cheerful outlooks shared by intellectuals like Noam Chomsky, or politicians like Bernie Sanders. As both of them never tire of emphasizing, our own times and contemporary generations are vastly more enlightened, in myriad ways, than any previous ones.

So, on the one hand, while Marx's aphorism about how "the dead weight of all earlier generations weighs like a nightmare on the minds of the living" remains as true as ever, on the other, the cultural legacies of racism, sexism, and many other kinds of bigotry are much less powerful today, in many ways, than ever before. 

Ironically, the social progress we can detect today is perhaps most evident from examining the most un-selfaware examples of unreconstructed racism that still persist among us today.

For example, look at an opinion condemning "reverse racism" presumably written by some North American law enforcement officer or veteran recently. He argues that, "Usually one of the easiest ways to determine if something is offensive (or in this case racist) is to take the comments and flip the races." (https://www.lawenforcementtoday.com/anti-white-bigot-university-of-georgia-teaching-assistant-keeps-position/ )

A statement like this is surely a completely "innocent" expression of what must seem to be self-evident truth by the author, and yet would be painfully embarrassing to anyone with the tiniest inkling about the history of the past four hundred years of racial brutality and discrimination in North America. And we can tell it's sincere ignorance and not mere propaganda (although "sincerity is greatly overrated", as Adolph Reed, Jr puts it), because nobody deliberately signs up for embarrassment and humiliation, and there's no effort to hedge it around with any limitations or acknowledgements that would otherwise feel mandatory for anybody with even a third-rate education in any of the relevant subject matter today.

So although you can find a multitude of examples to argue that the political left is a "mess" today, the rightwing is much, much more of a mess. So much of a mess, in fact, that it is desperately fighting rearguard actions, shadowboxing imaginary demons, trying to ban "critical race theory", yet painfully aware that a new generation is already in its midst, one which has shown up by the millions in the past couple years in the biggest protests against police brutality and racial injustice in US history. And it doesn't have the foggiest clue how to "fight back" against this, other than try and slam the barndoor shut after the horse has already gotten out.
