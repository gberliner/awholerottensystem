---
title: The Bad Old Days
author: guyberliner
type: post
date: 2022-10-20T00:40:40+00:00
url: /2022/10/20/the-bad-old-days/
categories:
  - Uncategorized

---
Today, there are two "major parties". They are the neoliberal capitalists, on the one hand, and the ultrareactionaries and neofascists on the other. Neoliberal capitalists are the party of the "bad old days". Ultrareactionaries and neofascists are the party of the "good old days".

Reactionary, ahistorical narratives of "progress", which attribute it to various kinds of semi-divine "providence", or rational, meritocratic decisions by the economically and politically powerful, or the reliable prevalence of "enlightened self-interest", are omnipresent in bourgeois discourse.

The expression, "the bad old days", epitomizes these tendencies. The implications are that, once upon a time, admittedly, "bad things happened", but that was BEFORE there were "all good men at number ten" (to quote the lyrics of Tom Robinson Band's famous song, "I'm Alright, Jack"), BEFORE our blessed age of enlightened, rational technocracy and saintly "job creators", etc. Today, however, an entirely new vista of progress is upon us!

A "bad old days" (juxtaposed against the enlightened present) narrative, however, becomes increasingly untenable in this era of discredited post-modern capitalism and neoliberalism, and of declining living standards and falling life expectancies for many demographics in much of the industrialized world. Today, instead, the alternative, flipped-upside-down narrative is offered, of nostalgia for "the good old days", which were precisely those "bad old days" that neoliberalism once assured us were dead and gone!

Stories about both "bad old days" and "good old days", it should be obvious by now, erase the agency and voices of the vast majority of people, replacing them with a narrow sliver of what we call today "the one percent" (plus their much larger complement of managers and retainers in the next 9%).

Social progress, when it ever does happen, comes about because the people who stand to benefit from it make themselves heard, and demand it effectively, until they can no longer be ignored. It is never thanks to the grace and *noblesse oblige* of their social superiors. And it CAN be reversed, sadly, when conditions deteriorate to the point of jeopardizing the power of less advantaged groups to defend their own hardwon gains.

The truth is that the same basic dynamics prevailed then and now, in which class conflicts and related contradictions are mostly the crucial factors that determine outcomes. 

Thus, there have never been either "good old days" or "bad old days". There have only ever been days of class struggle at varying levels of intensity, accompanied by their corresponding advances and setbacks for the vast, working class majority.

Accordingly, convincing people to decisively reject all ruling class fairytales, whether about "bad old days" OR "good old days", is imperative.
