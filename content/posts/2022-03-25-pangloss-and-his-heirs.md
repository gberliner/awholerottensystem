---
title: Pangloss and his heirs
author: guyberliner
type: post
date: 2022-03-25T00:40:40+00:00
url: /2022/03/25/pangloss-and-his-heirs/
categories:
  - Uncategorized

---
It should come as no great surprise or mystery that social élites of all times and places are generally enamored of rosy depictions of social reality, and not keen on changes to it, or, if changes need to be made, they prefer them to proceed at a slow and stately pace. Voltaire famously lampooned such rose-colored outlooks on reality in his character of the philosopher, Pangloss, whose singular teaching to his student, Candide, in Voltaire's novel by the same name, was “All is for the best in this best of all possible worlds”.

Naturally, today, we have our own age’s Panglosses, as all ages do. And also, as usual, the attraction of any grand ideologies, theirs or others, always lies in some degree of verisimilitude: if they were too obviously false in all respects, they could never enjoy any imaginative purchase at all.

So, today, we have our Stephen Pinkers and Hans Roslings, et al (often, as in their cases, academics turned popular writers), who claim to “turn conventional wisdom on its head”, thereby unexpectedly revealing very optimistic and pleasing truths about our world. And they’re not necessarily entirely wrong in their accounts of the world.

In Pinker’s case, for example, he recites statistics to demonstrate that our own period of history is actually the “least bloody” – in terms of the tolls of mass violence and warfare, as a proportion of world population – of any in history.

In fact, it would be surprising were this NOT the case. Because the same factors that drove peasants to submit to feudal lords in mediaeval times – exchanging a degree of autonomy and freedom for a greater degree of safety and predictability – apply today as much as ever. And so today’s world has its own overlords, at various levels, who offer forms of security like a “rules based order”, world trade, etc, which client states agree to enjoy in exchange for pledging their own forms of tribute to today’s latest overlords: the United States, NATO, etc. And as power becomes more concentrated in the world, the attractions of these arrangements become ever greater and more unavoidable. And as always, the Pinkers and Roslings of the world exist to fulfill their normal ideological roles: papering over any contradictions and risks inherent in these tradeoffs.

Naturally, though, risk is never wholly eliminated and, thanks to other thinkers who actually specialize in technical risk analysis, like the economists Nicholas Nassim Taleb (of “Black Swan” fame), or Mark Blyth, we can still learn about the darker sides of this fairytale world that the professional optimists specialize in selling us on.

So, for example, we learn from people like Taleb that the normal practices of economists for ensuring stable and predictable profits, shared by both the “quants” hired by hedge funds and the nerds hired to manage the world’s biggest central banks, amount to “deftly picking up spare change off the ground in front of moving steam shovels”. And from people like Blyth that their equations, like the famous “Black-Scholes-Merton”, are no more than trivially correct descriptions of routine social practices and understandings about how to conduct normal business today (but only hold true precisely when business *is* normal!).

The implications of insights like these are that the most consequential insights and predictions about the world completely elude conventional thinkers. Like that of biologist Stephen Jay Gould, who emphasized “catastrophism”, ie, the insight that biological evolution proceeds far more via monumental and unpredictable ruptures than it does by slow and steady, easily observed trends, the real history of human reality resembles Lenin’s description far more than Pangloss’s: “There are decades when nothing seems to happen, and there are weeks where decades happen.”

It seems abundantly clear that we are living through precisely one of those sorts of inflection points today. Right now, or very soon, none of the conventional rules will continue to apply.

So many novel factors are emerging and converging today that promise to be of transcendent importance to the future of humanity, on timescales of millennia and tens of millennia, not just centuries. It should hardly be necessary (nor is it even possible) to enumerate them exhaustively (climate chaos, nuclear proliferation, economic turmoil associated with unprecedented levels of globalized communications and interdependence, etc), any single one of which has the potential to upend the world for every human being today and for countless future generations.

Almost anything else we can talk about today feels utterly trivial and banal by comparison. Of course, the fact that we are forced to continue talking almost exclusively about the trivial and banal, instead of soberly contemplating the enormity of our present moment, bodes ill for us all.
