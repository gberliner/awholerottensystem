---
title: A leftist Tea Party
author: guyberliner
type: post
date: 2021-11-08T00:40:40+00:00
url: /2021/11/08/a-leftist-tea-party/
categories:
  - Uncategorized

---
I have lost count how many times I have heard somebody coming from a leftist orientation sing the praises of the reactionary “Tea Party” and bemoan the lack of (or call for the creation of) a “Tea Party of the left”. But more remarkable than that, in none of these paeans to the supposed manly political virtues of the reactionary Tea Party have I ever run across any concrete materialist analyses attempting to explain its successes, and which of those factors it might actually be possible to reproduce. 

For all anyone reading such praise for their “strength” and “discipline” might guess, it must be attributable to some very metaphysical or spiritual qualities of that movement itself and its participants. 

But actually, the explanations are staring us all in the face, if we cared to spend just a moment to look into the matter.

The “Tea Party”, above all, was partly a genuine “grassroots” phenomenon, but mostly a Koch brothers astroturf machine. And this had a whole lot to do with its successes. Now, unfortunately, at a superficial glance, most would stop right there and conclude, “well, obviously we don’t have any ultrarich sugardaddies in our court, and wouldn’t really want them anyways, so there’s nothing to gain from studying the Tea Party that could be repurposed for our (leftist political) purposes.” 

But wait, not so fast!

What the Kochs (principally, Charles) achieved with the Tea Party was something akin to “democratic centralism” – ie, imposing something like strong, centralized party discipline on numerous, formally independent candidates and other political actors. And all without ever actually becoming a formal political party with its own ballot line. A kind of discipline otherwise notably lacking entirely from US political life.[^*]

In other modern democracies, this kind of discipline is less glaringly absent, for concrete structural reasons. Notably, even when countries lack proportional representation, they often still enjoy certain advantages provided by it, especially the existence of large blocs of seats up-for-grabs during every election, a benefit of true national elections, instead of the highly balkanized structure found in this country, with its numerous staggered terms, “off-cycle” elections, and so on.

These and other factors make it prohibitively costly and inefficient for parties in the United States to field and support slates of true party candidates. Instead, in this country, each candidate runs an independent political “enterprise of one”, complete with their own fundraising machine, consultants, etc, and simply CANNOT depend primarily on any single organization, party or otherwise, for their support. And “parties”, in this country, perform few if any of the functions that parties in other countries, properly so-called, routinely do perform – imposing “party discipline” being chief among them.

The Kochs, on the other hand, singlehandedly managed to duplicate in-house many of these true party functions – thanks, obviously, to unlimited financial resources – and extended all these and other forms of assistance to their favored candidates. They also exercised a single point of financial control. This explains why, even on the one notable occasion when the so-called “Freedom Caucus” of Tea Party alumni in Congress (originally presided over by Mark Meadows) advanced a proposal adamantly opposed by Charles Koch (the so-called “Border Adjustment Tax” in 2017), no sooner did Koch catch wind of it than it was quashed without a further mention in the next draft of Trump’s so-called “Tax Cuts and Jobs” Act.[^*]

Admittedly, it would be a tall order to reproduce all of the machinery the Kochs managed to develop, especially given the unlimited financial resources at their disposal. However. the left HAS proven it can field winning candidates, funding them entirely with “small dollar donations”. Unfortunately, a byproduct of our much praised “small dollar donation” model is that it makes our resulting officeholders structurally indistinguishable from the rabble of hyperindividualists produced by “large dollar”, “medium dollar”, or any other elected leaders. Ie, they are ALL still just “political entrepreneurs” running “political enterprises of one”, no matter how high their ideals (or how low their average donations) might be.

This explains why they are incapable of conducting disciplined political battles as a solid front in Congress. I believe AOC herself once said to a newer “squad” member this past year, “at the end of the day, we all still just vote individually [come rollcall]”. “So what exactly was the point of having a ‘squad’ again??” many were heard to ask in response.

But, while it might be hard to fully reproduce the Tea Party machinery, we might still attempt (at least if we understand it and its strengths) to emulate parts of it. We might, for example, actively encourage people to REDUCE their contributions to individual candidates, and greatly INCREASE their contributions to disciplined political organizations whose principles we agree with. We might even encourage people to ask CANDIDATES, “is there an organization who is supporting your campaign to which I might be able to contribute?”

By now, everybody knows the slogan, “get organized!”, but we hardly apply that principle at all to actual candidates running for political office! But, understanding the valuable role of party discipline in achieving concrete victories, we might start to do so. We might even start to deprecate the bourgeois, hyperindividualist style of politics that has thus far been imposed on and accepted by our currently favorite leftist political leaders. And if those leaders feel sufficiently pressured to do so, they themselves might take the initiative to actively encourage a different style of politics (eg, beginning with encouraging more of their supporters to support ORGANIZATIONS instead of individual celebrity cults of personality).

Seen from this perspective, incidents like the infamous “blacklisting” carried out by the Democratic Party against consultants supporting primary challengers would be seen as a blessing in disguise. So much the better if leftists are forced to depend on the services of separate, parallel campaigning infrastructure – provided they can figure out how to duplicate those services themselves, which doesn’t seem like an insurmountable feat for a movement that took seriously the challenge of emulating a “Tea Party of the left” as described here.

Here’s a wild idea: Imagine if a half a dozen major national unions, single interest advocacy organizations, and high profile activist groups formed a coalition (for the sake of concreteness, let’s say ILWU, NNU, Teamsters, PNHP, People’s Action, and Sunrise Movement), perhaps calling itself the “Workers Democracy Project” (WDP). Suppose they all jointly agreed to set aside between one third and one half of each of their planned yearly budgets for political lobbying and contributions and dedicate it to the coalition instead. 

This organization's mission would be to support viable candidates similar in orientation to a PAC like Justice Democrats, but it would provide matching funds up to any legal limits to them, and a host of supporting services, including access to low or no-cost group health insurance for working class candidates without resources to pay out of pocket, and naturally, expert training from experienced campaigners. It would recruit campaigners with proven experience in recent campaigns, people like India Walton, Nabilah Islam, etc.

But, in exchange for all these services to new candidates, this coalition would require the candidates accepting its support to fundraise exclusively through it, which might act like a clearinghouse similar to Actblue. (Maybe the coalition would even still use a service like Actblue initially, rather than reinventing the wheel.) It would also require extensive control over messaging and policy on key issues of shared concern to the coalition, such as labor, healthcare, climate, etc. And it would expect candidates to participate in active consultation at all points on these sensitive interest areas, both before and after entering office.

Finally, it would require pledges from candidates receiving its help to “tithe” a substantial portion of their future salaries earned from elected office in excess of, say, twice the proposed new federal minimum wage (ie, $15/hr) back to the coalition, minus any reasonable per diem expenses inevitably incurred by officeholders like members of Congress (eg, costs of transportation, renting a second apartment, etc). 

This last requirement would serve both the immediate practical purpose of building up resources for the coalition, and highlighting a clear commitment to working class politics and a key differentiator setting its candidates apart from the political crowd, vital for building a strong new political “brand”. (In Seattle, Kshama Sawant, notably, has advocated something like this, and reportedly practices it already, by voluntarily foregoing a large part of her salary as a city council member, tithing that money back to her political party, Socialist Alternative, instead.)

An organization like this (with or mostly WITHOUT any ballot line of its own!) could start to reproduce some of the strengths of a genuine political party in this country.

[^*]:    For an excellent summary of how Charles Koch built his political empire, and in particular, how the Tea Party fell under its umbrella, see Christopher Leonard, “Kochland” (2019)
