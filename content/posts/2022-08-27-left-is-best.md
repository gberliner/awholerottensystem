---
title: Left is Best
author: guyberliner
type: post
date: 2022-08-27T00:40:40+00:00
url: /2022/08/27/left-is-best/
categories:
  - Uncategorized

---
"Left vs right" as an enduring, dominant political metaphor is undoubtedly a gross reduction and simplification of reality that occasions endless confusion. But while I am sympathetic to such reservations, nevertheless, I have not yet seen a way to entirely supersede it.

In fact, one of the best giveaways about someone's intentions often consists of claims that they aim to "move beyond left-vs-right". And, if one sympathizes more with the "left" side of the metaphor, those intentions are probably bad news.

Alternative formulations like "above vs below" are not inherently bad, but they still suffer from plenty of inexactitude.

For my purposes, I will use the late Michael Brooks's ad hoc definition of "leftism" as "ruthless critique of dominant institutions, combined with enormous compassion for individual human beings".

A "left" critique always partakes of these qualities: 

Recognizing... 

* That human affairs transcend individuals; 

* That, on average, events owe themselves as much if not more to systematic and historical factors as purely individual actors; 

* That the understanding of these factors requires public education; 

* That class divisions often play a decisive role in events throughout society; 

* That the working class has a special role to play, because it occupies a unique position in modern capitalism, being the one class capable of challenging the power of a sometimes seemingly all powerful bourgeois oligarchy; 

* That, in classical Marxist terms, which still retain enormous relevance, the working class is defined by its relationship to the means of production, ie, it does NOT own and fully control them, despite being crucial to the process of production, and despite constituting a numerical supermajority of the population;

* That the working class constitutes the vast and constantly increasing majority of humanity, and that working class interests are increasingly interchangeable with the interests of humanity as a whole.

Admittedly, this formulation leaves out important considerations. In particular, it does not consider minority populations worldwide, especially indigenous people. That remains a major missing piece, or blind spot, in leftist thought.

Solidarity has to become the glue that cements these missing pieces into place. And solidarity must have a material, and not just idealistic basis, as Eugene Debs famously emphasized.

If we read accounts like EP Thompson's "Making of the English Working Class", for example, we can start to see how evils like race discrimination are lineally descended from noxious, feudal myths like the supposedly "noble blood" of aristocrats and the "divine rights" of kings. How the treatment of the "lower orders" of society by hereditary aristocrats presaged the treatment later meted out to colonized peoples at the hands of colonial and imperialist powers.

It's not as if a "left-right" metaphor is the only valid way of conceiving reality, nor has any more inherent truth than all others. But, if we agree that humanity requires collective struggle to avoid its own worst impulses, and to direct its collective energies in life affirming directions, then we need a paradigm for describing these collective efforts. And so far, the "left" pole of the notional "left-right" division is the best I have seen.
