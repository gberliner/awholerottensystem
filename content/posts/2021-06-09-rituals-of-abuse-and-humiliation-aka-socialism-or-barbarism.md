---
title: Rituals of Abuse and Humiliation (aka, “Socialism or Barbarism”)
author: guyberliner
type: post
date: 2021-06-09T00:40:40+00:00
url: /2021/06/09/rituals-of-abuse-and-humiliation-aka-socialism-or-barbarism/
categories:
  - Uncategorized

---
Psychiatrist Aruna Khilanani raised a lot of eyebrows with her frank admissions of impotent rage at seeing the spectacle of racism in our society, and her (heretofore) secret fantasies of murdering random white people (https://www.facebook.com/RTnews/posts/10160178226674411).

  
Her fantasies admittedly sound pretty nasty, and her frankness about them was possibly imprudent. To which I say:

  
If people who are constantly persecuting the homeless were perfectly honest like she was, I bet that&#8217;s what they&#8217;d say about the poor. If 90% of Republican voters were similarly perfectly honest, that&#8217;s what they&#8217;d say about Black folks. Etc, etc. When a society has built itself around a universal pattern of ritualized contempt and humiliation, who can sincerely claim to be surprised that the principal intended targets of it are sometimes inclined to return the favor?

  
Whereas, if you enjoy the distinction of belonging to the group James Baldwin referred to as &#8220;people who think they are white&#8221;, you also have the luxury of genteelly enjoying from afar the spectacle of abuse and humiliation enacted by others daily on your behalf, and can get vicarious gratification from it and save yourself the messiness and trouble of actually being the one who has to deliver the baton blows, roust the homeless camps, evict the single mothers late on their rent, etc. Kind of like Hamas and the IDF: if someone would give them the same precision munitions as the Israelis use, I bet they&#8217;d be delighted to stick to a more precise and antiseptic form of mass killing.

  
In short, if we want this lady to be more measured and polite in her remarks, one way might be to hire more non-white officers and let them beat, taze, and senselessly kill much more proportionate numbers of whites, especially &#8220;middle class&#8221; ones. Then she too could get an even share of the vicarious pleasure of watching the spectacle of abuse and humiliation of others, and would have no interest in imagining having to directly dirty her hands with the messy business herself.

  
Or, alternatively, we could enact the kind of thoroughgoing social democratic reforms here in this country that we are so uniquely distinguished among all industrialized countries for lacking, as Usmani and Clegg discuss in their Catalyst Journal article, &#8220;Economic Origins of Mass Incarceration&#8221; ([https://catalyst-journal.com/…/the-economic-origins-of…/][1] )

  
But, hey, &#8220;better-dead-than-red&#8221;, right?

 [1]: https://catalyst-journal.com/2019/12/the-economic-origins-of-mass-incarceration/