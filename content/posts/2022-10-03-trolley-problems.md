---
title: Trolley problems
author: guyberliner
type: post
date: 2022-10-03T00:40:40+00:00
url: /2022/10/03/trolley-problems/
categories:
  - Uncategorized

---
Essentially all apologies for capitalism boil down sooner or later to one species or another of "trolley problems". ("Trolley problems" are a peculiar type of moral or ethical thought experiments in which one is called upon to embrace or justify a "solution" to a moral dilemma that optimizes outcomes in some way (minimizing harm or death to the greatest number, or maximizing the well being of the greatest number), while willingly suffering the intentional infliction of an immediate harm to a smaller number, in particular, a harm which shocks the "naïve" conscience. (https://en.wikipedia.org/wiki/Trolley_problem)

I must hasten to add, though, that this kind of dilemma is not unique to capitalism, but can arise in essentially all complex human social arrangements.

The "solution" to such problems, then, requires rejecting the terms of the problem itself, and the state of affairs that led up to it. ("Thinking outside the box", if I may be pardoned for using an overly trite expression.) Because organizing our affairs in a way that requires "moral injury" to ourselves, in the very course of making supposedly "optimal" decisions, is simply wrong. (https://www.psychologytoday.com/us/basics/moral-injury)

(Ironically, "conservative" thinkers, who are also usually strident apologists of capitalism,  make exactly the same accusations, commonly with little self-awareness, against leftist thinkers, ie, that the latter are guilty of uncritical acceptance of "relativism" and "utilitarianism"!)

Historian EP Thompson did not use such terminology in his relentless criticism of early neoliberal revisionist historians of the Industrial Revolution. But in his greatest book, "Making of the English Working Class" (1968), he surely had this kind of repudiation in mind, when he dedicated that work to "rescu[ing] the poor stockinger, the Luddite cropper, the 'obsolete' hand-loom weaver, the 'utopian' artisan, and even the deluded follower of Joanna Southcott, from the enormous condescension of posterity".

(These neoliberal revisionists contended, via various statistical sleights of hand, that the Industrial Revolution was an unquestionable good, actually, and that, whatever the suffering it inflicted on masses of people, in the course of its titanic convulsions, it nonetheless redounded, "on average", greatly to the benefit of most of the population of the first half of the 19th century.)

It is my contention that we must constantly challenge and forcefully reject the very discourse of "trolley problems" as a routine practice. If leftist thought falls into exactly the same trap, then we will be guilty of exactly the mistake that thinkers like the Christian anarchist Jacques Ellul warned of. Ellul pointed out that socialists and communists became very good over the course of the 20th century at coming up with systems of reform and revolution that expertly devised solutions to certain problems of capitalism, only to create whole new and equally intractable problems of their own.

Accordingly, we ought not to hew too closely to the later rhetoric of Marx and Engels touting "scientific socialism", which easily becomes exactly the kind of "technic" Ellul warned about. Instead, socialism must be regarded more like a kind of humanistic social wisdom of the sort that economist EF Schumacher famously called for in his classic book of "economics-as-perennial-philosophy", "Small is Beautiful" (1973). (https://en.wikipedia.org/wiki/Small_Is_Beautiful)

Schumacher warned against the conceit of "construct[ing] a political system so perfect that human wickedness disappears and everybody behaves well, no matter how much wickedness there may be in him or her."

Therefore, however complex our society may already be or may become, I nonetheless contend that we should aspire to construct a society in which as many people as possible are capable of sophisticated moral reasoning, a reasoning that rejects facile "solutions" to problems. A simplistic "greatest-good-for-the-greatest-number" sophistry can easily degenerate into a kind of demagogic tyranny of the majority. 

Whether such a hypothetical majority even really stands to benefit from such a "trolley problem" solution may always be subject to debate. But what's less debatable is that a society that opts for "trolley problem" solutions in the first place is already one that's guaranteed to fail in upholding fundamental, universal values, including that Enlightenment triad, of liberty, equality, and solidarity.
