---
title: Socialism or Barbarism
author: guyberliner
type: post
date: 2022-08-27T00:40:40+00:00
url: /2022/08/27/socialism-or-barbarism/
categories:
  - Uncategorized

---
 · 
We're in this crazy sort of situation, where Rosa Luxemburg's prophetic motto, "socialism or barbarism", has gone from being a literary shorthand to a literal historical crossroads. Where the necessity of large scale coordination and planned intervention in a globalized economy has become an urgent, blindingly obvious necessity for human survival. Blindingly obvious, that is, for people with even a passing acquaintance with the facts. (Of course, not everybody possesses that.)

But what happens when an unstoppable force meets an immovable object? What happens when blindingly obvious physical reality comes crashing into cherished beliefs about social reality and one's place in it? Naturally, only more and more bizarre, outlandish theories can then ward off the otherwise obvious conclusions that present themselves. Whence the rise of an age of ever wilder and more feverish conspiracy theories.

And the complexities of modern life nourish the survival of wild and outlandish theories, in an age where democratic institutions are profitably and systematically eroded, including academic and media institutions.

I suppose the odd religious quietist will tend to write all of this off as some kind of fateful product of "the Dharma" or "human nature" or something. But the truth is that NOTHING about the bizarre crisis we find ourselves in today was truly inevitable. All of it was contingent. All of it avoidable, or capable of substantial mitigation, but for only a small number of changes, or different decisions, by a ridiculously small number of powerful individuals.

Now I suppose that I myself might be accused of sounding like a conspiracy theorist! And yet, read a book like Christopher Leonard's "Kochland", and it's hard to avoid the conclusion.

For example, by 2010, Charles and David Koch had cemented a virtually unassailable stranglehold over the US Republican Congressional Caucus. And, while the Kochs opposed any climate legislation, in the face of Obama's ambitious carbon emissions reduction proposals then before Congress, their own top advisor on climate policy prepared a comprehensive brief of proposed decarbonization plans that demonstrated how Koch Industries could still reposition itself as a dominant player in a new, post-fossil fuel economy.

But Charles Koch, for his own reasons, simply chose to shelve and sideline all of it, in favor of "pedal-to-the-medal" climate denial, and buying off or crushing any GOP Congress members who stood in his way.

