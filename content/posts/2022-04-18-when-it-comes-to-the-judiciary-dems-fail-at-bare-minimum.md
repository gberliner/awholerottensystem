---
title: "When it comes to the judiciary, Dems fail at bare minimum"
author: guyberliner
type: post
date: 2022-04-18T00:40:40+00:00
url: /2022/04/18/when-it-comes-to-the-judiciary-dems-fail-at-bare-minimum/
categories:
  - Uncategorized

---
It's amazing how Congressional Dems fail to do the absolute, bare minimum they could to rein in GOP abuses by federal judicial appointees, who since Reagan increasingly include disproportionate numbers of grossly unqualified, rank partisan political hacks.

The "bare minimum", I have pointed out before, would be invoking the Constitution's Article 3, Section 2, which empowers Congress to prescribe the precise constraints on who and which cases enjoy standing to pursue remedies in the federal courts, and under which conditions.

Even a barely functioning majoritarian party would be routinely deploying this weapon to rein in flagrantly unruly judges. Even the credible threat of it would work well against them, since lower court judges enjoy pretty cushy jobs, with well paid lifetime appointments, and the more corrupt and partisan the judge, the less likely he or she would be to want to court the one sanction that could lose them their seats, ie, impeachment, with respect to which an excessively routine history of issuing rulings overturned on appeal would increase their unfavorable profile and vulnerability. 

On the other hand, the Supreme Court, while highly unlikely to ever face impeachment, has a much stronger incentive than lower courts to maintain its own power and legitimacy as the ultimate guardians of judicial branch power, and so is unlikely to invite open warfare with the legislative branch, if it were ever subject to even a modicum of discipline by the latter, even the threat of which has so far been almost entirely absent.
