---
title: Hes a fighter
author: guyberliner
type: post
date: 2022-01-06T00:40:40+00:00
url: /2022/01/06/hes-a-fighter/
categories:
  - Uncategorized

---
If there's one recurring, money line that sticks with me whenever I hear anythng from his fans about Donald Trump, it's the one: "he's a fighter!"

The precise contents of what he's "fighting" over are purely secondary, although everybody has a rough idea of a few of the outlines, of course. But the very fact that he is somehow raw, and "authentic" (even if only about as much as a pro-wrestler -- another spectacularly popular corporate media spectacle, obviously!), and that he even has enough blood pressure to be "fighting" about anything at all, is something that clearly excites a lot of his fans.

This combative, "fighting" spirit can and does help Trump win fans, even if only in marginal numbers, from surprising demographics. He signififcantly outperformed other, more centrist Republicans, like Mitt Romney, among many minority groups, including African American men, Latinos, etc. In close elections, peeling off small numbers of these surprising fans in his direction can make all the difference in the final outcome.

As famous Republican pollsters always explain to the candidates who consult them, "strong and wrong" always beats "right and mild" any day. Especially in the age of modern media, where "the medium is the message", as Marshall McLuhan said, and this remains as true as ever, even including online, social media, with its "engagement" algorithms. The winning formulae in all of them are the ones that short-circuit any discursive, logical thought, but drive strong emotions instead. "Engagement" means attention. Attention means revenue. Full-stop.

If Trump's listeners and viewers can at least vaguely identify and resonate with some of what Trump is "fighting" -- and a vast, fully funded far right corporate media infrastructure, fully captive to the GOP, is there to see to that, no real parallel to which exists on the Democratic side -- it is enough.

The fact that Democrats issue empty promises, mouth endless platitudes, and really do appear to stand for practically nothing other than being "not Trump" is incredibly perilous for the future of any kind of democratic governance in the United States.



