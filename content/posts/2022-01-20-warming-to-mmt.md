---
title: Warming to MMT
author: guyberliner
type: post
date: 2022-01-20T00:40:40+00:00
url: /2022/01/20/warming-to-mmt/
categories:
  - Uncategorized

---
After some exposure to the thinking spotlighted at "Money on the Left" blog and associated podcasts (especially, "Superstructure"), I've begun warming lately to Modern Monetary Theory (MMT).

One parallel that really struck me with MMT was the critique I've seen of various leftist advocates of much stiffer taxes on the wealthy, pointing out how, for example, Jeff Bezos has a "net worth" of such-and-such amount, and how, if you just forced him to sign over X% of it towards goal Y, goal Y could be fully achieved, while still leaving Mr. Bezos with (100-X)%, where 100-X would still leave him the richest man in history. The problem, critics of these talking points point out, is that much of Mr. Bezos's wealth is "notional", ie, based only a speculative valuation of his equity stakes in certain enterprises (above all, Amazon Corporation, of course).

It's a fair point. And it actually helps to illuminate the principles of MMT, too. Indeed, it could well be said that capitalists are already past masters of their own form of a sort of "MMT" -- ie, essentially issuing themselves speculative "credit" on the strength of expected future returns on existing, real business assets and organizations. For what else is MMT than exactly that, ie, the issuance of credit for worthwhile projects, based on expected economic and social returns consequent to their successful accomplishment?

For the benefit of anyone not following left debates over MMT, I will just mention in passing one or two of the objections that are commonly expressed to it, namely, that it obscures the importance of class conflict and actual changes in social relationships that are required, and perhaps offers a kind of false or misleading hope that such unpleasantness might somehow actually be avoided by some kind of financial trickery or other.

MMT proponents, on the other hand, counter that MMT is actually in some ways the more profound critique of capitalism than a simple class struggle approach, because purely focusing on class struggle runs the risk of falling straight into the trap set by bourgeois ideology, ie, boilerplate arguments like: "you commies are just thieves who don't know how to accomplish anything yourselves, so all you want to do is steal from others!" When we allow "class struggle" to just be defined as things like straight wealth transfers, we inadvertently lend way too much credence to these bourgeois arguments.

Whereas, if we use the credit system to achieve our same goals, we make it clear that there is no secret, brainy magic exclusive to the bourgeoisie going on here.

Naturally, of course, the unlimited expansion of credit *is* inflationary, as even MMT proponents will readily admit. At some point, discipline does have to be imposed on the credit system, and this discipline must, for our purposes, be imposed on capital's side of the ledger, in the form of something more or less equivalent to taxes, or "fiscal policy" broadly speaking -- as opposed to being inflicted on the working class, in the form of austerity, wage repression, etc.
