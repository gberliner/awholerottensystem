---
title: “Conservative” Kakistocracy
author: guyberliner
type: post
date: 2021-04-21T18:45:07+00:00
url: /2021/04/21/conservative-kakistocracy/
categories:
  - Uncategorized

---
The political movement commonly called &#8220;conservatism&#8221; is arguably better called &#8220;kakistocracy&#8221;. Which is a shame, since &#8220;conservatism&#8221; and &#8220;conservation&#8221; are cousins as words go, so a true conservative philosophy could have much to recommend itself. The peculiar aspect of the philosophy of political &#8220;conservatism&#8221; as we know it is, it inverts and relocates the space about which we are to be cautious and non-interventionist.  
  
So whereas &#8220;conservation&#8221; implies taking care of the natural world, especially the living biosphere upon which humanity has depended for its survival since time immemorial, &#8220;conservatism&#8221; concerns itself with non-intervention (aka, &#8220;laissez faire&#8221;) in the sphere of human affairs, especially the distribution of wealth and power, and great hesitance as regards regulating any of the affairs of the powerful in society. Why this can be called &#8220;kakistocracy&#8221; should start to become readily apparent.

Because for conservatism, the space of concern and veneration for the supposedly spontaneous unfolding of &#8220;nature&#8221; is NOT the biosphere at all, but strictly human activities that are often acutely in need of regulation. Under &#8220;conservative&#8221; political regimes, one needn&#8217;t worry about the depredation of literal wild wolves, upon whom open season can be declared without compunction, but we must constantly worry about the depredations of figurative human wolves (at least those who have enough money to make them legally untouchable).

Maggie Thatcher expressed this philosophy perfectly when she declared that &#8220;There is no society, there are but individuals and their families&#8221;. This gives away the game because, if there is no &#8220;society&#8221;, whatever can the use of government be, anyways? Clearly not to serve the interests of the majority (aka, the &#8220;society&#8221; whose existence she denied). In which case, government serves only to protect the already powerful from any combinations against them by the weak. It it is the philosophy of &#8220;do what thou wilt&#8221; applied to the political realm.