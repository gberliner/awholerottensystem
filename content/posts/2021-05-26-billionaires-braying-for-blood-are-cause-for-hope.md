---
title: Billionaires Braying for Blood are Cause for Hope
author: guyberliner
type: post
date: 2021-05-26T16:48:58+00:00
url: /2021/05/26/billionaires-braying-for-blood-are-cause-for-hope/
categories:
  - Uncategorized

---
If it remains the case, as John Dewey once said, that &#8220;[American] politics consist of the shadows cast by big business&#8221;, then paradoxically, what we see currently is a bit unusual and even cause for hope. Despite being a tiny segment of the population, the bourgeoisie (aka, &#8220;one percent&#8221;, aka, &#8220;ruling class&#8221;, etc) are more fragmented and conflicted than ever, agreeing on little else than one overriding point: that their class should survive and remain in charge, of course. So, far from being a cause for lament, the much bemoaned political &#8220;division&#8221; we witness today should probably be seen as largely salutary, if not celebrated.

Among the numerous factions of the ruling class, the most notorious and disagreeable one is, of course, what I refer to as the &#8220;Billionaires Braying for Blood&#8221; (or &#8220;BBB&#8221; for short), as exemplified by the far right media empires of Murdoch&#8217;s Fox, Sinclair, etc. The BBB phenomenon is something historically unusual and believe it or not, despite their power, more a sign of weakness than strength.

Historically, the bourgeoisie was able to rely more on the &#8220;transparent blinders&#8221; role of ideology for solidifying its cultural and social hold on power. The function of ideology is supposed to be a form of &#8220;internal policing&#8221; that transparently controls and modulates the behavior of the vast working class majority, largely without external intervention. It is only because that was starting to visibly break down in the 1960s and seventies that a segment of the &#8220;billionaire class&#8221; (as exemplified iconically by the infamous &#8220;Powell Memo&#8221;) resolved to intervene directly in academia, politics, and culture generally, in order to right their perceived-to-be-listing ideological ship.

Fast forward five decades, and it might seem that the power and ferocity of the bourgeoisie is more unassailable than ever, and owes much to the success of the project that the Powell Memo outlined for its current vitality. In reality, though, many elements in both the base and superstructure of our society are quite obviously breaking down, and there is little or no agreement within the bourgeoisie about what to do in response. Its own favored laissez-faire ideology (largely shared by both &#8220;liberal&#8221; and &#8220;conservative&#8221; elites) also largely precludes any of the necessary state-sponsored interventions that would be necessary to seriously confront any of the crises facing modern civilization.

Meanwhile, we see phenomena like the Texas legislature attempting to legally prohibit the teaching of &#8220;Critical Race Theory&#8221;, or literally any analysis of the fraught history of race relations in the United States in public schools at all! So somebody has clearly struck a nerve (I&#8217;m looking at you New York Times!), and it&#8217;s clear evidence of the disarray and a desperate rearguard maneuver among the BBB faction, in the face of the onslaught of movements like Black Lives Matter.

Of course, other factions of the bourgeoisie have reconciled themselves to the irreversible tide of certain social movements like the Black liberation struggle, and instead are looking to co-opt them via strategies like &#8220;corporate race relations&#8221; (eg, Robin DiAngelo&#8217;s &#8220;white fragility&#8221; classes adopted by HR departments across the country).

George Monbiot refers to rightwing parties like the Republican Party in the USA as the &#8220;warlord faction&#8221; of the bourgeoisie, ie, the most megalomaniacal and narcissist segment of the ruling class. These are folks who really do believe in what Chomsky calls &#8220;divine rights of capital&#8221;, take seriously conceits like Trump&#8217;s about their having &#8220;good genes&#8221;, and really do believe that the rest of us at lower economic stations than them are a rabble of subhuman scum, whom they deeply resent having to submit to in any way. (Whence their disdain for &#8220;democracy&#8221;, rage at having to pay taxes, etc.)



The problem for this segment of the bourgeoisie is that, politically, there isn&#8217;t any natural voting constituency in favor of anointing billionaires as the inheritors of feudal lords and kings, or at least not one large enough to ever win any elections practically anywhere.



Consequently, it has always had to rely somewhat on &#8220;wedge issues&#8221; to remain a going concern. And in the United States, race has always played a central role in that regard. Today, the &#8220;culture war&#8221; more broadly does that for them.

In some ways, this looks like a smashing success, at least momentarily. And although the civil war among the bourgeoisie over these issues is real in some ways, nonetheless, as Adolph Reed, Jr has pointed out, these divisions between them, to the extent they can successfully mirror them onto the 99% of the population who don&#8217;t really have a material dog in such fights, definitely serve their interests as a class in distracting the rest of the population from their power (or more importantly, our own).



But longer term, there are real warning signs flashing for them. In a recent interview with Paul Jay, economist Michael Hudson grappled with the question Jay posed as to why Biden refuses to honor his own campaign promise about writing down student debt. As Hudson points out, this is largely debt owed directly to the government that would not in fact cost the private sector anything were it cancelled (except to the limited xtent that private interests who are responsible for servicing it would lose some service fees as a result).



Hudson&#8217;s answer was in part in agreement with Jay&#8217;s suggestion that it represented &#8220;the threat of a bad example&#8221; &#8212; ie, if the proletariat were to ever get the idea that debts could be cancelled, the jig would be up for the ruling class as a whole, which since the dawn of financialization, especially in the USA, has become more and more dependent on rentier income for its survival. The theory being, &#8220;give them a finger, they&#8217;ll demand a hand, give them a hand, they&#8217;ll demand a whole arm&#8221;, etc.



Hudson says that the problem with this, however, is that it dooms us to a metastasizing economic crisis without any viable tools to control it. Debt deflation kills demand and leads to an irreversible cycle of economic collapse, as demonstrated by many historical examples. Hudson notably cites the Roman empire as one of them.