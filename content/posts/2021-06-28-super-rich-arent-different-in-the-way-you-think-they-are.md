---
title: The super-rich aren't different the way you think they are
author: guyberliner
type: post
date: 2021-06-28T00:40:40+00:00
url: /2021/06/28/2021-06-28-super-rich-arent-different-in-the-way-you-think-they-are/
categories:
  - Uncategorized

---
The very rich are not "different from you and me" in any of the esoteric, metaphysical ways that F. Scott Fitzgerald's quote is sometimes taken to imply, and certainly not in the ways in which they are themselves indoctrinated to believe about themselves. Actually, in some ways, they are in danger of being morally crippled by their own  "good fortune".

Our civilization is largely constructed on the basis of ensuring a high degree of continuity of inherited wealth and power. Formerly, though, for a fifty year period or so during the middle of the 20th century, anti-trust laws, banking reforms, and progressive taxation actually reined in the power and ability of the very rich to perpetuate dynastic wealth, Chuck Collins tells us in his interview with Paul Jay about his recent book, "How Billionaires Pay Millions to Hide Trillions".

But today, the Progressive Era and New Deal reforms that momentarily reined in the power of great wealth have been eroded down to practically nothing. This ruthless attrition has been accomplished thanks to the relentless war on them carried out, not just by the Big Bourgeoisie alone, but above all, Collins tells us, by their "coordinator class" handlers, high priced lawyers, lobbyists, and others who profit munificently from the backwash of massive wealth inequalities.

The latter socioeconomic group, while not necessarily themselves "über-wealthy", commonly feel an even more intensely visceral urgency to protect great wealth than the super-rich themselves do. Collins says he even knows very rich people who have had to twist their own financial advisors' arms to stop them from exploiting tax loopholes these rich taxpayers came to regard as abusive and immoral.

Unfortunately, though, people with such moral scruples are the exception rather than the rule. The rule is, the very rich tend to be indoctrinated into a bizarre kind of aristocratic ideology equating "responsibility" with preserving dynastic wealth. It's a kind of warmed over retread of feudalism, in which they are fulfilling a supposedly noble obligation to make sure that "the better sort of people" (ie, their kin) remain on top of the pyramid. Which makes it hard for them to respond to any moral claims made by the mass of humanity, no matter how horrific the consequences portend to be for all future generations.

Because today, as Thomas Piketty points out, extreme wealth concentration threatens not only democracy, but the future habitability of our planet (https://www.lemonde.fr/blog/piketty/2019/06/11/the-illusion-of-centrist-ecology/)

https://www.youtube.com/watch?v=p1waHr7qhuE
