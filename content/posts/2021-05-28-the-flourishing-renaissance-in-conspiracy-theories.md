---
title: The Flourishing Renaissance in Conspiracy Theories
author: guyberliner
type: post
date: 2021-05-28T02:51:06+00:00
url: /2021/05/28/the-flourishing-renaissance-in-conspiracy-theories/
categories:
  - Uncategorized

---
Demographic studies of the crowd at the Jan 6 insurrection riots are revealing: it skewed towards an older group that is not typical of violent street protest events. It also consisted mostly of people who are not die-hard activists of any stripe, rather than the &#8220;usual suspects&#8221;. These were mostly not people who had been to many (or any) such events in the past, did not have a long history of association with extremist groups, etc. Associations with Qanon and related conspiracy theories were, however, prominently present and visible. 

You can blame Trump all you want for the bonkers &#8220;stop the steal&#8221; and other conspiracy theory agitating, and he does have to shoulder a lot of blame for it. You can blame social media &#8220;filter bubbles&#8221;, and you&#8217;d have a point there, too. But something else is happening that&#8217;s probably even more important, but conveniently overlooked by corporate media. 

The demographic that was overrepresented at the Capitol riots, and is overrepresented in Trump&#8217;s &#8220;base&#8221; in general, as well as in online conspiracy theory adherents, etc, also has something else in common. It skews towards people who grew up during that thirty year period some call &#8220;la trentaine merveilleuse&#8221; in France, ie, from about 1950 through to the late seventies, when capitalist crises in the developed countries were held mostly in abeyance, thanks to Bretton Woods and the post-war vaguely social democratic consensus that also prevailed. So they aren&#8217;t all that conversant with the fact that, historically, that period was really an aberration. 

But if you grew up in that era with certain expectations, it kind of makes sense to believe that there must be some vast conspiracies afoot to account for the perfect storm of economic, social, and ecological crises now besetting us. Whereas a modest acquaintance with world history would suffice to show you that no such elaborate theories are necessary for explaining the emergence of crises in a world system that has mostly been racked by crises every four to seven years, throughout its four centuries or so duration, as Richard Wolff points out. 

Also historically, though, as Wolff again points out, certain minority groups, especially Black folks, have functioned as a sort of &#8220;shock absorber&#8221; for capitalist crises: last hired, first fired. And so they have never been able to widely hold such expectations. But now, all of a sudden, they are getting scapegoated by a reinvigorated white grievance politics for refusing to put up with that historical legacy of abuse which they have been on the receiving end of for too many generations. 

And once again, it makes all the sense in the world that people who HAVE been able to take certain things for granted historically, but increasingly no longer can do so, would find such historically marginalized groups to be very attractive scapegoats for their rage. So it is not an accident that women, minorities, and &#8220;Democrat politicians&#8221; &#8212; as GOP demagogues tirelessly if ungrammatically rail about &#8212; are both the targets for the rage as well as overrepresented as subjects of the wild conspiracy theories.