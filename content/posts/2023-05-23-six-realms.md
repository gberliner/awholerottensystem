
---
title: "Six realms"
author: guyberliner
type: post
date: 2023-05-23T00:40:40+00:00
url: /2023/05/23/six-realms/
categories:
  - Uncategorized

---
In many varieties of Mahayana Buddhism, there are said to be six distinct "realms" or planes of existence for sentient beings. They are: the God realm, the Angry God or Demon realm, the Human realm, the Animal realm, the Hungry Ghost realm, and the Hell realm. Whether taken literally or metaphorically (it is sometimes said that a person can pass through all six in the space of a single hour!), these realms figure prominently in many accounts of Buddhist theology.

If one were to undertake a kind of modern day Buddhist economic hermeneutics, one could say that the bourgeoisie, that is, the very rich, those modern day kings and princes of the earth, are those who abide or seek to abide for protracted periods in the God realm. The God realm is the realm of eternal perfection.

"Eternal perfection" sounds like it must be very nice, even earthly paradise. However, according to another, even more basic Buddhist teaching, that of the Four Noble Truths (in fact, the most basic teaching of all, Buddhism 101 if you will), and also because of another foundational teaching, that of Anitya, ie, "Impermanence", this eventually must fall apart. One can potentially spend more or less protracted periods of time in the God realm, but as a mortal, one can never permanently abide there.

This then gives rise to the Angry or Jealous God/Demon realm. In the Angry God realm, one has noticed imperfections and threats creeping in on one's beautiful life in the God realm, and naturally this provokes frustration and defensive measures. One then seeks to "make it all great again". 

Again, a Buddhist political economics would ascribe the petty bourgeoisie to this realm. They are those who have perhaps gotten a brief taste or glimpse of the God realm, but have seen or are seeing it slip away from them, much to their chagrin.

If you ever hear anybody saying something like, "well, I worked my ass off for everything I've got!", that's coming straight out of the Angry God realm.

Which brings us back to the Four Noble Truths, as well as Impermanence. The First Noble Truth asserts the inevitable existence of suffering, and the Second Noble Truth attributes suffering to grasping and aversion. Furthermore, the teaching of Impermanence asserts that all observable earthly phenomena (aka, "dharmas") are impermanent, ie, they arise and fall away. Which means that the quest to abide forever in the God realm is doomed to failure, by the teaching of Impermanence, and the very effort to do so produces suffering in the one who strives to do it, by the Second Noble Truth. A double whammy!

The predictable results of such failed efforts are everywhere apparent, and often ugly.

(In popular culture, Willy Wonka could be taken for an avatar of the God realm, and the character of "Carol" from "Where the Wild Things Are" as an avatar of the Angry God realm.)

"But what about the Third and Fourth Noble Truths?", you ask. I will save those for next time!
