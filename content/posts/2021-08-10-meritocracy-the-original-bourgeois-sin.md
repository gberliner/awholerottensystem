---
title: Meritocracy, the original bourgeois sin
author: guyberliner
type: post
date: 2021-08-12T00:40:40+00:00
url: /2021/08/12/meritocracy-the-original-bourgeois-sin/
categories:
  - Uncategorized

---
"Meritocracy" is the original bourgeois sin.

Those who expect to be differentially "rewarded" with greater power and status over others, on the basis of "hard work" or "accomplishments", already vitiate the work they do itself.

If the major motivation for someone's work does not flow from pride and enjoyment in the work itself, or in their contributions to the wellbeing of others, but only primarily in an urge to lord it over their fellows, the work itself must be considered suspect. That base motivations rarely inspire good and honest work should go without saying.

