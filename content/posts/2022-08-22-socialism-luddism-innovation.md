---
title: "Socialism, Luddism, and Innovation"
author: guyberliner
type: post
date: 2022-08-22T00:40:40+00:00
url: /2022/08/22/socialism-luddism-innovation/
categories:
  - Uncategorized

---
"Socialism" is assailed by the political right on the grounds that it afflicts "individual freedom" and stifles "innovation".

Let's be clear right away that "socialism" is a MODERN response to MODERN predicaments, arising during the emergence of industrial capitalism, specifically as it has evolved since the late 18th and early 19th centuries. The earliest use of the word itself only dates to about the year 1800.

The most notable, early popular revolts against industrial automation in England prominently include the Luddites, who destroyed factory machinery under the banner of an imagined "General Ned Ludd". This movement has been the subject of endless distortion in the two centuries since.

We had might as well acknowledge here that, yes, "socialism" is not singularly dedicated to maximum "innovation", or fanciful notions of "individual freedom" that involve the unlimited supposed rights of billionaires to build personal space programs, the consequences to the rest of humanity be damned. And that Luddism, a very deliberately and methodically "misunderstood" movement, was early kinfolk to modern socialism. 

Luddism was NEVER about some kind of blind, fanatical opposition to all technological innovation. Many of its participants were themselves among the most skilled early mechanics and engineers. But it WAS insistent that "innovation" must always be subservient to human needs. And that that defining characteristic of capitalist economics, "uneven development", must be brought to heel by the deliberate, planned intervention by the larger community, in the interests of protecting larger communal priorities.

Capitalism never resolves its inherent contradictions, it only shifts them around spatially and temporally. Socialism intervenes by demanding controls and limits on "innovation" that prioritize the most vulnerable, and rejects the logic of blind faith in the rich and powerful, and the technocrats they can buy to work exclusively on retainer for them.

EP Thompson's splendid "Making of the English Working Class" (1968) vividly recounts the trials and tribulations of Luddites and other early socialist rebels in the cauldron of England's breakneck Industrial Revolution. And we all should read it today, because the basic conflicts and dilemmas it illuminates, while differing radically in their minute details, are as relevant today as ever in their essential fundamentals.
