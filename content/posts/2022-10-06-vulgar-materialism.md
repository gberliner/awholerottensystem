---
title: Vulgar materialism
author: guyberliner
type: post
date: 2022-10-06T00:40:40+00:00
url: /2022/10/06/vulgar-materialism/
categories:
  - Uncategorized

---
Dialectical and historical materialism are great tools for understanding the world. They are helpful at arriving at plausible explanations for events because they depend on a minimal number of ad hoc hypotheses. 

The more hypotheses an explanation depends on introducing, the more fragile it becomes, and the greater the odds of it being inaccurate. Based on simple probabilities, if your explanation depends on N different hypotheses, and the probability of each hypothesis being correct is 50%, then the probability of your explanation as a whole being accurate is now only 1/2^N, which rapidly becomes vanishingly unlikely, as N increases.

Conspiracy theories originate out of exactly such a sequence of moderately plausible hypotheses. Like Ptomely's theory of epicycles, built up to support pre-Copernican, geocentric astronomy, most conspiracy theories depend on the introduction of numerous, ad hoc hypotheses to arrive at the results that the conspiracy theorist favored from the start.

Materialism emphasizes concrete material conditions as setting constraints on events and the subjects who participate in them. The participants are always affected by many factors, ideological, psychological, political, and so on. But various material factors create constraints on their freedom of action, starting with considerations of immediate survival and physical safety, and proceeding to longer term ones -- economic survival and the ability to supply one's own material needs and those of one's family, over a period of time.

A form of "vulgar materialism" would be an attempt to blithely ignore any and all other considerations, and to collapse all of a given individual's motivations down to material and physical coercion. This is a form of obsessive nuttiness that people can easily fall prey to. It would be like assuming, say, that anybody who carries a concealed handgun must have an imminent plan to shoot someone with it (if you were so fanatically opposed to all handguns at all times, for example, that you insisted on believing the most uncharitable possible motives of anyone who owned one).

Vulgar materialism that ignores all other considerations can easily evolve into its own form of conspiracy thinking. Because in order to ignore those other facts that contradict your favored explanation of a situation, you have to make the same mistake of introducing new, unwarranted hypotheses that explain them away, alleging they are false or a mere disguise for your single, preferred explanation.

Take, for example, the propensity of judges to rule against former President Donald Trump in legal cases lately. They seem to do so repeatedly, even in many instances where they share his nominal party affiliation, and even in some cases where they were originally appointed by him!

A conspiratorial mindset could say, "well, it must not be on the merits of the case, let's not look too far into that, but it must be because the 'deep state' hates Trump, and these adverse judges are surely part of it, otherwise they'd have been loyal to him!" An equally conspiratorial mindset might say, "well, any Trump appointee will surely rule in his favor all the time. So if one DIDN'T do it, they really somehow DID! It must all be part of a grander plan." 

Whereas, concerning the judges, we need not introduce any new hypotheses unique to the partcular case at all, and can often simply conclude that they decided a given case on its merits, because whatever their personal qualities and virtues might be, they are likely to have a greater loyalty to an institution, the judiciary (and its norms), one which they depend on for their own futures, than they do to Donald Trump -- a currently unemployed former President --  as an individual.

An angry person who initially discovers things like dialectical and historical materialism, and then develops a very superficial acquiantance, can get excited about their explanatory power for accounting for their personal predicament, say, and become vulnerable to falling for vulgar materialism. Such sharp tools should be used more like an exacto blade, but an overenthusiastic novice may want to swing them around like an axe.

The late Michael Brooks lampooned this tendency in a hilarious impression of "Bernie Sanders as personal financial advisor", explaining how to build "intergenerational wealth through politics". Brooks's Bernie Sanders goes on to say that becoming a staunch defender of working class interests in politics is a great but slow path to financial success. You have to build many alliances, try and win office and achieve tangible gains for ordinary people, get close to winning a presidential primary, and then, finally, write a book all about it. Then, if the book is a bestseller, voila, you can become a millionaire!

Michael Brooks's schtick was intended to lampoon the assertion that "Bernie Sanders is a sellout", and the attribution to him of ulterior motives to explain his failure to produce the desired results or to conform to the favored political strategies or stylistic preferences of his disillusioned supporters.

In his routine, Brooks did not expand on the larger pattern of delusional thinking implied by such a tendency, but doing so remains highly important. Like the exacto blade, skillful use of any tool can be very helpful, but UNSKILLFUL use can easily cause much more harm than good!
