---
title: Bourgeois "warlords"
author: guyberliner
type: post
date: 2021-06-29T00:40:40+00:00
url: /2021/06/29/2021-06-29-bourgeois-warlords/
categories:
  - Uncategorized

---
George Monbiot refers to the most reactionary segments of the bourgeoisie as the "warlord faction".

The "warlords" make a virtue out of the necessity of their class's inherent alienation and emotional deadening to the lot of the rest of humanity and the living world surrounding them.

"Warlords" regard the rest of the human race, not with a sense of "noblesse oblige", but instead, barely disguised contempt. It's a contempt for weakness, often adopted as a protective response to their own harsh upbringings at the hands of emotionally deadened parents, and it's often reflected in harsh tendencies favorable towards racism, xenophobia, and alacrity for reactionary themes like "culture wars", denunciation of "victimhood mentalities", and similar themes.

(Monbiot coins this terminology in the context of "Brexit", which he describes as a smokescreen for empowering the warlord faction in its ruthless efforts to convert the British state into a vessel for their power, untethered by even minimal international commitments to such things as international labor standards, environmental laws, etc. Commitments which, as weak as it is, the EU still binds the UK to.)

https://www.youtube.com/watch?v=s5VgkCb8lYI
