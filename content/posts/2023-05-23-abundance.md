
---
title: "Abundance"
author: guyberliner
type: post
date: 2023-05-23T00:40:40+00:00
url: /2023/05/23/abundance/
categories:
  - Uncategorized

---
"If we all do a little more than our share, then there can be abundance." - Kyogen Carlson, Zen teacher and late abbot of Dharma Rain Zen Center (Portland, Oregon)

"I've worked my @$$ off for everything I've got!" - a random, proverbial "Small Business Owner"

The two quotes above express practically diametrically opposed views regarding the origins of prosperity. The first, like the second, while also emphasizing the importance of individual effort, nevertheless implies a lightly held expectation regarding any personal rewards. Instead, the anticipated rewards are COLLECTIVE in nature. The second, on the other hand, holds tightly to the expected rewards, which are assumed automatic and justly to be held exclusively by individuals.

These two views express fundamentally different and largely incompatible cosmovisions. One could stereotype the first as "communism" and the second as "capitalism", or "universal" vs "particular", or any number of other such opposing pairs.

And although both may well express "truths" in their own ways, my clear preference and contention is for the former, as expressing the deeper and more important truth.

The chief flaw of the second statement is that it grasps too hard for something far too insubstantial and uncertain because, in the end, "just individual rewards for effort" are exceedingly fickle. Whereas, investing our efforts in a jointly held, community endeavor is more like social insurance. If, say, we build a store and put ALL our efforts into making a great success of it ("i worked my @$$ off for everything!"), things MAY work out fine. But, on the other hand, one random fire in the attic could end it all overnight, with no hope of ANY rewards. Whereas, if we invest a little in an insurance policy that also helps out anybody else who falls prey to unforeseen distress ("if we all do a little more than our share..."), then our future prospects are far more secure, both collectively and individually.
