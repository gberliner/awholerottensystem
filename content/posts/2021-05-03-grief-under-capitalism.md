---
title: Grief under capitalism
author: guyberliner
type: post
date: 2021-05-03T17:15:52+00:00
url: /2021/05/03/grief-under-capitalism/
categories:
  - Uncategorized

---
Any ideology that abjures or suppresses the expression of grief damages our humanity in profound ways. Certainly capitalism qualifies in that regard, and is a key part of our ecological crisis. 

This month, we are approaching the yearly observance of the Wesak holiday, aka, &#8220;Buddha&#8217;s birthday&#8221; in various countries. This thought about grief reminded me of a popular song, &#8220;Holy Day of Wesak&#8221;, often sung by Buddhist groups in this country and others around this time of year. The lyrics are: 

> Buddha Lord we offer  
> On thy birthday fair  
> Garlands of the brightest  
> Blossoms choice and rare.  
> Holy day of Wesak  
> Day of Buddha’s birth  
> When the sun of wisdom  
> Shone upon the earth.  
> Incense too we offer  
> On this festive day  
> For the things we cherish  
> All must pass away.  
> Through this holy symbol  
> We shall learn to see  
> Things of priceless value  
> Hid in transiency.  
> And the deep gong sounding  
> Bids us leave the self  
> And in Buddha’s teaching  
> Find the truest wealth.  
> Lights upon the altar  
> Show to us the way  
> From the realms of darkness  
> To Nirvana’s day. 
Perhaps these verses are mostly banal, especially for anybody not already an adherent of Buddhism, but they make a profound point, entirely integral to Buddhism in all its varieties, about what Buddhists call &#8220;Dharma&#8221; (sometimes translated as &#8220;Universal Law&#8221;, etymologically related to the English word, &#8220;firm&#8221;): the fact that every thing in this world that we know has a beginning, middle, and end. This is inherent in every entity&#8217;s existence, from its very beginning. This is the Principle of Impermanence, (&#8220;anitya&#8221; in Sanskrit, &#8220;anicca&#8221; in Pali). And its vital importance stems from the fact that our very ability to appreciate the beauty and preciousness of this life and this world depends on our appreciation of this fact. 

One can begin to sense the profound relevance to ecological consciousness here already. And not only ecological in the narrow sense of the &#8220;nonhuman biosphere&#8221;, but also in human and social ecology, and all our relationships. For this understanding of impermanence (&#8220;transience&#8221; in the song) is irreconcilable with a transactional worldview seeking constant gain and endless &#8220;growth&#8221; (&#8220;the ideology of the cancer cell&#8221;). The Principle of Impermanence clearly highlights the literal impossibility of such a fantasy. And the song reminds me of the emotional irreconcilability of such an attitude with healthy relationships, both with other human beings as well as the rest of the living world. 

I am reminded of jokes people sometimes make about men always trying to &#8220;fix&#8221; their partner&#8217;s sadness, a supposed part of a common male &#8220;emotional intelligence&#8221; deficit in our culture. And surely this deficit is not unrelated to the traditional male role of &#8220;breadwinner&#8221; under capitalism. For this role demands constant, relentless &#8220;positivity&#8221;, as the proper attitude and counterpart of the monotonic growth ideological fantasy. But one cannot always &#8220;fix&#8221; grief, and attempting to do so can be actively harmful. Sometimes, one must sit with it and learn to cherish it as an indivisible part of a whole that is itself cherished. 

[For a more profound exploration of the &#8220;cult of positivity&#8221; under modern capitalism, Korean-German philospher Byung-Chul Han has written several valuable books, including &#8220;The Burnout Society&#8221;, and most recently, &#8220;Psychopolitics&#8221;.]