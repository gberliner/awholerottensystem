---
title: The Ghost Ship
author: guyberliner
type: post
date: 2022-01-04T00:40:40+00:00
url: /2022/01/04/the-ghost-ship/
categories:
  - Uncategorized

---
Cenk Uygur, the progressive media impresario and sometime congressional candidate, made a promo awhile ago for his popular online franchise, "The Young Turks", in which he infamously referred to the Democratic Party as a "ghost ship" ripe for takeover by an insurgent leftist faction. Of course, we all see how that has turned out so far. No sooner did the pirates attempt to board the seemingly abandoned ship than, in a thrice, its ferocious defenders emerged seemingly out of nowhere, and thoroughly repelled and thwarted the attempted hijacking with great prejudice.

Ironically, though, now that elected Democrats actually hold an official "Trifecta" (ie, power over all three major elected federal branches of government), it really does feel once again as though we are indeed aboard a listing ghost ship, drifting inexorably and pitilessly towards rocky shoals that likely spell our final ruin.

We could already see the outlines of our hopeless situation emerging as early as November after Biden's 2020 victory, when he was caught in an infamous leaked audio recording hectoring representatives of predominantly Black civil rights groups about "the Constitution!" and how he had no intentions of more than minimally rolling back the devastation of the Trump era, defying even the very gentle suggestions of career civil rights law professionals on a conference call.

Now, we can see even more clearly the intentions of both Democrats and Republicans. While Democrats are blithely mesmerized by their quotidian obsessions with the comfortable rhythms of Washington, Republicans are openly and ostentatiously plotting their millennarian revenge tour, introducing hundreds and passing dozens of draconian laws to gerrymander, suppress, and rewrite votes and even whole election outcomes altogether at will.

We would have known six months ago whether Democrats were at all serious about holding onto and exercising power (as opposed to continuing their biennial and quadrennial musical chairs dance with the now official death-cult-qua-opposition-party) had AG Merrick Garland already empaneled a January 6 "special prosecutor" and begun issuing subpoenas to high profile persons of interest. 

This would have been a clear "crossing the Rubicon" moment for Democrats, signaling their interest in exercising power in earnest because, despite the fulminations of Republicans against them, the latter know that their ascendancy to permanent minority rule is by no means a done deal yet. And any really aggressive stance by the current Democratic majority could have long term adverse consequences for them, which would surely chasten a few among them who are privy to knowledge about the locations of certain skeletons and the closets hiding them.

Instead, though, we see Democrats practically sleepwalking towards a "shellacking", as they now call it (since Obama started using the term in 2010 to refer to their usual ritual of toothless electoral surrender every other cycle). Of course, were they to actually take the project of rolling back incipient fascism as seriously as incipient fascism takes itself, a totally different attitude would be called for. But, if you anticipate and plan on a potentially vengeful foe taking over, you don't do too much to upset him beforehand, if only for reasons of pure self-preservation.
