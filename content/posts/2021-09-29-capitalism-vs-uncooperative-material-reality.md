---
title: Capitalism vs uncooperative material reality
author: guyberliner
type: post
date: 2021-09-29T00:40:40+00:00
url: /2021/09/29/capitalism-vs-uncooperative-material-reality/
categories:
  - Uncategorized

---
Capitalism and a certain dominant set of social relations associated with its now planet-straddling circuits of trade and capital circulation are phenomena we now take for granted. 

But, as [David Harvey](https://www.youtube.com/watch?v=KAux0DvLZ7g) points out, in the larger span of human history, these are remarkably new phenomena, whose trajectories were only starting to become really apparent in Marx's time -- but which were then still relatively marginal, in the sense that they had yet to sweep up the vast majority of humanity into their orbits. It was Marx's considerable prescience to clearly see these trajectories and predict the outcomes we are now living as a consequence.

Today, however, certain facts of physical reality, not the least of which atmospheric physics and chemistry, are proving highly uncooperative for purposes of the required maintenance and expansion of these schemes of social control.

Now, you can't literally fight physical laws. But you can certainly attempt to obfuscate and deny them for as long as possible. Does it really come as any surprise, then, that we suddenly find ourselves by some supposedly miraculous coincidence in an "age of unreason", replete with bizarre conspiracy theories and chaotic protests against a variety of mysterious moral panics, often about things as mundane but lifesaving as infectious disease countermeasures in the middle of a global pandemic, for example?

Faced with clear, existential crises posed by our currently dominant economic patterns, some (most?) economic elites respond with a shrug and at least a grudging acceptance that they had better care, or at least, pretend to care, and go along with at least some modest but logical responses to these crises.

Others, however, have a different idea.

When Charles Koch, for example, first heard about the climate crisis, he put his "best minds" to work attacking it -- or not exactly "it", but any countermeasures at all proposed to address it. And, with his money and resources, he was easily able to hire the most talented people. Often, at least initially, though, they came back with fairly reality grounded proposals as to how Koch Industries could help mitigate the climate crisis AND profit at the same time. But, he had already made his mind up -- if material reality was being uncooperative, then to hell with material reality! (Christopher Leonard's excellent "Kochland" details the saga of Koch's war on climate policies and science.)

More generally, Koch and a number of other bourgeois ultrareactionaries (the Mercers, the Uihleins, the Coorses, the Mellon-Scaifes, the Murdoch and Sinclair media empires, etc) have an extensively elaborated program for cementing minority rule, and destroying any prospective threats to it, that involves systematically defunding, undermining, and demolishing any components of the public sector that do not directly enrich themselves. And their program can be summarized perfectly by white nationalist commentator Tucker Carlson's blatant, proud admission that he always votes for "the most corrupt and incompetent candidate in any election", on the grounds that, the more corruption and incompetence at all levels of government, the less likely anybody is to want to raise his taxes to pay for it.

