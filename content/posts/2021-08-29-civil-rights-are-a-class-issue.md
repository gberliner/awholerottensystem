---
title: Civil rights are a class issue
author: guyberliner
type: post
date: 2021-08-29T00:40:40+00:00
url: /2021/08/29/civil-rights-are-a-class-issue/
categories:
  - Uncategorized

---
Civil rights are a CLASS issue. While it should be obvious to anybody, nonetheless, it's long past time we carefully and patiently explain this elementary fact to our libertarian friends, among others.

Ask anybody without "legal" housing about the respect shown by the cops for their first, fourth, and fifth amendment rights in the United States, notably. The ability to exclude unwanted intruders on your personal property is a right reserved to those who OWN property. The US Supreme Court has even ruled that any legal claims for police protection are reserved to those who can claim a "property interest" in the matter (Castle Rock v Gonzales, 2005).

The United States is effectively a straightforward, legally sanctified bourgeois class dictatorship, in which *de jure* protections for the civil rights of everybody else are deficient if not nonexistent.

The de facto primacy of the rights of the bourgeoisie over everybody else has been amply and poignantly demonstrated most recently by the Supreme Court's "shadow docket" ruling against the Biden administration and the CDC, enjoining enforcement of a residential eviction moratorium, in the middle of a global pandemic and "shelter-in-place" public health guidance. (Although thankfully the court did not literally outright rule that the "property rights" of landlords take precedence over the lives and well being of the entire public, especially tenants, that is the short term practical consequence.)
