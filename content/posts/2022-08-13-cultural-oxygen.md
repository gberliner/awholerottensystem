---
title: Cultural Oxygen
author: guyberliner
type: post
date: 2022-08-13T00:40:40+00:00
url: /2022/08/13/cultural-oxygen/
categories:
  - Uncategorized

---
As I continue reading EP Thompson lately ("Making of the English Working Class"), I think one of the outstanding points he makes in his work is the enduring propensity of the opulent class to suck all the cultural oxygen out of our ambient environment. It's a power which, more than brute, physical force, accounts for their astonishing ability to sap all resistance against them by their vastly numerically superior adversaries in the rest of population.

Thompson says of his book that his mission was to rescue generations of resistance heroes from "the enormous condescension" of history, especially that of the professional academics, historians, sociologists, etc, who are empowered to define it. In this, he "names their names", people like John Doherty, John Gast, Gravener Henson, whom the powerful would prefer be erased from our memory.

And it continues to be in their power to define the limits of amorphous words like "civilization", or "humanity" using mantras like "there is no alternative". In this, they present potential aspects of our individual and collective behavior, like competition, boundless greed, and rapacity, as the necessarily dominant and defining characteristics of "human nature". And certainly they are, when it comes to the present form of global capitalism, as it has taken shape over the past five hundred years (a batting of an eyelash in the million year history of human life on Earth). 

Meanwhile, we don't hear, for example, about any of the civilizations (however few or many they may be), like the Ladakhis or Tibetans, say, who built advanced, technologically sophisticated societies on the basis of incredibly meager physical resources, which nonetheless endured very stably for over a thousand years, right up until the early 1980s, when anthropologist Helena Norberg Hodge did her major work studying and documenting them. (I recommend her excellent documentaries and books, "Learning from Ladakh", et al).
