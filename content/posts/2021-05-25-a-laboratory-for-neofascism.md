---
title: A Laboratory for Neofascism
author: guyberliner
type: post
date: 2021-05-25T23:25:25+00:00
url: /2021/05/25/a-laboratory-for-neofascism/
categories:
  - Uncategorized

---
By now, the sickening footage of Loveland, CO police senselessly brutalizing an elderly woman with dementia has made the rounds everywhere, but not until after a year long delay while the family desperately pleaded with authorities for more information, only to be stonewalled until they threatened lawsuits. But luckily for them, it turns out the newly passed law partially stripping officers of immunity in that state came into effect just in the nick of time.



Watching this dehumanizing spectacle, and worse yet, the footage of the officers gleefully mocking her while they were holding her at their station is incredibly disturbing. It&#8217;s hard to avoid concluding that this kind of work often attracts exactly the kinds of individuals who are least fit for entrusting with any responsibility for ever using any force against others.



Then there&#8217;s the footage of what can only be called the extrajudicial execution of Ronald Greene, a troubled Black motorist, by Louisiana State Police after a reported high speed chase. Greene died after a savage and apparently unprovoked beating by a whole gang of patrolmen on the scene. (&#8220;High speed chase&#8221; deserves scare quotes, since the officers involved completely fabricated the original cause of death, and consequently, there are few grounds for confidence in any of the other claims they have made.)



Once again, it is hard to avoid the conclusion that this line of work attracts a fair number of people who get a release from inflicting gratuitous suffering and humiliation against others, and actively look for pretexts to do so. It&#8217;s also hard not to notice the loudly expressed political sympathies of an apparently large cohort of these individuals (made clear by the loudly announced official campaign endorsements of national police unions), and the obvious parallels with a larger politics which itself appears to be obsessed with looking for pretexts for the gratuitous infliction of suffering and humiliation.



It&#8217;s almost as if the political economy of a society dedicated to artificial scarcity via rentier capitalism (in areas such as housing, health care, and even things as elementary as clean air and water and wholesome food), and stark delineations between the &#8220;worthy&#8221; and the &#8220;useless eaters&#8221; to justify the predations necessary for maintaining it, is practically a breeding ground for such mental pathologies.



As a result of all of which, USAnian society has become over the past four decades a practical laboratory for &#8220;Gain of Function&#8221; experiments in the spread of neofascist movements.