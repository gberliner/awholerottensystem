---
title: "Laser levelling"
author: guyberliner
type: post
date: 2023-04-29T00:40:40+00:00
url: /2023/04/29/laser-levelling/
categories:
  - Uncategorized

---
An irrigated agricultural field needs to be level to produce an optimal yield. If it's too uneven, then water won't get to all the young plants that need it, but instead, too much will collect at the low points, uselessly and even harmfully overwatering them, while little or none will reach the higher points, starving those plants of the moisture they need to thrive. And as it turns out, one of the most promising modern technological tools for potentially saving double digit percentages of energy and water resources, and improving agricultural yields, is laser levelling. All by itself, it has the potential to buy us a few years to forestall oncoming climate catastrophes, while we urgently redistribute our efforts and resources to focus on the larger crisis.

Human society as a whole, though, interestingly enough, is not so very different from an irrigated field. As Nobel laureate economist Thomas Piketty explains in his magnum opus, "Capital in the 21st Century", based on voluminous statistical evidence over the past four hundred years of capitalism worldwide, across the most varied countries and regions, rates of returns to capital have systematically outstripped average rates of economic growth by several percentage points. 

It's a highly predictable factor, completely independent of "hard work", "merit", and all the other bourgeois fairy tales we are constantly having drilled into us, that the most consistent and reliable predictor of ending up with a bigger pile of money is starting out with a big one in the first place.		

Hence our biggest crises, both social and ecological, can be categorized as "software" rather than "hardware" bugs. That is to say, we are not beset primarily by "technical" challenges susceptible to normal R&D, but rather, issues involving the logic of a society controlled by the bourgeois/capitalist class, whose modus operandi is always to incentivize and signal to everybody that their survival priorities must be directed towards expanding the total volume of water carried by the metaphorical irrigation channels, and not concerning themselves with how and where the water flows.
