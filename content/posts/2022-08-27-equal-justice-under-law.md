---
title: Equal Justice Under Law
author: guyberliner
type: post
date: 2022-08-27T00:40:40+00:00
url: /2022/08/27/equal-justice-under-law/
categories:
  - Uncategorized

---
One time, i was getting off a train platform with my hands full, juggling some groceries i had bought, along with a backpack with a broken strap. A bunch of transit cops were doing fare inspections. One of them forced me to put me all my stuff down and dig out my ticket. 

Then, before I even got off the platform, a couple more accosted me again, and again, demanding to see the same ticket the earlier cop did!

By the time I made it off the platform, I was ready to strangle the last cop. And I'm "Caucasian", so i don't think they were doing it out of racial bias.

I can only imagine what it would feel like to go through this kind of crap all your life, and have definite indications that it WAS racial bias.

Non-white North Americans are often left having to repeatedly guess, over and over again, "Why am I being treated like sht THIS time? Is it actual racial bias? Or is it just that poor people in general are treated like sht, and people of my racial makeup are often poor, therefore guilt-by-association? Or because poor and minority individuals ARE more likely to commit crimes out of economic desperation (again, another form of guilt-by-association)?? Or is it all just a coincidence this time???"

Now cops, OTOH, might well just be "doing their job". And they may know, from experience, that there IS a strong correlation between poverty and street crime, and between race and poverty, in turn. So, realistically, in a highly economically unequal society, with an enormous and ongoing burden of decades and centuries of entrenched, unequal treatment, are they really going to completely refrain from ever "racially profiling"??

Conclusion: "equal justice under law" is a complete fairy tale in a society that suffers extreme economic inequality.
