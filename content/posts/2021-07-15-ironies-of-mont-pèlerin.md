---
title: Ironies of Mont Pèlerin
author: guyberliner
type: post
date: 2021-07-15T00:40:40+00:00
url: /2021/07/15/ironies-of-mont-pèlerin/
categories:
  - Uncategorized

---
It should be counted as an irony of the profoundest importance that the Mont Pèlerin Society (https://en.wikipedia.org/wiki/Mont_Pelerin_Society ), commonly regarded as the formal birthplace of "neoliberalism" as a distinct ideology or political program, numbered the following two among its six "core principles" (or "Statement of Aims", as it called it) which it announced at its founding meeting convened in April, 1947:

> 3. Methods of reestablishing the rule of law and of assuring its 
> development so that individuals and groups are not in a position to encroach
> upon the freedom of others and private rights are not allowed to become 
> a basis of predatory power.
> and
> 4. The possibility of establishing minimum standards by means not inimical
> to initiative and the functioning of the market.

(Quoted from the book, _The Road from Mont Pèlerin: The Making of the Neoliberal Thought Collective_, eds Philip Mirowski and Dieter Plehwe, Harvard University Press, 2009 -- an anthology of essays on the historical development of neoliberal ideology)

Nowadays, obviously, such concerns appear completely alien to any contemporary understanding of the expression "neoliberalism". But the reason we should not only acknowledge but emphasize the significance of these preoccupations in the foundational documents of such an organization has to do with what it tells us about the nature of class power.

It's vitally important to understand that the intellectuals who embarked on this program which they themselves dubbed "neoliberalism" did not set out on a quest to subvert civilization or Enlightenment ideals via a kind of neofeudalist conspiracy, with the goal of breaking it down into a vast bourgeois war of all-against-all, even if that is in fact what their original program has degenerated into.

(In this connection, a close reading of the book cited above would immensely profit leftist intellectuals of all stripes.)

It's equally important to understand that what today we call "liberal democratic societies", despite having many admirable (if only mostly aspirational) qualities, are in fact the ideal growing media for a bourgeois class that in practice certainly does tend to culture upon such a substrate a kind of "amoebic civilization" (if, that is to say, individual amoeba were also to one day develop the ability to carry Kalashnikovs and fire them into crowds of other amoebae).

In other words, any program, no matter how seemingly enlightened and rational on its face, can hardly survive the corrosive influence of class interests, in a society that legally permits their consolidation into any form of capitalism as we know it.

Economist Bill Black refers to deregulation as "criminogenic". But it is also a primary drive of the bourgeoisie, insofar as "regulation" implies the use of state power in ways that limit its class power. The writer Nassim Taleb speaks admiringly of "antifragility", or a capacity to thrive under conditions of chaos. But chaotic conditions above all promote the thriving of criminal elements. Thieves, notoriously, watch for people moving houses as prime opportunities to practice their craft, amidst the inevitable disorder that any move entails.

To be clear, this is not necessarily a "conspiracy" in the usual sense of the word, even if it does have conspiratorial elements. Because although, when we speak of the "bourgeoisie", or capitalist class, we are talking about a very small fraction of the entire population (between one and ten percent, depending on how you classify the upper echelon of the "coordinator" or "professional managerial" class), it is easy for them to converge on certain common class interests, often without a lot of coordination or premeditation, even if, otherwise, they are themselves on the whole very fragmented and fractious in their opinions on most other subjects.
