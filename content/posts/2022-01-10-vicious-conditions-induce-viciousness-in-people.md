---
title: Vicious conditions induce viciousness in people
author: guyberliner
type: post
date: 2022-01-10T00:40:40+00:00
url: /2022/01/10/vicious-conditions-induce-viciousness-in-people/
categories:
  - Uncategorized

---
It's not a uniquely US phenomenon: vicious conditions induce viciousness in people. It's eye-opening to hear a journalist and social critic such as Ulrike Hermann, from another country, one like Germany, tell it. Even a country with comparatively generous social welfare benefits, such as Germany, is subject to much the same pressures under global capitalism as the United States.
