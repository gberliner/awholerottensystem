---
title: Art and agitation, whats our destination?
author: guyberliner
type: post
date: 2021-12-25T00:40:40+00:00
url: /2021/12/25/art-agitation-whats-our-destination/
categories:
  - Uncategorized

---
"The true treasure is the one you pass by believing it not worth the trouble of your trip, or that your life's too short to take a detour and visit, on your way to that shiny one you're aiming for just beyond it." - B. Traven, *Treasure of Sierra Madre*

That great art can carry meanings that surprise  even its creators is a truism. But when singer-songwriter John Mellencamp wrote his hit song, "Pink Houses", he could not have been surprised that some missed his barely hidden irony. Indeed, he himself has indicated that he mischievously intended as much. 

Mellencamp *was* at first surprised, though, by his subject's peculiar cheerfulness: "a Black man with a black cat," waving and smiling at him from a pink house, as he sped driving by on the interstate highway that plowed through the man's front yard! This fleeting experience caused him to reflect, he says, "Wow, is this what life can lead to? Watching the fuckin’ cars go by on the interstate?"

On further reflection, though, he imagines the man's cheerful railery with his wife in the kitchen, as he sings, "hey darlin', I can remember when you could stop a clock!"

And then, he suggests in the song, that man is much like "you and me". The singer says he was 
> "a young man in a t-shirt
\
> Listenin' to a rockin' rollin' station
\
> [Who's] got greasy hair, a greasy smile
\
> [And] says, 'Lord this must be my destination'"

Nevertheless, Mellencamp has repeatedly expressed annoyance at the obtuseness of those who completely overlook the song's more bitter notes, and blithely turn it into an uncritical popular anthem and nostalgia trip for "the home of the free", as the repeated refrain in it goes (such as, most recently, the late arch-militarist and super-patriot Senator John McCain, at campaign events during his 2008 presidential run -- who, it must be said, promptly and wisely desisted when the singer complained).

Mellencamp continues to regret, he says, not coming up with a more aggressive ending, that would have left less ambiguity and provoked more agitation in his listeners.

However, while the song may not have worked as an instrument for political agitation as the artist originally hoped for, it does convey a profound humanity that has its own value.

The same can be said for a song like "En el Río Mapocho," by the great Chilean folksinger Victor Jara. The song dramatically evokes a devastating flood that hit a poor neighborhood in Santiago. Recounting how some people in the past had been wont to drown unwanted kittens from unspayed cats in the river, the singer says that now, though, with the flood, "hombres, perros y gatos, ¡la misma fiesta!" -- "men, dogs, and cats, are all enjoying each other's company!"

But then, amidst grim scenes of tables, walls, and whole buses being swept away in the deluge, the singer describes a young boy, seemingly oblivious to it all, who plays with a toy boat he has fashioned from a beer can, imagining himself a mighty ship's captain. Sometimes, all you can do not to cry is to laugh, reflects the singer. How can we not but marvel at the resiliency of life and humanity, despite it all? It's a profound message, albeit not always hopeful.

Amidst our agitation, we on the left, like Mellencamp, would also be wise not to scorn the "simple man", as he puts it, and his simple pleasures and solaces. For these are not always necessarily the product of mere "false consciousness". The man in the pink house with an interstate roaring through his front yard, for example, is also acutely aware of others who are simply camped out in tents and lean-tos under overpasses, so it's not necessarily without reason that "he thinks he has it made", as the song goes.

At the same time, however, we know all of this is fleeting. And without agitation, even the modest gains enjoyed by the owner of the "pink house" might not last, let alone be improved upon. And so, too, we know that they stand in desperate need of improvement, in a world where that man's grandchildren, growing up in the shadow of that interstate's ferocious, interminable roar, are likely to suffer asthma, learning disabilities, and a raft of other adverse effects from the conditions they're forced to live under.

Whether any of these hopes are even possible, though, hangs very much in the balance right now. As *Jacobin* magazine contributor Cale Brooks put it recently, there's no particular reason that a "leftist opposition", such as very recently has seemed to have awoken from long dormancy, need exist at all, and we might well awaken a few years from now under the boot of a much more thoroughgoing and brutally reactionary regime than most of us ever imagined, forced to whisper any dissent a lot more quietly and carefully than Mellencamp did.

For the most dedicated, organized, reactionary wing of the US oligarchy is acutely aware and correct in certain of its estimations. And it has a very old but effective playbook, which compensates for any of its creative deficits. So, for example, when rightwingers rail against "outside agitators", and long for "the good old days" when they could get their way with hardly a whiff of resistance -- which, but for the nuisance posed by the former, they might perhaps return to -- they are by no means entirely wrong. 

In that regard, the very toughness, adaptability, and resiliency celebrated in these songs is both a strength and a weakness. Because ordinary people, deprived of cultural and educational opportunities, and kept ignorant of their own proud heritages of autonomy, resistance, and solidarity, are often inclined to take what they can get and just "muddle along", however dreadful the conditions inflicted upon them eventually become.

We can see, for example, very unsubtle intimations of the intentions of these reactionaries in their latest "CRT" moral panic, and the censorious and draconian laws they have hastened to pass against it in numerous legislatures. This acronym can more properly be said to stand for "coherent rational thought", as applied to any historical considerations of race, class, economics, and politics (since the examples they invariably supply of what they object to amount to any "critical" understanding of material factors that adversely affect the wellbeing of ordinary, and especially historically disadvantaged people).

Thus the challenge -- more urgent than ever for would-be agitators and organizers, no less than artists -- becomes how to connect our work with the lives of ordinary people, in a way that both cherishes life as it is, however daunting, yet aspires to much greater things.

