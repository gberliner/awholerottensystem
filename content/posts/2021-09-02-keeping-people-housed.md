---
title: Keeping people housed and protecting the places we live in is paramount
author: guyberliner
type: post
date: 2021-09-02T00:40:40+00:00
url: /2021/09/02/keeping-people-housed-protecting-places-we-live-in-is-paramount/
categories:
  - Uncategorized

---
Keeping people housed and protecting their tenure in their local communities is a crucial part of a larger theme, which gets short shrift even from the left: protecting social stability in general from the chaos and so-called "creative destruction" wrought by capitalism.

Social stability, "stable families", security of tenure in one's home and possessions, these are all commonly associated with "conservative values". But not so coincidentally, what passes for "conservatism" these days is merely an endless game of distractions to draw attention away from the chaos caused by precisely the same oligarchs who bankroll both major political parties.

Leftists should emphasize family policies focused on protecting social stability. It's been said before, but the connections with housing in particular are rarely made. We should see this as fertile political ground, though.

Because bourgeois power has built a world in which capital is all powerful and lowly individuals are powerless, a billionaire from overseas can swoop into your city and neighborhood and turn your and your neighbors' lives upside down overnight, and you are powerless to stop them -- or often, because of shell companies and quasi-legal money laundering the likes of which Chuck Collins has documented, even to fully identify them ("The Wealth Hoarders: How Billionaires Pay Millions to Hide Trillions", *Polity* (2021)).

But where the rightwing demonizes the 90% of the population who lack the wealth to protect their own housing tenure and financial security, while also demonizing even poorer people, often immigrants from other countries, or the homeless, the left should recognize a common thread permeating this discourse. (Ironically, though, "liberals" who read the likes of JD Vance often signalboost far right class war rhetoric AGAINST the vast working class majority.)

Where human security as a whole is deficient, and no political solutions are on offer, people are apt to accept demagogic explanations for their problems, explanations that blame people even poorer and more powerless than themselves.

Accordingly, we should not be afraid of pointing fingers at the *real* "outsiders" here, and calling for policies that protect incumbent residents, whether renters or homeowners, from their depredations. And cities and towns, larger municipalities, and even states, should adopt policies that encourage their existing residents to stick around, by favoring them over newcomers. It might sound like this is courting dangerous instincts towards xenophobia, but the truth is, people WANT to stay put and express loyalty to their current friends and neighbors, and naturally care about their existing relationships and what is familiar to them. And that is a GOOD thing.

The footloose mobility of the working class is an asset and a weapon exploited by the big bourgeoisie, but is anathema to our capacity to build working class power. Transient workforces like those of Amazon or Walmart are almost impossible to organize. The crushing of generational connections to place makes all places and the people who live in them easier to exploit, whether for cheap labor or resource extraction.

Specific policies that encourage the "insourcing" of capital and labor include: taxes like those enacted here in Portland, OR against big-box franchises, with the funds directed towards economic development programs targeting existing poor and working class residents. Existing educational benefits such as tuition breaks for local and state residents are another. But we should be pushing for much more. Some may require lifting state and federal preemptions that often handcuff the power of local governments in the name of "free trade". Dismantling tax breaks that encourage so-called "economic development zones", which are really just predatory schemes for money laundering and exploitation and displacement of the poorest urban communities, should be another priority.

Encouraging "pride of place" is a "conservative value" that the left should embrace, as part of a larger program prioritizing human security, which is inseparable from protecting the natural world. 

Marie Christina Wood of the University of Oregon's Environmental Law program has been a rare and important voice on this subject. Read an interview with her in The Sun magazine here: https://thesunmagazine.org/issues/518/before-its-too-late

As she points out, attachment to the places we live in is also inseparable from any program to protect them and the larger natural world from the depredations of a rapacious capitalism that is already carrying us over the brink of planetary ruin.
