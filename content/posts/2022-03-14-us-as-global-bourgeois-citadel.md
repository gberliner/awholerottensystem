---
title: US as global bourgeois citadel
author: guyberliner
type: post
date: 2022-03-14T00:40:40+00:00
url: /2022/03/15/us-as-global-bourgeois-citadel/
categories:
  - Uncategorized

---
As the economic, military, and political citadel of the global bourgeoisie, the US has always had quite a bizarre, not to say schizophrenic, attitude towards foreign lands and peoples. 

From the very beginning of the US nation state, it has never been able to achieve a level of purely internal population growth that would allow it to mobilize enough workers, skilled and unskilled, to fully exploit the immense resources of the vast continent that fell under its control. Whence the truism, "a nation built by immigrants". And this remains truer than ever today, in an age of declining birthrates among domestically born citizens.

Yet, in order to become precisely such a citadel, as the ultimate defender of the wealthy and propertied few against any threats from the poor and destitute many worldwide, it has had to excite a state of constant, pathological paranoia against such foreigners and "aliens", whether at home or abroad. Whence the paradox: the US is the most diverse and "open" of all societies in the world, yet it is also the most aggressive and militaristic, too.

So, on the one hand, the country has benefited immeasurably from a nonstop "braindrain" of talented immigrants that it lures from countries worldwide, at great expense to their countries of origin. Yet, on the other, homegrown reactionaries make endless hay out of supposed threats from foreign "criminals", "terrorists", and "illegal voters", and on and on. (And these tropes are by no means new or unique to the Trump era, but merely a rehash of the same constant scare tactics, practically indistinguishable from the era of McKinley's assassination, or the first Red Scare of the teens and twenties of the last century.)

Another eternal, obviously moronic trope repeated by idiotic reactionaries, and merely rehashed by the likes of Trump, is the "go back to where you came from" line. Which misses an obvious fact of contemporary reality: while you can in principle "leave the US", the US, as the one and only remaining world hegemon, never "leaves" *you*. All the rest of the world, save one or two nation states, essentially falls into one of two camps: they are all either vassal states of the US, with very limited true national sovereignty, or they are under active economic and political if not military siege *by* it. 

Speaking of which, leftists are often accused by reactionaries of cowtowing to "illegals" and wishing they could vote "illegally". But perhaps it's time we stop being so bashful in the face of such boneheaded nonsense, and just roll out the old saw from this country's original revolution against the British Empire: "no taxation without representation!" Among whose obvious practical implications are: either the US and its Pentagon, Treasury, et al, must finally agree to lift its boot off the necks of the rest of the world, OR it should agree to let them all fully participate in what certainly amounts at present to an inadmissible scheme of "taxation without representation"!
