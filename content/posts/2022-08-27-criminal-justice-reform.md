---
title: Criminal Justice Reform
author: guyberliner
type: post
date: 2022-08-27T00:40:40+00:00
url: /2022/08/27/criminal-justice-reform/
categories:
  - Uncategorized

---
If you were a billionaire who liked cops, primarily because they never give YOU grief, but DO give your low wage workers -- who might, say, be inclined to organize and bargain with you, or bring actions against you for wage theft, or sexual harrassment, or other illegal workplace practices --  a LOT of grief, what would you do about a "progressive prosecutor", who sincerely believes in the principle of "equal justice under law", and the precept that it would be better to risk letting a hundred guilty men go free than to take the life and freedom of a single innocent one?

Maybe you'd drop a whole bunch of money on negative campaign advertising to vilify the prosecutor, by cherrypicking cases that sound like outrageous miscarriages of justice "favoring criminals". 

Some of those cases might be ones that would make ANY prosecutor look bad, like weak plea deals against criminals who promptly recidivate after serving short sentences. Except that the plea deals may have been the best the prosecutor could extract, given the paucity of evidence supplied by overworked, lazy, corrupt, or incompetent police.

Others might be marginal and involve judgment calls by the prosecutor. For example, the evidence might be way too marginal to risk going to trial, but easily strong enough to extract stiff plea deals. 

All of which leaves "progressive prosecutors" with a tough dilemma. Because their opponents will always seek to vilify them on the basis of anecdotes cherrypicked out of hundreds of cases. But the prosecutor will always be fighting an asymmetric battle against the billionaire funded "tough-on-crime" demagogues and their allied police unions, because the prosecutor will be privy to evidence about the case that they are not at liberty to reveal to the public, due to privacy concerns and the rights of both defendants and victims (and police officers!), all of which enter into their decisions whether and how to charge and prosecute an arrestee.

https://jacobin.com/2022/05/sf-da-crime-recall-chesa-boudin-wealthy
