---
title: Lumpen bourgeoisie
author: guyberliner
type: post
date: 2022-10-11T00:40:40+00:00
url: /2022/10/11/lumpen-bourgeoisie/
categories:
  - Uncategorized

---
Slavoj Zizek once quipped about the supposedly recent emergence of a "lumpen bourgeoisie", a riff on Marx's observations about the sub-class he referred to as the "lumpen proletariat" (ie, otherwise working class people condemned to what more recently has been called the "informal" sector, who are chronically unemployed or underemployed, and serve as part of the "reserve army of labor", even if they are never fully integrated into formal, wage-based capitalism and its working class). 

In Zizek's use of the term, though, he is referring to another implication of "lumpen", ie, tending to lack even the most rudimentary educational attainments, which would make one more eligible for wage labor, ie, more likely to be literate. Which, in turn, also makes one more easily reached by class based organizers, eg, agitators and pamphleteers. Thus, "lumpen" implies a level of desperate subsistence and lack of awareness that makes one part of a relatively "unorganizable" mass.

So the "lumpen bourgeoisie" of Zizek's imagination would then correspond to a section of the bourgeoisie relatively immune to appeals to "enlightened self-interest", or to the kind of aesthetic concerns to which Oscar Wilde appealed in his "Soul of Man Under Socialism". Accordingly, for example, a "lumpen bourgeosie" mercifully never produces any Lenins or Trotskys!

Sadly, though, the "lumpen bourgeoisie" is not only not new, it has always been closer to the rule than the exception. And it's a little hard to make the case that such a problem is any more acutely felt today than it ever was in the past. (Just read EP Thompson's account, in "Making of the English Working Class", of the ignorant, brutal, philistine mercenaries who constituted Manchester's manufacturing overlords during the first half of the Industrial Revolution. They made the likes of the Koch Brothers sound like highly enlightened savants by comparison!)

If anything, the recent rightwing urge in the United States to target public education and vilify things like "CRT", "wokeness", and any kind of education in the humanities, is arguably just part of a long standing program more aimed against members of the "lumpen bourgeoisie" than it is the proletariat! 

After all, suppressing any aspirations for an ordering of social priorities that ever aims at something beyond merely "dull economic compulsion", as Marx put it, is logically a far more urgent concern vis a vis the economic class of people who are already under the LEAST such compulsion, than it is against those who are under the most!

Covering any such movements among the bourgeoisie in a stench of maximal ill repute, then, is always very logically of paramount concern for reactionaries.

Accordingly, an entire language has been reinvented and renovated to this purpose by contemporary reactionaries. In the past, phrases like "champagne socialist" were used. Today, terms like "virtue signaling" and "white savior" are more contemporary usages that serve the same purpose.

The implication is always that certain very woolly-headed, comfortable people are promoting pie-in-the-sky ideas that have no practical application, other than to make themselves feel good, but that are actually detested by the good, salt-o-the-earth "masses" whom they claim to sympathize with. The ones hurling all the invective, it turns out, are the REAL advocates for these benighted masses. It is THEY who are schooled in "realism", "tough love", "street smarts", etc, etc, and understand "human nature".

Obviously, none of this rhetoric ever has to be supported by a body of compelling evidence or other substance, of course. This is never content designed for heady academic debates or something. It is more than enough if it is repeated ad nauseam with a suitably loud megaphone. It is the usual tactic of "hurling shit down from a great height", as someone put it recently, that makes all rightwing propaganda effective.

Accordingly, an entire language has been reinvented and renovated to  this purpose by contemporary reactionaries. In the past, phrases like  “champagne socialist” were used. Today, terms like “virtue signaling”  and “white savior” are more contemporary usages that serve the same  purpose.

The implication is always that certain very  woolly-headed, comfortable people are promoting pie-in-the-sky ideas  that have no practical application, other than to make themselves feel  good, but that are actually detested by the good, salt-o-the-earth  “masses” whom they claim to sympathize with. The ones hurling all the  invective, it turns out, are the REAL advocates for these benighted  masses. It is THEY who are schooled in “realism”, “tough love”, “street  smarts”, etc, etc, and who understand “human nature”.

Obviously, none  of this rhetoric ever has to be supported by a body of compelling  evidence or other substance, of course. This is never content designed  for heady academic debates or something. It is more than enough if it is  repeated ad nauseam with a suitably loud megaphone. It is the usual  tactic of “hurling shit down from a great height”, as someone put it  recently, that makes all rightwing propaganda effective.

The great thing about this kind of shorthand is that it so easily shortcircuits all rational thought, and can be deployed by anybody with a command of a handful of sharp insult lines, once they have been effectively memorized through sufficient repetition of straightforward formulas.  

Such rhetoric almost always fits easily within Twitter character limits, for example, requires little in the way of qualifications to deploy, and could probably even be almost fully automated, via cutting edge "machine learning" and "natural language processing" algorithms.

But beauty of it all is, even without resorting to fancier, high-tech techniques, once you build a critical audience mass for such "contentless" content, it can propagate itself, via the endorphin rewards it creates in that audience, without your having to pay extra for the grassroots "signal boost".  And, being both optimally simple AND labor saving, it thus constitutes almost the perfect kind of capitalist propaganda,  for both the industrial and post-industrial age.

("Dobbs", the infamous character from B. Traven's classic 1930s novel, "Treasure of the Sierra Madre", is the psychological model for all rightwing propaganda. He is the conniving villain who, partially in a bout of delirium brought on by isolation, partially as part of a longer, premeditated scheme, sets out to kill one of his friends and partners in a low budget gold mining adventure and steal their shares of the mutually mined gold. When his companion on the trail protests at Dobbs's accusations projecting Dobbs's own base motives on HIM, Dobbs dismisses the companion's protestations as part of a merely self-serving, conniving plot, thus justifying Dobbs's own villainy as really being an act of legitimate, preemptive "self-defense".)
