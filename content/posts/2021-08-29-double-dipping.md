---
title: Double-dipping
author: guyberliner
type: post
date: 2021-08-29T00:40:40+00:00
url: /2021/08/29/double-dipping/
categories:
  - Uncategorized

---
Rents are the bourgeoisie's way of "double-dipping", ie, extracting additional surplus value they can't get on their first pass (ie, via their ownership of the means of production and distribution of real goods and services).

Because the rates of profits on real goods and services are subject to inexorable downward pressures due to competition (among other factors), but the appetites of the bourgeoisie who own the means of production of those goods and services do not diminish in tandem with those profits, the former naturally look for other ways to prop their total returns up.

A rich portfolio of rents are available for this purpose exclusively to the big bourgeoisie (ie, capitalists who control the "commanding heights" of the economy, such as finance/insurance/real estate (aka, "FIRE"), industrial conglomerates like Koch Industries, etc). 

Rents (ie, guaranteed passive income streams) can be collected on everything from farm land (such as the holdings that Bill Gates buys up and rents out to aspiring young farmers), to no-bid contracts on bits of Britain's NHS that Boris Johnson's Tories are selling off to US-based Big Insurance/Big Pharma in no-bid contracts. (https://www.opendemocracy.net/en/ournhs/nhs-opening-us-business-reassurances-are-demonstrably-false/ https://www.theguardian.com/commentisfree/2020/jul/22/the-tories-new-trade-bill-means-the-nhs-is-now-unquestionably-up-for-sale)

The big bourgeoisie sweetens the deal for the petty bourgeoisie by joyfully suffering the latter to exploit weak tenants protections and extract a bit of rental income from the hardest up, propertyless tenants for themselves.

A careful consideration of the balance of material interests at stake here would probably shed a lot of light on the direct interest that the former group has in fomenting opposition to even the mildest public health interventions during our current pandemic emergency. (For example: 
https://www.commondreams.org/views/2020/10/16/koch-funded-legal-group-pushes-allow-mass-evictions-during-pandemic)

The most violent, reactionary faction of the big bourgeoisie has clearly flexed its muscles here and demonstrated its power to throw our entire society into chaos over a phantasm of "individual rights", while its hand-picked Supreme Court throws out millions of renters to suffer and possibly die on the streets during a global pandemic and shelter-in-place public health guidance from the CDC.

