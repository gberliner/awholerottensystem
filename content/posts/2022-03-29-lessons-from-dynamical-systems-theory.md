---
title: Lessons from dynamical systems theory
author: guyberliner
type: post
date: 2022-03-29T00:40:40+00:00
url: /2022/03/29/lessons-from-dynamical-systems-theory/
categories:
  - Uncategorized

---
We can learn important lessons from the cutting edge theory of dynamical systems. "Dynamical systems" are systems that obey a small number of simple, physical laws, but out of which can emerge complicated and surprising consequences. They provide models for approximating essentially all nontrivial physical systems in our world.

It turns out that, even when we limit the applicable physical laws exclusively to Newtonian mechanics, all but the very simplest models of our physical reality have a large number of states, the vast majority of which are completely chaotic, in the sense that their detailed and precise evolution defies any computation. But, over shorter or longer periods of time, these systems may converge on one of a much, much smaller number of stable equilibrium states (called "attractors", in the contemporary parlance of the field).

These equilibrium states have limited regimes of stability, ie, combinations of systemic conditions over which they will oscillate in a limited, predictable way around their equillibrium. But, when perturbations are introduced to these conditions, they can start to "wobble", and eventually collapse altogether out of their equilibrium regime, and lapse back into a chaotic state (which may or may not eventually lead into a new attractor state).

An example of a complex system that comes to mind, and that illustrates some very interesting dynamical properties that scholars in this field are closely studying nowadays, might be a set of holes in the ground, with tarp stretched loosely over the ground, and a set of golf balls in one of the holes.

Imagine you had a putting iron, and managed to knock a new golfball into the hole, where the others already were. If the hole was not too shallow, and the landscape fabric not too taut, the new ball would jostle the other balls already in the hole, but all of them would eventually settle back down into the hole.

But now imagine that a few people start yanking the four corners of the landscape fabric, while you knock new balls into the hole. The new balls will still jostle the others that are already in the hole, which will roll around the (shallower, because the landscape fabric is being pulled taut) hole, and eventually come to a rest again (after a measurably slightly longer time). 

The lengthening of the time to reach equilibrium (all the balls settled at the bottom of the hole) is called "critical slowing down" by contemporary dynamical systems theorists, and is regarded as a signal that can be used to detect when a system is approaching a critical stability threshold. (https://www.wired.com/2015/11/listening-to-natures-early-warning-system-may-save-species/)

Finally, if the ones yanking at the four corners pull the fabric tight enough, even the slightest perturbation may be enough to knock all the balls out of the hole altogether, causing them to roll chaotically around on the fabric. Maybe some will land in another hole, while others roll off the fabric altogether. The former equilibrium state, in any case, has completely collapsed.

Today, the phenomenon of "critical slowing down" is suspected of being observable in numerous different dynamical systems, including human mental and physical conditions like epilepsy, emotional depression, etc, as well as in ecological systems, like global climate.

So, perhaps we can observe useful signs and signals about our world, and hopefully act judiciously on them, even if any precise and detailed calculations elude us. In this way, our grip on our world, and even ourselves, cannot be fully described as "free will", nor as destiny. It is and always has been some strange mix of both.

