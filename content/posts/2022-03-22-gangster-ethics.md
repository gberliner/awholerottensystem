---
title: "Gangster ethics and 'omertà'"
author: guyberliner
type: post
date: 2022-03-22T00:40:40+00:00
url: /2022/03/22/gangster-ethics-and-omerta/
categories:
  - Uncategorized

---
If you think about it, Vladimir Putin's recent invasion of Ukraine, while a violation of ordinary, bourgeois ethics, as well as international norms, is perfectlly in keeping with street hoodlum ethics, or "omertà".

"Revenge is a dish best served up cold", goes the saying. And Putin's reaction to Ukraine -- and Ukraine's EU and NATO ambitions -- stays true to that principle. There was no new, immediate, credible casus belli -- at least none that hasn't already existed for almost a decade.

Sociologists who study ordinary street violence have long observed the class concentration of such phenomena. Random violence -- such as street brawls, bar fights, etc -- is overwhelmingly more prevalent among people who fall into lower socioeconomic strata. And its prevalence there is usually attributed to the more limited options of escape from one's immediate environment and social conditions allowed to people in such strata.

If one backs down from a fight and "loses face" at some bar, but one has infinitely many other bars they can go to -- perhaps a few blocks away, or even across town, if necessary -- it's no big deal. 

But suppose you can't afford to go to any bar, or have no means of transportation to get there. Or not enough time to do so, since your work shift starts too early in the morning. Then, the choice might be a lot starker: stand up for yourself at THIS bar, or lose face and never go back again, to the only bar you know and have any prospects of ever knowing, the only social scene you know, etc, etc.

Putin is behaving a lot more like the poor hoodlum with few choices who can't afford to lose face than like a billionaire for whom the world is his oyster. And maybe that's because, although he IS one of the richest, more powerful men on Earth, his geopolitical position and range of options is actually a lot poorer and weaker, relative to the overlords and plutocrats of the Western imperial countries.

So although Putin's behavior is thuggish, it may not be any superior sort of morality that separates him from his plutocrat brethren in richer countries, but only the law of the jungle: the dominant male in a pride of lions can afford to be a little more indulgent towards occasional annoyances from any weaker members of the pride than can a younger or weaker male lion still struggling to establish his place in the pecking order.

