---
title: Healthcare vs Social Darwinism
author: guyberliner
type: post
date: 2022-03-16T00:40:40+00:00
url: /2022/03/16/healthcare-vs-social-darwinism/
categories:
  - Uncategorized

---
On the neighborhood oriented website, "Nextdoor", one can easily read abundant posts by very clever (though not very cosmopolitan or widely read) petty bourgeois homeowners stumbling on what they imagine to be a profound and novel realization: that our society still does not yet strictly follow the logic of Social  Darwinism, but for which they are sure their neighborhoods would be much tidier and more pleasant.

Afterall, providing free social services of any description "enables" the continued survival of unsightly elements of the population who, whether or not they are willfully so, or merely cannot hack capitalism due to some combination of bad luck and genetic defects, eventually would surely either "shape up", move away, or simply perish,  much to the relief of their somewhat more affluent fellow citizens.

For any who are familiar with the intellectual history behind this line of thinking, it's precisely the basis for early 20th century eugenics, as first innovated in North America, and eventually applied even more vigorously in Nazi Germany. Interestingly, it's possibly also at the heart of why the United States still doesn't have any national health insurance program, unlike essentially every other major industrialized country, as author Thom Hartmann wrote in a very interesting book some years ago ("Hidden History of American Healthcare").

It seems that, in response to increasing calls for just such a program by progressive reformers as early as the late 19th and early 20th centuries, a certain brilliant Ivy League statistician and social scientist, Frederick Ludwig Hoffmann, became aware of some very enlightening health statistics vis a vis the recently formerly enslaved African American population.

Hoffman discovered that African Americans had dramatically higher morbidity and mortality rates than other races, and died much younger on average. In fact, he came to believe that, due to inherent genetic defects, African Americans would gradually die off altogether, if simply left to their own devices, thereby relieving the country of no end of headaches in the form of ongoing political and social problems and conflicts.

Hoffman therefore weighed in on the debate around public healthcare provision, and his voice was among the strongest arguing against any such universal provision, at least until such time as the "Black" problem was solved (or rather, organically "solved itself").

Hartmann argues in his book that Hoffman's charming hypothesis played an important role in quashing these early efforts at extending public health insurance along the lines of 19th century Bismarckian Germany's pioneering model, thereby helping to pave the way for the chaotic current private health insurance morass this country "enjoys" today.

Now, although Hoffman's precise hypothesis did NOT bear fruit in exactly the way he originally hoped, nonetheless, Social Darwinism has proven itself quite "correct", in a limited sense: It is entirely possible, as we see now, to render the average level of material and social conditions for large parts of the population virtually unlivable, with the side effect of creating a level of unbridled wealth concentration and opulence for a tiny part of the population, while an even larger, middling portion lives in a state of constant precarity, wedged between these two extremes: envying the plutocrats, while despising and openly hoping for the elimination of the beggars and lumpens in their midst (on account of the middle layer lacking, unlike the plutocrats, sufficient wealth to live in exclusive, inaccessible compounds where they might be spared any unpleasant contact with their social inferiors).
https://www.nwprogressive.org/weblog/2021/10/book-review-not-so-hidden-racism-and-profit-define-the-sickness-of-american-healthcare.html
