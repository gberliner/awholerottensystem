---
title: BBB is now DOA
author: guyberliner
type: post
date: 2022-01-07T00:40:40+00:00
url: /2022/01/07/bbb-is-now-doa/
categories:
  - Uncategorized

---
Biden's "Build Back Better" (BBB) is now DOA.

Dems can still talk about it *aspirationally*, but it's now more of an embarrassment than anything else, given that they have spent a whole year on it, without actually passing anything.

They need a strategy that salvages this disaster, while serving multiple goals at once, as they stare down the barrel of a GOP tsunami that combines organic dissatisfaction with their leadership together with a concerted Republican program to maximally weaponize every existing legal tool at their disposal to gerrymander, voter suppress, and outright nullify elections whose results displease them. All of which, it has been said repeatedly, constitutes a five-alarm-fire for the continued survival of small-d democratic governance in the United States.

Bernie Sanders has suggested pointing the finger for BBB's failure at both Republicans *and* recalcitrant Democrats who willfully betray their constituents' interests.

Meanwhile, House Democrats continue their "Jan. 6" investigations, and representatives like Jamie Raskin (D-MD) issue clarion calls for "accountability" (with little help from a largely flaccid DOJ which, aside from prosecuting a motley crowd of street-level offenders, like those who physically assaulted officers that day, or smeared feces on walls, has not lifted a finger to investigate, much less prosecute, any C-suite or Beltway insiders behind the larger coup attempt, details of which are even being openly boasted about by the likes of Trump's "trade advisor" Peter Navarro).

Jan. 6 is the tip of a vastly greater iceberg that's awfully GD relevant to anybody who is not in the top 1% It's just a token prelude of a GOP program for shoring up that party's longstanding demographic problems (owing to its always demographically-thin-on-the-ground voter base consisting of a coalition of a tiny group of plutocrats, and a larger, motley group of assorted libertarians, "traditional conservatives", bigots, religious cranks, etc).

Until very recently, they could just keep kicking that can down the road, by appealing to the odd assortment of occasional minorities ("conservative Asians, Latinos, Jews", etc). 

But Trump has forced their hand, by blowing the lid on that approach, and converting their demographic "base" into a full-on white nationalist political cult. That's a real problem for them, which is what now forces them to maximally weaponize all the latent, anti-majoritarian-democratic tools already baked into the constitution and other existing laws.

The end result will be turning the entire country into a version of Wisconsin writ large, where practically all officeholders enjoy completely noncompetitive districts and elections. As bad as things are now, and hard as it may be to imagine, this will make almost any efforts at political change vastly more difficult, barring outright violent revolution. 

This is really, really bad, coming at a time when climate apocalypse stares us in the face. And it also means that, as that apocalypse unfolds, the coping measures for dealing with it will veer more and more towards outright fascism -- eugenics, even more radical anti-immigrant moves, and so on.

Such a situation can appear utterly forlorn and hopeless. However, plenty of potentially promising ideas for tackling and salvaging something from the dire situation actually lie hidden in almost plain sight.

Biden, in the wake of his humiliation in the face of BBB, desperately needs a mission and talking points to save face, and grace. You could hear him grasping at some of that during his remarks commemorating the one-year anniversary of the Jan. 6 events. But unfortunately, Biden's instincts will remain limited to playing the role of "not Trump", unless vigorously goaded into doing anything more substantive.

Meanwhile, Bernie Sanders, not a tactically agile politician, but certainly a thoroughly anti-fascist one, has suggested the glimmerings of a path forward. Notably, Sanders was probably even more energetic a campaigner for Biden against Trump than Biden was! And now that the likes of Raskin and others are ringing such loud alarm bells, and the intentions of Republican politicians -- as well as their stunning NATIONAL level of anti-small-d-democratic coordination -- have become crystal clear to almost everybody, the makings of a counter-movement can emerge. 

Raskin, helpfully, suggested some important learnings recently that he claims to have garnered from his own historical research into the subject of "defending democracy" against fascist threats.

In particular, Raskin points out, from looking at other countries, that in other cases of incipient fascist movements, decisively turning the tide against them has always involved engaging coalitions that include at least some center and center right elements. The left alone has not been able to turn them back all by themselves, once they are strong enough to start to take root in the streets and the halls of power.

But, as Biden himself has just pointedly admitted (uncharacteristically!), practically no elected Republicans have been willing to substantively oppose Trump. McConnell, notably, was unable to muster even ten of his own party's senators to do so in his second impeachment trial. (This remained true despite all of them having been held hostage by a hostile crowd incited by Trump!)

So, if Raskin is right, and Biden's observations are taken to heart, what forward path is left to us??

Clearly, the only hope for avoiding a catastrophic outcome, which nobody on the Democratic side disputes lies in store absent defeating TRUMPISM (if not Trump himself, regardless whether he runs in 2024), is appealing to a FRACTION of Republican voters themselves.

Fortunately for Democrats, though, there actually exist surprising wedge issues that can do just that for them, while also advancing their own, preexisting priorities!

So in addition to Sanders's suggestion that BBB still be defended, but in the context of laying the blame for its (hopefully temporary) blockage at the feet of both Republicans and reactionary Democrats, let us take note of the surprising observation by journalist Paul Jay. 

Jay says that the climate crisis could be a hidden wedge issue lying in plain sight for Democrats. He points to recent polling data, according to which, the climate crisis is ranked as a number one concern by up to 5% of Republican voters! (A numerically small percentage, but easily enough to swing a close election, if Democrats actually exploited it the way Republicans aggressively exploit wedge issues that divide Democratic voters.)

What's needed, then, is a detailed program to develop this and other wedge issues. (Jay also suggested, based on estimates by economist Robert Pollin, that for a shockingly miniscule sum -- say, a few billion dollars, but certainly a minute fraction of BBB -- ALL fossil fuel workers could be guaranteed their full salaries for a period of several years during an inevitable transition away from coal and other dirty energy sources!) Such a campaign would require an emergency mobilization of highly strategic canvassing and GOTV work in swing districts, by students, minority activists, and other traditional Democratic constituencies such as has rarely happened in US politics, but something close to which was successfully carried out by Obama's 2008 campaign.

The first steps for such a mobilization would require representatives from ALL important Democratic constituencies coming together for planning and brainstorming sessions, to agree on a coordinated strategy. It sounds like a heavy lift, and yet, even most centrist and conservative Dems, including hardcore neoliberal ideologues, generally agree that fascism is bad, and a return of Trump, or Trumpism, is a completely dire prospect. Such a coordinated campaign does not even require firm commitments for progressive policies from the many more corporate friendly powerbrokers in the Democratic establishment. And it does not necessarily require such expectations from progressive activists, either.

What it does require is a recognition of certain basic, shared interests at this moment. And the imperative of getting Biden to at least prioritize publicly championing SOMETHING beyond the mere rhetoric of being "not Trump". Clearly, though, a substantive move like actually outright canceling at least $10k of student debt immediately would go a lot further as an earnest of something tangible for some of his hoped-for, core constituencies.

Progressive activists, on the other hand, must recognize that a return to Trumpism could be truly terminal, especially given the draconian, unprecedented package of anti-democratic legal measures Republicans are rapidly enacting. And also that, even if Biden only pays lip service to larger progressive goals, such rhetorical concessions could at least pave the way for more substantive ones in the future. His repeated knuckling under pressure, by at least postponing student debt repayments, for example, shows that he is not immune or impervious to persistent campaigning on progressive priorities targeting him.
