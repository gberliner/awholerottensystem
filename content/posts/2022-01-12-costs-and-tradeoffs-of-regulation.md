---
title: Costs and tradeoffs of regulation
author: guyberliner
type: post
date: 2022-01-12T00:40:40+00:00
url: /2022/01/12/costs-and-tradeoffs-of-regulation/
categories:
  - Uncategorized

---
"Crime doesn't pay", goes the saying. But what if it does? Obviously, not all criminals are caught, and though it's rarely discussed in any detail, the fact is, there is always a tradeoff between costs and benefits of controlling crime. Ideally, there would never be any crime, but in practice, the costs of creating an enforcement and legal apparatus completely airtight against ANY crime is usually too high, and we have to settle for a degree of trust, occasional "exemplary punishment", and the hope that deterrents are strong enough to dissuade most would-be criminals, as well as the occasional criminal being caught long after the act.

Conservatives are quick to condemn "overregulation" as being too "costly to business", and equally quick to applaud harsh punishment for "blue collar crimes", without noticing any apparent ironies in the contradiction. But surely, if there are tradeoffs in the one, there are also tradeoffs when it comes to the other.

A good book that explores some of tradeoffs inherent in the first kind of law enforcement is "Lying for Money", by Dan Davies. Someone should really write a rejoinder that compares the costs and tradeoffs when it comes to blue collar crimes and street policing.

Psychologically, the rightwing holds the advantage, unfortunately, because street crime mostly victimizes people of modest means, and is far more tangible to them than something like insider trading or financial fraud, even though the repercussions of the latter two are capable of being vastly more consequential to more people. Consequently, the tribunes of plutocracy can easily run circles around those of us who call for harsher treatment of white collar crime and (sometimes) more lenient treatment of blue collar ones.

But I dare say that the statistics and sociology are strongly on our side, in arguing for flipping our current script that coddles "crimes in the suites", while harshly and often indiscriminately suppressing "crime in the streets". Our emphasis has to be on "root causes", and clearly, crime that exacerbates inequality across the whole society is exactly the kind of "aggravating factor" we have to take most seriously. Why? Because we know that social inequality is a DRIVER for crime in general, especially violent crime. (eg, see: https://www.scientificamerican.com/article/income-inequalitys-most-disturbing-side-effect-homicide/ ) And even reactionaries have never bothered even attempting to argue for the reverse relationship (ie, arguing that "street crimes" somehow drive "suite crimes").

Unfortunately, though, such rational discussion tends to disappear completely in the entirely demagogic manipulation surrounding debates around slogans such as "defund police".
