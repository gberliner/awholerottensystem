---
title: The bourgeoisie are damaged people
author: guyberliner
type: post
date: 2021-06-29T00:40:40+00:00
url: /2021/06/29/2021-06-29-bourgeoisie-are-damaged-people/
categories:
  - Uncategorized

---
The bourgeoisie (aka, "super rich") are not to be envied their "good fortune". They are disproportionately likely to be profoundly morally and emotionally damaged people, paralyzed by fear and paranoia over any threats of diminution or loss of their privileged positions, and as a result, even incapable of responding to the plight of humanity and the rest of the living world around them.

The scariest part is, even later in life, members of the bourgeoisie are not able to act fearlessly as individuals with true agency over their own lives and moral aspirations. Rather, they are tied down by a very sick, quasi-feudal, "dynastic" philosophy of "generational wealth", according to which they are duty bound to ensure that "the better sort of people" (ie, their own kin) carry on their legacies of wealth and power. 

This partly explains why even politicians who reach the height of power and prestige, ie, the Joe Bidens of the world, still cannot act fearlessly in service of others, even in their twilight years, when one might naively suppose "they have nothing to lose".

Chuck Collins covers these themes and more in his excellent recent interview with Paul Jay about his (Collins's) latest book.

https://www.youtube.com/watch?v=p1waHr7qhuE
