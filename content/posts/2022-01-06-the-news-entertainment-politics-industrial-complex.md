---
title: The News-Entertainment-Politics Industrial Complex
author: guyberliner
type: post
date: 2022-01-06T00:40:40+00:00
url: /2022/01/06/the-news-entertainment-politics-industrial-complex/
categories:
  - Uncategorized

---
Donald Trump is a reality tv show star. Donald Trump is a perennial presidential candidate. And then, all of a sudden, Donald Trump is actually elected president. Many are surprised. Most are shocked.

Donald Trump enjoys being president. Donald Trump decides he wants to stay put, election or no. Donald Trump decides any election that would remove him is fraudulent. Donald Trump persistently makes allegations of fraud even before the elections happen. Donald Trump's official election returns displease him. Donald Trump finally incites a crowd to lay siege to Congress during its ceremonial quadrennial election certification. 

Again, many are surprised. Most are shocked. But, the thread binding all these observations all together is hidden in plain sight. As everybody already knows, the secret of successful advertising is passionate, persistent repetition.

And there's another continuity here of even more paramount importance. The erasure of firewalls between news, entertainment, and politics is one of the most notable -- and noted -- phenomena of the past fifty years. Donald Trump was not even our first mass-media-star-turned-president. That honor would go to Ronald Reagan.

But even before Reagan, Roger Ailes got his start in politics coaching Richard Nixon on how to make the most effective transition (or better, amalgamation) from dour politician to media celebrity. Nixon complained it was a shame serious leaders had to dabble in such gimmicks. But the young tv producer harshly lectured the president that "this was no gimmick", this was the future of all politics, full stop.

And Ailes was exactly right. In fact, Ailes played a central role in the full merger of these branches of "industry". Many had already started to point out how politicians were sold to the public like brands of toothpaste. But Ailes was thefirst one to actually perfect the art and science of doing it, going on to mastermind an entire corporate media empire, Murdoch's Foxnews franchise, dedicated to completing the highly purposeful merger of these once distinct realms.

That Trump decided "the election was stolen" should come as no surprise at all. The conceit that "the election was stolen" and the notion that "the media are fakes and liars" are coterminous. The fact that a widely recognized merger of these three distinct kinds of enterprises has in fact taken place and has been fully noted by almost all serious observers of each of the three is what supplies all the initial oxygen for Trump's claims. A lack of any supportive evidence becomes no obstacle anymore, in a universe in which it is widely recognized that "you can't trust the media", because their only product is profitable views sold to advertisers on behalf of other profitable businesses.

In Trump's world, there is no reason the election can't be "stolen", any more than a reality tv star who plays a successful business mogul has to actually have ever run a successful business, as opposed to a series of borderline scams sued by many of their own customers and employees. The ellision of all boundaries between fakery and reality, sanctified by capitalism itself in its almighty God-given quest for profits, also sanctifies any conceit a reality tv character entertains.

In Trump's mind, he very likely feels perfectly justified, given his highly elastic model of "ethics". Even if the facts don't support his exact storyline, other facts show that "all the politicians are corrupt, all the media lie all the time" anyways, so even if this particular election wasn't stolen in precisely the ways that Trump wants to spin, his particular spin merely pales in comparison to all the other spin and garbage being thrown out there all the time anyways. And, he can rationalize, his particular spin is in service of some "American greatness", unlike the other spins. So why on Earth should he be reproached for it, any more than these others?? 

People complain endlessly about "social media" these days, and put it at the root of the problem. But the fateful merger started long before it. The principle of selling the ears and eyeballs of viewers to advertisers and corporate sponsors was its foundation. The rest has been an organic unfolding, under the same pressures of capitalism under which all other industries operate.

Capital's imperative is accumulation, the multiplication of itself. Capitalism needs access to the ears, eyeballs, and votes of its customers -- consumers -- to do business. "Media" is, by definition, the middleman or broker between these two. Capital naturally insists on and drives relentlessly towards the breakdown of any "artiicial" barriers in the way of any of this.

In this regard, politics and politicians are merely a special trade in capitalism's larger sphere. The immediate currency of politicians is votes, which they need first, before they can sell their services to their sponsors -- who are predominantly capitalist businesses and their owners -- in the form of favorable policies, laws, and even public assets themselves, which businesses might be interested in profitably acquiring and winning rents from.

Donald Trump fudging votes and election outcomes to his own personal benefit, in this scheme of things, is only a logical extension of Donald Trump fudging his business books to ensure his own personal balance sheets remain positive. If he has never faced any serious personal and legal consequences for the latter, that alone sanctifies the soundness, success, and respectability of his approach, certainly in his own eyes, at least. Why on Earth should he not extend it logically whever else he can?? Again, we can just see it as the merger of business, entertainment, and politics, that are only once again seeking out their natural completion here.

So many echoes of all these phenomena have already long since haunted our culture. The sci-fi-horror-show-turned-reality nightmare of a fascist regime blending media images, news reporting, and politics was already prefigured in Ray Bradbury's dystopian novel, "Fahrenheit 451", and even Orwell's "1984". The film "Network" (1976), Paddy Chayevsky's dark satire, was already eerily prescient. The tv show "Cops" took a big step towards moving Bradbury's fantasy novel straight into reality. Trump's political career has taken another.

Donald Trump does not come to "drain the swamp". Donald Trump is the creature risen out of the swamp itself. But Donald Trump can handily point back, reproachfully, at that swamp, one not of his own creation at all, and in that much, strictly speaking, he is correct.

