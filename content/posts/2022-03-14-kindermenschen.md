---
title: Kindermenschen
author: guyberliner
type: post
date: 2022-03-14T00:40:40+00:00
url: /2022/03/14/kindermenschen/
categories:
  - Uncategorized

---
In *Siddhartha*, Hermann Hesse's fanciful retelling of the Buddha's life, he dubs the urban sophisticates whose lives the protagonist momentarily immerses himself in following his encounter and affair with the courtesan Kamala as "Kindermenschen", or "childlike people". 

These latter are noteworthy for their intense seriousness and industriousness, for coveting and building elaborate houses and "pleasure gardens" in their city, and for winning and losing immense fortunes in the course of a few hours of dice and like games of chance, even while Siddartha attained his supreme realization in the course of aimlessly walking on foot through an immense wilderness, in the company of penniless monks and the occasional salt-of-the-earth boatman. 

His epithet for them, though, is not solely dismissive, but also intensely sad. For he discovers, it turns out, that he can only fully empathize with their suffering by fully sharing, at least for a time, in their very same delusion.

Today, the heirs of *Siddhartha*'s "Kindermenschen" gamble no longer just with dice and pleasure palaces, but with the fate of an entire planet.
