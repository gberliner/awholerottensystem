---
title: Shadowboxing phantom problems, while ignoring real ones
author: guyberliner
type: post
date: 2021-08-12T00:40:40+00:00
url: /2021/08/12/shadowboxing-phantom-problems-ignoring-real-ones/
categories:
  - Uncategorized

---
Our society dedicates monumental and inordinate efforts to combating hypothetical, phantom problems that are scarcely ever in evidence, while studiously ignoring burning fires right under our noses -- literally.

Take inequality. The evidence for the toll taken by extreme inequality is everywhere, but our society dedicates its energies to fighting the supposed dangers of "too much equality", on the theory that, otherwise, we might run afoul of the danger of "producing too little" by not sufficiently "incentivizing hard work". 

Meanwhile, children are left unattended by overworked parents, desperate social problems are ignored all around us by harried citizens who have no time to engage in civic activities for fear of losing their jobs, housing, health care, and on and on. And so far from "underproduction" being a problem, consumer excess and OVERPRODUCTION rapidly turn whole landscapes into strip mines and toxic landfills. And a set of plausible explanations for problems that may have occurred historically in other places is used as a pretext for inflicting massive distress and misery here on our own society today.
