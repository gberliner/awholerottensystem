---
title: Courts cannot protect majorities
author: guyberliner
type: post
date: 2021-08-30T00:40:40+00:00
url: /2021/08/30/courts-cannot-protect-majorities/
categories:
  - Uncategorized

---
A long history of jurisprudence in the US proves conclusively that courts cannot and will not ever protect majorities against powerful and predatory minorities.

Courts rightly defer to legislatures in many cases, arguing that "political questions" should be decided by the "majority", and not by judicial fiat. The trouble is, most legislatures themselves are increasingly minoritarian, and wildly diverge from being representative of the larger population.

Moreover, private power can dispense with a lot of public goods and services altogether, and increasingly does not need or depend on them. Accordingly, private oligarchs are content -- in fact, delighted -- to see to it that public goods and services decay.

The Supreme Court's ruling in [Castle Rock v. Gonzales (2005)](https://supreme.justia.com/cases/federal/us/545/748/) is instructive in this regard. The Court held that Gonzales, a domestic abuse victim, had no due process claims against the City of Castle Rock, Colorado, and its police for demanding enforcement of a temporary restraining order against her abusive ex-spouse. Although the legislature had passed a law that appeared to categorically require it, the Court essentially said the language was still "not strong enough" to override a presumption of discretion on the part of police. The Court's ruling was based on a narrow reading of "due process of law" that only took into account the part about property rights (one cannot be deprived under the 14th Amendment of "life, liberty, or property" without "due process of law").

The Court was restricted to consider only "property rights" here because, again, the 14th Amendment contains no protections against private abuses committed by one individual against another. So the plaintiff in the original case had to argue that they were deprived of a "property right".

Ironically, the Court itself even considered precedents that suggested how to interpret the right in question as a "property right", by evaluating the cost of retaining private security in place of seeking a court order, say. But its majority opinion airily dismissed this line of reasoning without further consideration, by arguing that the services of afforded by private security are far different (and more extensive) than those provided by a court order.

Unsurprisingly, the entire line of reasoning in the Court's opinion in Castle Rock v Gonzales ignores the significance of class distinctions, and the fact that a rich individual would hardly have to make recourse to any public remedies at all in such a case. Whereas a person of modest means is going to have to depend on the smooth and effective functioning of a whole panoply of public institutions (INCLUDING but not limited to enforcement of a temporary restraining order, courts like theirs to bring actions in case the order is not being enforced, etc).

The end result of this and other rulings is that the rich have every incentive to use their influence over legislative and executive branch officials to degrade and demolish public institutions for their private gain, knowing that the vast, working class majority will then have no legal recourse against their depredations. 

In fact, powerful and predatory minorities will go one step further and subvert and weaponize theoretically “majoritarian” institutions against majorities, whenever they can do so, leaving no available legal recourse to their victims.

This is precisely how a depraved, ultraviolent bourgeois class dictatorship is built.
