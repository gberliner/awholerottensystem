---
title: "Fundamental principles that can't be proven"
author: guyberliner
type: post
date: 2022-01-18T00:40:40+00:00
url: /2022/01/18/fundamental-principles-that-cant-be-proved/
categories:
  - Uncategorized

---
A lot of fundamental principles are potentially "wrong", depending on our precise definitions of certain words, but can't be conclusively proven or disproven. A slogan like "all men are created equal" of the American Declaration of Independence (today, we'd say, "all people") is not a statement of fact so much as an ethical principle for organizing society.

The latter principle is so foundational, it's counted among the three cornerstones of the French Revolution: "liberty, equality, fraternity". And it remains a truly revolutionary one, whose full implications are far from being realized.

Aside from the fact that what exactly is meant by "equal" is highly ambiguous, it's clear that people are different and highly varied. And the precise extents to which such variations are owing to environment, genetics, history, and so on, is also highly ambiguous.

Instead, though, the true, revolutionary ethos of the principle has to be embraced as not just a vague idea, but also an aspiration implying a call to action.

Across innumerable spans of human history, empires have risen and fallen, social status has varied and changed, and with them, ideologies and religions have come and gone. There are not necessarily any simple formulae  or lodestones upon which to hold onto. So, if we embrace an ethos like equality, for example, it cannot be solely -- or even primarily -- on the basis of some narrow set of facts (although it's certainly not going to be INSPITE of all the facts we know, either).

Instead, though, we opt for such a principle because it accords with innumerable elements of our common sense arising from our daily experience, as much as any abstract knowledge of history and human existence.

But to fully embrace a principle like equality, in a world riven by arbitrary and horrifying inequalities of condition, is to embrace a constant ethical challenge, even within our own personal lives. It's not likely to be very comfortable for many of us, who may compare our situations to some others relatively or very favorably. So it stands to reason that the attraction of such a principle, when a smorgasbord of other, alternative organizational principles is also on offer, may be severely limited.

Accordingly, those of us who do adhere to such a principle have to think long and hard about our arguments. Certainly we have to make arguments about facts, and history. But we also have to make arguments pointing to the concrete realities of cooperation, interdependence, and mutualism that are always all around us.

Certain arguments, in this fraught context, make a whole lot more sense than others. Arguments, for example, that blithely ignore all self-interest, are obviously incredibly dangerous. The conceit, for example, that we can lecture people about a concept such as "white privilege", and simply assume that, being self-conceived "good people", they are bound to recalibrate all their behavior so as to abandon such an unearned benefit, in favor of other people most of whom they don't even know, seems downright bizarre.

We ought, instead, to use a very different sort of terminology. "Psychological wages" is a pretty good one, for example. Emphasizing that, for example, racism has inflicted grievous harm on ALL working class people (most of whom are, in the United States, not historically part of explicitly identified-as-disadvantaged minority groups), is also crucial. Pointing out that, in absolute numbers, far more *white people* are victims of police brutality than African Americans, for example, is both a basic truth and very prudent to strongly emphasize.

