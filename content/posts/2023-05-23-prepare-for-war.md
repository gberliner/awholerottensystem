---
title: "Prepare for war"
author: guyberliner
type: post
date: 2023-05-23T00:40:40+00:00
url: /2023/05/23/prepare-for-war/
categories:
  - Uncategorized

---
"If you want peace, prepare for war."

Τhis aphorism from a classical Roman military text purports to express a dubious sort of truth that is more nearly the opposite of reality. Naturally, though, this does not prevent it from being a favorite chestnut of reactionaries everywhere.

The broader truth, though, is that holding low expectations of others is a great way of assuring that they sink to them -- or worse. Whereas holding high expectations, and seeking to live up to them yourself, is the more reliable way to improve your mutual prospects.

Once again, the pithy "contrarian" cynicism of the "tough guy" proves itself to be a form of mere "crackpot realism". It is the philosophy of the villain Dobbs in "Treasure of Sierra Madre" who, half crazed from thirst and exhaustion on the trail back from hauling their equal shares from a joint gold mining party, accuses his companion of planning to lie in wait for him and kill him in his sleep, so as to make off with all the booty for himself. His companion vehemently denies such nefarious schemes. But Dobbs mocks his pious denials. This then forces his companion to arm himself and hold Dobbs at gunpoint himself, to avoid becoming the victim of the latter's planned, supposedly"preemptive" attack. Thus Dobbs brings about an outcome resembling the very one he cynically claims to fear.

Dobbs could well be taken for the avatar of all reactionary politics, fascism, militarism, and imperialism.
